package com.premiumconnect;

import android.app.Application;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.appevents.AppEventsLogger;

import com.corbt.keepawake.KCKeepAwakePackage;
import com.facebook.react.ReactApplication;
import com.zmxv.RNSound.RNSoundPackage;
import com.ocetnik.timer.BackgroundTimerPackage;
import com.github.wumke.RNImmediatePhoneCall.RNImmediatePhoneCallPackage; 
import com.dooboolab.RNIap.RNIapPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.RNPlayAudio.RNPlayAudioPackage;
import com.dooboolab.RNAudioRecorderPlayerPackage;
import io.rumors.reactnativesettings.RNSettingsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {
  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
    
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNSoundPackage(),
            new BackgroundTimerPackage(),
            new RNImmediatePhoneCallPackage(),
            new RNIapPackage(),
            new ReactNativeContacts(),
            new ReactNativeAudioPackage(),
            new FIRMessagingPackage(),
            new RNPlayAudioPackage(),
            new RNAudioRecorderPlayerPackage(),
            new RNSettingsPackage(),
            new MapsPackage(),
            new RNFirebasePackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(mCallbackManager),
            new RNI18nPackage(),
            new PickerPackage(),
            new RNFirebaseAuthPackage(),
            new KCKeepAwakePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
