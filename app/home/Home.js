import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  BackHandler,
  AppState
} from 'react-native';
import { Tabs, Tab, Icon, Toast } from 'native-base'
import GroupList from '../componnents/GroupList'
import Header from '../componnents/Header'
import CreateGroup from './createGroup/CreateGroupStep1'
import CreateGroupStep2 from './createGroup/CreateGroupStep2'
import CreateGroupStep3 from './createGroup/CreateGroupStep3'
import CreateGroupStep4 from './createGroup/CreateGroupStep4'
import I18n from 'react-native-i18n';
import Api from '../componnents/api'
import FCM, { FCMEvent } from 'react-native-fcm'
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import { Actions } from 'react-native-router-flux';
import firebase from '../componnents/Firebase';
var chatData
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groupList: [],
      newGroupList: [],
      button: 'grouplist',
      initialTab: 0,
      groupType: '',
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      loading: false,
      loading2:false,
      groupName: '',
      selectPlan: '',
      imageData: '',
      isAdmin:false,
      moveDown:'',
      buttonType: [
        {
          button: 'grouplist',
          groupType: 'public',
        },
        {
          button: 'grouplist',
          groupType: 'public',
        },
        {
          button: 'grouplist',
          groupType: 'public',
        }
      ],
      tabindex:this.props.groupType? this.props.groupType == 'public' ? 0 : this.props.groupType == 'private' ? 1 : 2 : 0
    }
    this.buttonClick = this.buttonClick.bind(this)
    this.searchFilterFunction = this.searchFilterFunction.bind(this)
   
  }
  buttonClick(name, imageData, groupType, groupName, selectPlan) {
    // console..log("group type:  : ", name, imageData, groupType, groupName, selectPlan)
    this.setState(state => (
      this.state.buttonType[this.state.tabindex].button = name,
      this.state.buttonType[this.state.tabindex].groupType = groupType,
      this.state.buttonType[this.state.tabindex].groupName = groupName,
      this.state.buttonType[this.state.tabindex].selectPlan = selectPlan,
      this.state.buttonType[this.state.tabindex].imageData = imageData
      , state))
  }
  async componentWillMount() {
    AsyncStorage.getItem('isAdmin').then((isadmin)=>{
      // console..log('is admin : ',JSON.parse(isadmin))
      this.setState({isAdmin:JSON.parse(isadmin)})
    })
   
    await FCM.requestPermissions({ badge: true, sound: true, alert: true })
  
    FCM.on(FCMEvent.Notification, async (res) => {

       if (res.opened_from_tray) {
        // console..log("opened from tray", res)
        if (res.type == 'event') {
         // // console..log('event data========> : ', JSON.parse(res.event))
          var event = JSON.parse(res.event)
          Actions.Chat({ private: this.props.private, groupData: { id: res.groupId, name: res.groupName }, _eventData: event })

        }
      } else {
        // console..log("notification", res)
        if (res.type == 'event') {
          //// console..log('event data========> : ', JSON.parse(res.event))
          var event = JSON.parse(res.event)
          Actions.Chat({ private: this.props.private, groupData: { id: res.groupId, name: res.groupName }, _eventData: event })
        }
      }
    });

    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng, loading: true })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng, loading: false }))
    this.getGroupList("PUBLIC")
    btntype = this.state.buttonType
    index = this.state.tabindex
    BackHandler.addEventListener('backPress', () => {
      if (btntype[index].button == 'create') {
        this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'grouplist', state))
      }
      else if (btntype[index].button == 'step2') {
        this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'create', state))
      }
      else if (btntype[index].button == 'step3') {
        this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'step2', state))
      }
      else if (btntype[index].button == 'step4') {
        if(btntype[index].groupType=='public' || this.state.isAdmin)
          this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'step2', state))
        else
          this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'step3', state))
      }
      else if (btntype[index].button == 'payment') {
        this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'step4', state))
      }
      else if (btntype[index].button == 'grouplist') {
        Toast.show({
          text: "Press Back Again To Exit App!",
          duration: 3000
        })
        this.setState(state => (this.state.buttonType[index].button = '', state))
        setTimeout(() => {
          // console..log("exit again")
          this.setState(state => (this.state.buttonType[index].button = 'grouplist', state))
        }, 3000)
      }
      else {
        BackHandler.exitApp()
      }
    })
  }
  componentDidMount(){
    AppState.addEventListener('change', this._handleAppStateChange)
    chatData = firebase.database().ref().child('groupMessage')
    chatData.on('value', snapshot => {
      // console..log("grp type: ",this.state.buttonType[this.state.tabindex])
      var grouptype=this.state.buttonType[this.state.tabindex].groupType=='public'?'PUBLIC':this.state.buttonType[this.state.tabindex].groupType=='private'?"PRIVATE":"MYGROUPS"
      // console..log("group----- ",grouptype)
      this.getGroupList(grouptype,false)
    
    })
  }
  componentWillUnmount(){
   
    AppState.removeEventListener('change', this._handleAppStateChange)
  
  }
  _handleAppStateChange = (nextAppState) => {
    // console..log("willunmount",nextAppState)
    chatData.off()
 } 
 _logout() {
  AsyncStorage.getItem('userData').then(userData => {
    // console..log("user data : ", userData)
    userdata = JSON.parse(userData)
    if (userdata.signUp_with == 'google')
      GoogleSignin.signOut()
    else if (userdata.signUp_with == 'facebook')
      LoginManager.logOut()
    //  this.props.clearInterval()
    AsyncStorage.clear()
    Actions.reset('Login')
  })
}
  getGroupList(groupType,loader) {
    // console..log("group type : ", groupType)
   this.setState({loading2:loader})
    AsyncStorage.getItem('userData').then(user=>{
     user=JSON.parse(user)
    AsyncStorage.getItem('token').then(token => {
      AsyncStorage.getItem('deviceId').then(deviceID => {
        console.log("device Id -------------------- {{{{{{{{{{{}}}}}}}}}}}: ",deviceID)
        Api.postWithToken('/api/matchDeviceId', { deviceId: deviceID }, token)
          .then((result) => {
             console.log('logout  ==> ', result)
            if (result.logout) {
              this._logout()
            }
          })
      })
      Api.postWithToken('/api/getallgroup', { groupType: groupType, token })
        .then(res => {
          // console..log("getAllgroup api data : ", res)
          data = []
          notifygroup=[]
          if (res.data.status == 'Success') {
            // console..log('data  =========>  : ', res.data.data)
            res.data.data.map((group, index) => {
              onlineCount = -1
              group.userData.map(item => {
                if (item.online)
                  onlineCount += 1
              })
              // console..log('data grouppp =========>  : ',group)
              var chatData = firebase.database().ref().child('groupMessage/' + group._id + '/messages')
              chatData.once('value', snapshot => {
              var  member=snapshot.child('members').val()
                if(member){
                  var newMember=true
                  //// console..log('data grouppp =========>  : ',group)
                  member.map(item=>{
                  // // console..log("mmmememmmmvmmmmmv : ",item)
                    if(item.id==user._id){
                      data[index] = {
                        id: group._id,
                        name: group.groupName,
                        onlineMembers: onlineCount,
                        profile: group.groupImage,
                        createdDate: group.createDate,
                        members: group.membersList.length,
                        groupType: group.groupType,
                        cityName: group.cityName,
                        city_latitude: group.city_latitude,
                        city_longitude: group.city_longitude,
                        creater: group.creater,
                        notification:item.newMsg
                      }
                      newMember=false
                    }

                })
                if(newMember){
                  data[index] = {
                    id: group._id,
                    name: group.groupName,
                    onlineMembers: onlineCount,
                    profile: group.groupImage,
                    createdDate: group.createDate,
                    members: group.membersList.length,
                    groupType: group.groupType,
                    cityName: group.cityName,
                    city_latitude: group.city_latitude,
                    city_longitude: group.city_longitude,
                    creater: group.creater,
                    notification:false
                  }
                }
                  
                }
                else{
                  // console..log('data grouppp =========>  : ',group)
                  data[index] = {
                    id: group._id,
                    name: group.groupName,
                    onlineMembers: onlineCount,
                    profile: group.groupImage,
                    createdDate: group.createDate,
                    members: group.membersList.length,
                    groupType: group.groupType,
                    cityName: group.cityName,
                    city_latitude: group.city_latitude,
                    city_longitude: group.city_longitude,
                    creater: group.creater,
                    notification:false
                  }
                }
                // console..log('data =========>  : ',data)
                if(index==res.data.data.length-1){
                  this.setState({ groupList: data, newGroupList: data })
                  this.setState({loading2:false})
                }
              })
              
           
            
            })
          }
          else{
            if(res.success==false){
              this._logout()
            }
            this.setState({ groupList: data, newGroupList: data,loading2:false })
          }
         
          // console..log('data : ', data)
          
        })
        .catch((err) =>  console.log('ERROR', err))
    })
 
  })
  
}
  currentTab(tab) {
    if (tab.i == 0) {
      this.getGroupList("PUBLIC",true)
      this.setState({ tabindex: 0 })
    }
    else if (tab.i == 1) {
      this.getGroupList("PRIVATE",true)
      this.setState({ tabindex: 1 })
    }
    else {
      this.getGroupList('MYGROUPS',true)
      this.setState({ tabindex: 2 })
    }
  }
 
  groupList(_private, mygroup) {
    btntype = this.state.buttonType
    index = this.state.tabindex
    if (btntype[index].button == 'create')
      return <CreateGroup button={this.buttonClick} />
    else if (btntype[index].button == 'step2')
      return <CreateGroupStep2 button={this.buttonClick} imageData={btntype[index].imageData} />
    else if (btntype[index].button == 'step3')
      return <CreateGroupStep3 button={this.buttonClick} groupType={btntype[index].groupType}
        imageData={btntype[index].imageData}
        groupName={btntype[index].groupName} />
    else if (btntype[index].button == 'step4')
      return <CreateGroupStep4 button={this.buttonClick} groupType={btntype[index].groupType}
        imageData={btntype[index].imageData}
        groupName={btntype[index].groupName}
        selectPlan={btntype[index].selectPlan}
      />
    else if (btntype[index].button == 'payment')
      return <Payment button={this.buttonClick} />
    else
      return this.state.loading2?
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
         <ActivityIndicator size='large' color='black' />
      </View>:
      <GroupList searchFilterFunction={this.searchFilterFunction} data={this.state.newGroupList} private={_private} mygroup={mygroup} />

  }

  createGroup() {
    this.setState({ button: 'create' })
    this.setState(state => (this.state.buttonType[this.state.tabindex].button = 'create', state))
  }
  searchFilterFunction(text) {
    const newData = this.state.groupList.filter(item => {
      const itemData = item.name.toUpperCase();
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ newGroupList: newData });
  };
  render() {
    I18n.locale = this.state.appLanguage
    if (this.state.loading)
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='large' color='black' />
        </View>
      )
    else
      return (
        <View style={styles.container}>
          <Header openDrawer={this.props.openDrawer} />
          <Tabs
            initialPage={this.state.initialTab}
            tabBarUnderlineStyle={{ backgroundColor: '#020304' }}
            onChangeTab={(tab) => this.currentTab(tab)}>
            <Tab tabStyle={{ backgroundColor: '#020304' }}
              activeTabStyle={{ backgroundColor: '#020304' }}
              textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 12 }}
              activeTextStyle={{ color: '#ff7a00', fontSize: 12 }}
              heading={I18n.t('public groups')}>
              {this.groupList(false)}
            </Tab>
            <Tab
              tabStyle={{ backgroundColor: '#020304' }}
              activeTabStyle={{ backgroundColor: '#020304' }}
              textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 12 }}
              activeTextStyle={{ color: '#ff7a00', fontSize: 12 }}
              heading={I18n.t('private groups')}>
              {this.groupList(true)}
            </Tab>
            <Tab
              tabStyle={{ backgroundColor: '#020304' }}
              activeTabStyle={{ backgroundColor: '#020304' }}
              textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 12 }}
              activeTextStyle={{ color: '#ff7a00', fontSize: 12 }}
              heading={I18n.t('my groups')}>
              {this.groupList(false, true)}
            </Tab>
          </Tabs>
          {
            this.state.buttonType[this.state.tabindex].button == 'grouplist' ?
              <TouchableOpacity
                onPress={() => this.createGroup()}
                style={styles.addgroupbtn}>
                <Icon name='group-add' type='MaterialIcons' style={{ color: 'white', fontSize: 35 }} />
              </TouchableOpacity> : null
          }
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  addgroupbtn:{
    position:'absolute',
    bottom:20,
    backgroundColor:'black',
    width:60,
    height:60,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'center',
    borderRadius:50
  }
});
