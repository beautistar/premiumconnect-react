import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  AppState,
  View,
  Linking,
  Text,
  AsyncStorage,
  Alert
} from 'react-native';
import SideBar from '../componnents/SideBar'
import RNSettings from 'react-native-settings'
import Api from '../componnents/api';
import Home from '../home/Home';
import firebase from '../componnents/Firebase';
import { Drawer } from 'native-base';
import { Actions } from 'react-native-router-flux'
import BackgroundTimer from 'react-native-background-timer';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import moment from 'moment'

var interval
var max = 0, oldLat, oldLong, time
export default class AppDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage: this.props.lng ? this.props.lng : 'en',
      loading: true,
      drawerOpen: false,
      speed: 0,
      gps: '',
      appState: AppState.currentState,
      subscriber: false,
      subscription: '',
      isAdmin: false,

    }
    this.clearInterval = this.clearInterval.bind(this)
  }
  _handleSharedLocation(action, location, item, userid) {
   // // console..log('ITEM KEY', item.key)
    var shareRef = firebase.database().ref().child('/shareLocation').child(userid)
    if (action == 'accept') {
      shareRef.remove()
      Actions.SharedLocation({ userLocation: location })

    } else {
      shareRef.child(item.key).remove()
    }
  }
  clearInterval() {
  ///  // console..log("clear interval")
    // if (Platform.OS == 'ios')
    //   BackgroundTimer.stop();

    // BackgroundTimer.clearInterval(timeoutId);
    clearInterval(interval)
  }
  
  componentWillMount() {
    AsyncStorage.getItem('isAdmin').then((isadmin) => {
    //  // console..log('is admin : ', JSON.parse(isadmin))
      this.setState({ isAdmin: JSON.parse(isadmin) })
    })
    AsyncStorage.getItem('token')
      .then((token) => {
       // // console..log("token========================> : ",token)
       
        Api.get('/api/EmsSubscription', token)
          .then((result) => {
           // // console..log('Check Subscription Api Result', result)
            this.setState({ subscriber: result.data.subscription, subscription: result.data })
          })
      })
    var key = []
    AsyncStorage.getItem('userData')
      .then((data) => {
        var user = JSON.parse(data)
        var shareRef = firebase.database().ref().child('/shareLocation').child(user._id)
        shareRef.on('value', snapshot => {
          snapshot.forEach((item) => {
            // console..log('Value', item.val())
            if (!key.includes(item.key)) {
              key.push(item.key)
              key.map((_key) => {
                Alert.alert(
                  'Location Shared',
                  `${item.val().name} wants to share his location`,
                  [
                    {
                      text: 'Decline',
                      onPress: () => this._handleSharedLocation('decline', null, item, user._id),
                      style: 'cancel',
                    },
                    { text: 'Accept', onPress: () => this._handleSharedLocation('accept', item.val().location, item, user._id) },
                  ],
                  { cancelable: false },
                );
              })
            }
          })
        })
      })
  }
  closeDrawer() {
    this.drawer._root.close()
    this.setState({ drawerOpen: false })
  }
  openDrawer() {
    this.drawer._root.open()
    this.setState({ drawerOpen: true })
  }
  _openLocationSetting() {
    if (Platform.OS == 'android') {
      RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
        then((result) => {
          if (result === RNSettings.ENABLED) {
           // // console..log('location is enabled')
            this._getLocation()
          }
        })
    }
    else {
      Linking.openURL("App-Prefs:root=Privacy&path=LOCATION_SERVICES")
    }
  }
  _logout() {
    AsyncStorage.getItem('userData').then(userData => {
      //// console..log("user data : ", userData)
      userdata = JSON.parse(userData)
      AsyncStorage.getItem('token')
        .then((token) => {
          //// console..log("user token : ", token)
          Api.postWithToken('/api/logout', { memberId: userData.id }, token)
            .then((result) => {
              // console..log('Logout Api Result', result.data.status)

              if (result.data.status == 'Success') {
              //  // console..log("google : ", userdata.signUp_with)
                if (userdata.signUp_with == 'google') {
                  GoogleSignin.signOut()
                  AsyncStorage.clear()
                  Actions.reset('Login')
                }
                else if (userdata.signUp_with == 'facebook') {
                  LoginManager.logOut()
                  AsyncStorage.clear()
                  Actions.reset('Login')
                }
                else {
                //  // console..log("logout ----->()")
                  AsyncStorage.clear()
                  Actions.reset('Login')
                }
                navigator.geolocation.clearWatch(watchid)
              }
            })
        })
    })

  }
  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) *
      Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }
  _update(status, lat, long) {
    AsyncStorage.getItem('token')
      .then((token) => {
        Api.put('/api/updatelocation', { latitude: lat, longitude: long, status: status }, token)
          .then((result) => {
           // // console..log(' Update Location api result', result)
          })
      })
  }
  getDistance(lat, long) {
    //// console..log("get distance : ",lat,"  ",long,'old lat long : ',oldLat," ",oldLong)
    if (oldLat && oldLong) {

      distance = this.getDistanceFromLatLonInKm(oldLat, oldLong, lat, long).toFixed(3)
      // // console..log('distace : ',distance)
      if (distance > 0.003) {
        oldLat = lat
        oldLong = long
        time = moment()
       // // console..log("green")
        this._update('green', lat, long)
      }
      else {
        var duration = moment.duration(moment().diff(moment(time)));
        var minutes = duration._data.minutes;
        var seconds = duration._data.seconds;
        //  // console..log(minutes)
        if (minutes == 15 && seconds == 0) {
         // // console..log("yellow")
          this._update('yellow', lat, long)
        }
        if (minutes == 30 && seconds == 0) {
         // // console..log("red")
          this._update('red', lat, long)
        }
        if (minutes == 59) {
        //  // console..log("logout")
          this._logout()
        }
      }

    }
    else {
      oldLat = lat
      oldLong = long
      time = moment()
    }
  }
  _getLocation() {
    watchid = navigator.geolocation.watchPosition((position) => {
     console.log('watch Location', position.coords.latitude, position.coords.longitude)
     //  // console..log('---------========{max value}============------------- : ', max)
      this.getDistance(position.coords.latitude, position.coords.longitude)
      //this.getDistance(21.182452 , 72.826381)
      // AsyncStorage.getItem('token')
      //   .then((token) => {
      //     Api.put('/api/updatelocation', { latitude: position.coords.latitude, longitude: position.coords.longitude }, token)
      //       .then((result) => {
      //         // console..log('Location api result', result)
      //         if (result.data.logout) {
      //           clearInterval(locationInterval)
      //           _logout()
      //         }
      //       })
      //   })
      speed = Math.round(position.coords.speed) * 3.6
      //  // console..log('speed ',speed)
      if (max < speed && speed > 100) {
        max = 100
        //  // console..log("max value : " + position.coords.speed)
      }
      if (max == 100 && speed == 0) {
        max = 0
        // if(this.state.isAdmin){
        //   this.clearInterval()
        //   Actions.Ems({ speed: speed, subscription: this.state.subscription })
        // }
        //Linking.openURL('app://PC')
        if (this.state.subscription.subscription)
          if (this.state.subscription.activeEmsService) {
            this.clearInterval()
            Actions.Ems({ speed: speed, subscription: this.state.subscription })
          }
      }
      this.setState({ speed: speed })

    }, (error) => {
     // // console..log(error)

    },
      { enableHighAccuracy: true, distanceFilter: 0 }
    )
  }
  componentDidMount() {
    // if (Platform.OS == 'ios') {
    //   BackgroundTimer.start();
    // }
   // timeoutId = BackgroundTimer.setInterval(() => {
   //   // console..log('app is now background');
    //._getLocation()
   //}, 5000);
   this._getLocation()
    // interval = setInterval(()=>{
    //   console.log("set interval.......->")
    //   this._getLocation()
    // },5000)   
    AppState.addEventListener('change', this._handleAppStateChange);

  }
  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
     // // console..log('App has come to the foreground!');

    }
    this.setState({ appState: nextAppState });
  };
  componentWillUnmount() {
    // clearInterval(locationInterval)
    AppState.removeEventListener('change', this._handleAppStateChange);
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar _subscriber={this.state.subscriber} clearInterval={this.clearInterval} closeDrawer={() => this.closeDrawer()} />}
        >
          <Home openDrawer={() => this.openDrawer()} />
        </Drawer>
        {/* <Text style={{alignSelf:'center',marginBottom:10,fontWeight:'bold'}}> speed : {this.state.speed}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5eaf9',
  },
  drawer: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 3
  },
  loading: {
    flex: 1,
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
