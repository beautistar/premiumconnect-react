import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ActivityIndicator,
  Dimensions,
  FlatList,
  AsyncStorage,
  Modal,
  PermissionsAndroid,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import moment from 'moment'
import firebase from '../../componnents/Firebase';
import ImagePicker from 'react-native-image-crop-picker';
import ImageViewer from 'react-native-image-zoom-viewer';
import Api from '../../componnents/api';
import * as Progress from 'react-native-progress';
import AudioPlayer from 'react-native-play-audio';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Container, Header, Left, Body, Right, Button, Icon, } from 'native-base';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import I18n from 'react-native-i18n';
import { setLayout } from '../../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
import MessageBar from '../../componnents/messageBar'

let audioPath = AudioUtils.DocumentDirectoryPath + '/test.aac';
var sound = require('react-native-sound')
var userId, profile,user_name, userToken, audiofile,timer,updateMember=[]
export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chats: [],
      message: '',
      sender: true,
      recording: false,
      totalDuration: 0,
      recordTime: 0,
      play: -1,
      imageViewer: false,
      imageURL: '',
      loading: true,
      notifypopup: this.props._eventData ? true : false,
      deviceLanguage: 'en',
      appLanguage: 'en',
      allmember:[],
      reportModal:false
    }
    global.groupId = this.props.groupData.id

  }
  componentWillMount() {
    // console..log("user List : ",this.props.userList)
    members=[]
    // console..log("group chat")
    groupData = this.props.groupData
    // console..log("group ", groupData)
    AsyncStorage.getItem('token')
      .then((token) =>{ 
        userToken = token
        // Api.get('/api/smsNotification',token)
        // .then((result)=>{
        //   // console..log('user id ',result) 
        // })
      })
      var members=this.props.members
      AsyncStorage.getItem('userData').then(user=>{
       user=JSON.parse(user)
       if(user){
        // console..log("user id : ",user._id)
        // console..log("member id ",this.props.members)
        // console..log("index of user id : ",members.indexOf(user._id))
        var chatData = firebase.database().ref().child('groupMessage/' + this.props.groupData.id+ '/messages')
        chatData.once('value', snapshot => {
          var  members=snapshot.child('members').val()
          // console..log("member : ",members)
          if(members){
            members.map((item,index)=>{
              if(item.id===user._id){
                updateMember[index]={id:item.id,newMsg:false}
              }
              else{
                updateMember[index]=item
             }
            })
          }
          else{
            var member=this.props.members
            member.map((item,index)=>{
                  updateMember[index]={id:item,newMsg:false}
            })
            // console..log('notfrirebase')
          }
        })
       // members.splice(members.indexOf(user._id), 1)
        // console..log('delete : ',updateMember)
       var chatData = firebase.database().ref().child('groupMessage/' + this.props.groupData.id + '/messages')
       chatData.child('members').set(updateMember)
       }
      
      })
  }
  componentwillUnmount() {
    this.onStopRecord()
    groupId = null
  }
  
  componentDidMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

    if (Platform.OS == 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO)
        .then((result) => {
          // console..log('Permission result', result)
          if (result == 'granted') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
              .then((result) => {
                AsyncStorage.getItem('userData')
                  .then((data) => {
                    // console..log('UserData', JSON.parse(data))
                    var userData = JSON.parse(data)
                    userId = userData._id,
                    user_name=userData.fullName
                    profile = userData.image 
                  })
                var chatData = firebase.database().ref().child('groupMessage/' + this.props.groupData.id + '/messages')
                chatData.on('value', snapshot => {
                  var messages = []
                
                  snapshot.forEach(child => {
                  //  // console..log(child.val())
                    messages.unshift(child.val())
                  })
                  this.setState({ chats: messages, loading: false })
                })
              })
          }
        })
    } else {
      AsyncStorage.getItem('userData')
        .then((data) => {
          // console..log('UserData', JSON.parse(data))
          var userData = JSON.parse(data)
          userId = userData._id
          profile = userData.image
        })
      var chatData = firebase.database().ref().child('groupMessage/' + this.props.groupData.id + '/messages')
      chatData.on('value', snapshot => {
        var messages = []
       
        snapshot.forEach(child => {
          // // console..log(child.val())
          //rrthis.props.
          messages.unshift(child.val())
        })
        this.setState({ chats: messages, loading: false })
      })
    }

    this.audioPrepare()
  }
  audioPrepare() {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac"
    });
    AudioRecorder.onProgress = (data) => {
      // console..log('time : ', data)
      var min = Math.floor(data.currentTime / 60);
      var sec = Math.floor(data.currentTime % 60);
      this.setState({
        recordTime: min + ':' + sec
        //Math.round(data.currentTime)
      })
    };
  }
  async onStartRecord() {

    this.audioPrepare()
    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      // console..error(error);
    }


  }

  async onStopRecord() {
    try {
      const filePath = await AudioRecorder.stopRecording();
      // console..log("file path : ", audioPath)
      this.setState({ filePath: audioPath, recordTime: 0 })
      this._selectAttachment(1)
      if (Platform.OS === 'android') {
        //this._finishRecording(true, filePath);
      }

      return filePath;
    } catch (error) {
      // console..error(error);
    }


  }

  _play(item, index) {
    // console..log('play ',item.file)
    if (this.state.play === index) {

      audio.stop();
      this.setState({ play: -1, totalDuration: 0, recordTime: 0 })
    }
    else {
      sound.setCategory('Playback');
      this.setState({ play: index })
     audio = new sound(item.file,'',(err)=>{
      this.setState({ totalDuration: audio.getDuration() })
        if(err){
           // console..log('Error',err)
           return
        }
        audio.play((res)=>{
          if(res){
            // console..log('Audio played')
          }
          else{
            // console..log('audio errr')
          }
        })
      })

      // AudioPlayer.prepare(item.file, () => {
      //   AudioPlayer.play();
        // audio.getDuration((duration) => {
        //   // console..log('Total Duration', duration);
        //   this.setState({ totalDuration: duration })
        // });
      
      // });
    }
    timer = setInterval(() => {
      audio.getCurrentTime((currentTime) => {
        this.setState({ recordTime: currentTime+1 })
        // console..log('--------CurrentTime',Math.round(this.state.recordTime) ,Math.round(this.state.totalDuration))
        if (Math.round(this.state.recordTime) >= Math.round(this.state.totalDuration)) {
          this.setState({ play: '', totalDuration: 0, recordTime: 0 })
          clearInterval(timer)
        }
      });
    }, 1000);
  }
  componentWillUnmount(){
    clearInterval(timer)
    if(!this.state.play){
      audio.stop()
    }
    
  }
  _report(){
    AsyncStorage.getItem('token')
    .then((token) =>{ 
      Api.postWithToken('/api/reporting',{},token)
      .then((result)=>{
        // console..log('Reporting API ',result)
        if(result.status == "Success"){
          this.setState({ errorMsg: 'Reported Successfully',color: 'green',reportModal:false })
          this.msgBarRef._animateMessage()
        }
      })
    })
    
  }
  _reportModal(){
    return(
      <Modal animationType="fade"
      transparent={true} visible={this.state.reportModal} onRequestClose={()=>{}}>
        <TouchableWithoutFeedback onPress={()=>this.setState({reportModal:false})} >
          <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.5)',justifyContent:'center',alignItems:'center'}}>
            <View style={{backgroundColor:'white',padding:20,width:'80%',borderRadius:5}}>
              <TouchableOpacity onPress={()=>this._report()}>
                <Text style={{color:'black',fontSize:16}}>Report</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  getProfile(id){
    // console..log('sender id : ',id)
    var profile=''
    var name=''
    this.props.userList.forEach(item=>{
      if(item.id==id){
        // console..log('profile : ',item.profile)
        profile=item.profile
        name=item.name
      }
    })
  
      return {name,profile}
  }
  renderContent(item, index) {
    var {name,profile}=this.getProfile(item.senderId)
    if (item.message && item.fileType == '') {
   
      return (
        <View>
          <TouchableOpacity onLongPress={()=>this.setState({reportModal:true})} style={{ alignItems: item.senderId == userId ? 'flex-end' : 'flex-start', width: '80%' }}>
            <Text style={{fontSize:10}}>{name}</Text>
            <View style={{ backgroundColor: item.senderId == userId ? '#ffeedc' : '#f5f5f5', padding: 5, flexDirection: item.senderId == userId ? 'row' : 'row-reverse', borderRadius: 4, alignItems: 'center' }}>
              <Text style={{ fontSize: 14, paddingHorizontal: 8, color: 'black' }}>{item.message}</Text>
              <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={profile?{ uri:profile}:require('../../assets/images/defaultprofile.png')} style={{ height: 40, width: 40, borderRadius: 20 }} />
            </View>
            <View style={{ alignSelf: 'flex-end' }}>
              <Text style={{ fontSize: 10, paddingHorizontal: 4, color: 'black', fontWeight: 'bold' }}>{moment(item.createdAt).format('hh:mm a')}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
    else if (item.fileType == 'image') {
      return (
        <View>
          <TouchableOpacity onPress={() => this.setState({ imageViewer: true, imageURL: item.file })} style={{ alignItems: item.senderId == userId ? 'flex-end' : 'flex-start', width: '80%' }}>
          <Text style={{fontSize:10}}>{name}</Text>
            <View style={{ backgroundColor: item.senderId == userId ? '#ffeedc' : '#f5f5f5', padding: 5, flexDirection: item.senderId == userId ? 'row' : 'row-reverse', borderRadius: 4,  }}>
              <Image source={{ uri: item.file }} style={{ height: 120, width: 180, margin: 5 }} />
              <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={profile?{ uri:profile}:require('../../assets/images/defaultprofile.png')} style={{ height: 40, width: 40, borderRadius: 20 }} />
            </View>
            <View style={{ alignSelf: 'flex-end' }}>
              <Text style={{ fontSize: 10, paddingHorizontal: 4, color: 'black', fontWeight: 'bold' }}>{moment(item.createdAt).format('hh:mm a')}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
    else if (item.fileType == 'audio') {
      var progress = 0
      if (this.state.recordTime != 0) {
        progress = Math.round(this.state.recordTime) / Math.round(this.state.totalDuration)
      }
      return (
        <View>
          <TouchableOpacity style={{ alignItems: item.senderId == userId ? 'flex-end' : 'flex-start', width: '80%' }}>
          <Text style={{fontSize:10}}>{name}</Text>
            <View style={{ backgroundColor: item.senderId == userId ? '#ffeedc' : '#f5f5f5', padding: 5, flexDirection: item.senderId == userId ? 'row' : 'row-reverse', borderRadius: 4, alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 5 }}>
                <Icon name={this.state.play === index ? 'pause' : 'play'} onPress={() => this._play(item, index)} type='AntDesign' style={{ marginRight: -5, zIndex: 1 }} />
                <Progress.Bar color='#ff7a00' progress={this.state.play === index ? progress : 0} width={200} />
             
              </View>
              <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={profile?{ uri:profile}:require('../../assets/images/defaultprofile.png')} style={{ height: 40, width: 40, borderRadius: 20 }} />
            </View>
            <View style={{ alignSelf: 'flex-end' }}>
              <Text style={{ fontSize: 10, paddingHorizontal: 4, color: 'black', fontWeight: 'bold' }}>{moment(item.createdAt).format('hh:mm a')}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
    else if(item.fileType == 'location'){
      return(
        <TouchableOpacity onPress={() => Actions.SharedLocation({ userLocation: { latitude: item.file.latitude, longitude: item.file.longitude,latitudeDelta: 0.0922,longitudeDelta: 0.0421} })} style={{ alignItems: item.senderId == userId ? 'flex-end' : 'flex-start', width: '80%' }}>
           <Text style={{fontSize:10}}>{name}</Text>
        <View style={{ backgroundColor: item.senderId == userId ? '#ffeedc' : '#f5f5f5', padding: 5, flexDirection: item.senderId == userId ? 'row' : 'row-reverse', borderRadius: 4, alignItems: 'center' }}>
              <View style={{marginHorizontal:10}}>
              <MapView
                liteMode
                style={{ height: 90, width: 200 }}
                initialRegion={this.state.cityLatlong}
                region = {{
                  latitude : item.file.latitude,
                  longitude: item.file.longitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
              >
                <Marker coordinate={{ latitude : item.file.latitude, longitude: item.file.longitude }} />
              </MapView>
              <Text style = {{color:'red',fontSize:16}}>{item.message}</Text>
              </View>
              <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={profile?{ uri:profile}:require('../../assets/images/defaultprofile.png')} style={{ height: 40, width: 40, borderRadius: 20 }} />
        </View>
        <View style={{ alignSelf: 'flex-end' }}>
          <Text style={{ fontSize: 10, paddingHorizontal: 4, color: 'black', fontWeight: 'bold' }}>{moment(item.createdAt).format('hh:mm a')}</Text>
        </View>
      </TouchableOpacity>
      )
    }

  }
  
  _renderChat() {
    return (
      <FlatList
        extraData={this.state}
        data={this.state.chats}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingVertical: this.state.paddingBottom }}
        renderItem={({ item, index }) =>
          <View key={index} style={{ paddingHorizontal: 5, marginVertical: 3 }}>
            <View style={{ borderRadius: 8, alignItems: item.senderId == userId ? 'flex-end' : 'flex-start' }}>
              {this.renderContent(item, index)}
            </View>
          </View>
        }
        inverted
        keyExtractor={(item, index) => index.toString()}
      />
    )
  }
  sendNotification(message){
    AsyncStorage.getItem('token')
    .then((token) =>{ 
      Api.postWithToken('/api/getDeviceId', { groupId: this.props.groupData.id},token)
      .then((result)=>{
        // console..log('user id ',result) 
        if(result.status=='Success'){
            Api.postWithToken('/api/pushNotification', {
             deviceids:result.deviceId,
             title:result.title,
             groupId: result.groupId,
             groupType:result.groupType,
             message:message
            },token)
          .then((result)=>{
            // console..log('send Notification ',result) 
          
          })
        }
      })
    })   
  }
  _sendMessage(type, attachment) {
    
   var  members=this.props.members
    AsyncStorage.getItem('userData').then(user=>{
      user=JSON.parse(user)
      if(user){
      // // console..log("user id : ",user._id)
       // console..log("member id ",this.props.members)
      // // console..log("index of user id : ",members.indexOf(user._id))
       updateMember=[]
       members.map((item,index)=>{
         if(item===user._id){
           updateMember[index]={id:item,newMsg:false}
         }
         else{
           updateMember[index]={id:item,newMsg:true}
         }
       })
       //// console..log('delete : ',updateMember)
        var chatData= firebase.database().ref().child('groupMessage/' + this.props.groupData.id + '/messages')
        chatData.child('members').update(updateMember)
      }
     
     })
    var chatRef = firebase.database().ref().child('groupMessage')

    if (type == 'image') {
      var ApiData = { groupId: this.props.groupData.id, message: 'Image' }
      // AsyncStorage.getItem('token')
      //   .then((token) => {
      //     Api.postWithToken('/api/checkNotification', ApiData, token)
      //       .then((result) => // console..log('Push notification api', result))
      //   })
      this.sendNotification('Image')
      chatRef.child(this.props.groupData.id + '/messages').push({
        message: this.state.message,
        senderId: userId,
        //profile: profile ? profile : 'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/GroupImage/defaultprofile.png',
        fileType: 'image',
        file: attachment,
        createdAt: Date.now(),
        id: Date.now()
      })
      this.setState({ message: '' })
    }
    else if (type == 'audio') {
      var ApiData = { groupId: this.props.groupData.id, message: 'Audio' }
      this.sendNotification('Audio')
      // AsyncStorage.getItem('token')
      //   .then((token) => {
      //     Api.postWithToken('/api/checkNotification', ApiData, token)
      //       .then((result) => // console..log('Push notification api', result))
      //   })
      chatRef.child(this.props.groupData.id + '/messages').push({
        message: this.state.message,
        senderId: userId,
        //profile: profile ? profile : 'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/GroupImage/defaultprofile.png',
        fileType: 'audio',
        file: attachment,
        createdAt: Date.now(),
        id: Date.now()
      })
      this.setState({ message: '' })
    }
    else {
      if(this.state.message){
        var ApiData = { groupId: this.props.groupData.id, message: this.state.message }
        // AsyncStorage.getItem('token')
        //   .then((token) => {
        //     Api.postWithToken('/api/checkNotification', ApiData, token)
             
        //   })
          this.sendNotification(this.state.message)
          chatRef.child(this.props.groupData.id + '/messages').push({
          message: this.state.message,
          senderId: userId,
          //profile: profile ? profile : 'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/GroupImage/defaultprofile.png',
          fileType: '',
          file: '',
          createdAt: Date.now(),
          id: Date.now()
        })
        this.setState({ message: '' })
      }
    }
  }
  _selectAttachment(id) {
    if (id == 1) {
      var name = this.state.filePath.split('/')
      var filename = name[name.length - 1]

      var type = filename.split('.')
      var data = new FormData()
      // console..log("token : ", userToken)
      data.append("avatar", {
        uri: 'file://' + this.state.filePath,
        type: 'audio/' + type[type.length - 1],
        name: filename
      })
      // console..log('Data', data)
      fetch(
      
        'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/api/uploadaudio',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            'token': userToken
          },
          body: data
        }).then((result) => result.json())
        .then((res) => {
          // console..log('API---------', res)
          this._sendMessage('audio', res.data)
        })
    } else {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true
      }).then(image => {
     
        var name = image.path.split('/')
        var filename = name[name.length - 1]
        var data = new FormData()
        data.append("avatar", {
          uri: image.path,
          type: image.mime,
          name: filename
        })
        fetch(
          'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/api/uploadaudio',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
              'token': userToken
            },
            body: data
          }).then((result) => result.json())
          .then((res) => {
            // console..log('API---------', res)
            this._sendMessage('image', res.data)
          })
      });
    }
  }
  _imageView() {
    var url = { url: this.state.imageURL }
    // console..log('image Viewer', url)
    return (
      <Modal visible={this.state.imageViewer} onRequestClose={() => this.setState({ imageViewer: false })}      >
        <ImageViewer shown={true} enableSwipeDown swipeDownThreshold={5}
          imageUrls={[url]}
          onSwipeDown={() => this.setState({ imageViewer: false })}
          index={0}>
        </ImageViewer>
      </Modal>
    )
  }
  _eventMessageSend(message) {
    var chatRef = firebase.database().ref().child('groupMessage')
    this.setState({ notifypopup: false })
    chatRef.child(this.props.groupData.id + '/messages').push({
      message: message,
      senderId: userId,
      profile: profile,
      name:user_name,
      fileType: '',
      file: '',
      createdAt: Date.now(),
      id: Date.now()
    })

  }
  _notifypopup() {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <Modal
        visible={this.state.notifypopup}
        onRequestClose={() =>{}}
        transparent={true}
      >
        <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0,0.7)', justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ backgroundColor: 'white', width: '90%', borderRadius: 5, overflow: 'hidden' }}>
            <View style={{ alignItems: 'center', backgroundColor: '#f37c26', padding: 10 }}><Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>{I18n.t('group admin')}</Text></View>
            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', paddingVertical: 2 }}>
                <Text style={{ color: 'black' }}>{I18n.t('title')} : </Text>
                <Text>{this.props._eventData.title} </Text>
              </View>
              <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', paddingVertical: 2 }}>
                <Text style={{ color: 'black' }}>{I18n.t('notification time')} : </Text>
                <Text>{new Date(this.props._eventData.eventTime).toLocaleTimeString()}</Text>
              </View>
              <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', paddingVertical: 2 }}>
                <Text style={{ color: 'black' }}>{I18n.t('notification date')} : </Text>
                <Text>{new Date(this.props._eventData.eventDate).toLocaleDateString()}</Text>
              </View>
              <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', paddingVertical: 2 }}>
                <Text style={{ color: 'black' }}>{I18n.t('message')} : </Text>
                <Text>{this.props._eventData.message}</Text>
              </View>
              <View>
              </View>
            </View>
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={{ height: Dimensions.get('window').height / 4, width: '100%' }}
              region={{
                latitude: this.props._eventData.eventLocation.latitude,
                longitude: this.props._eventData.eventLocation.longitude,
                latitudeDelta: 0.19061583341495592,
                longitudeDelta: 0.28823353350162506,
              }}
              loadingEnabled={true}
            >
              <Marker coordinate={this.props._eventData.eventLocation}
              />
            </MapView>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity
                onPress={() => this._eventMessageSend('Attending')}
                style={{ width: '50%', padding: 10, backgroundColor: '#f37c26', alignItems: 'center', borderRightWidth: 1.5, borderColor: 'white' }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>{I18n.t('attending')}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._eventMessageSend('Not Attending')}
                style={{ width: '50%', padding: 10, backgroundColor: '#f37c26', alignItems: 'center', borderLeftWidth: 1.5, borderColor: 'white' }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>{I18n.t('not attending')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
  
  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='large' color='black' />
        </View>
      )
    }
    else {
      return (
        <View style={styles.container}>
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <Header style={styles.header}>
              <View style={{ width: '10%', alignItems: 'center' }}>
                <TouchableOpacity
                  onPress={() => Actions.pop()}
                >
                  <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
                </TouchableOpacity>
              </View>
              <View style={{ width: '80%', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 20 }}>{this.props.groupData.name}</Text>
              </View>
              <View style={{ width: '10%', alignItems: 'center' }}>
                {/* <TouchableOpacity>
                  <Icon name='dots-three-vertical' type='Entypo' style={{ color: 'white', fontSize: 25 }} />
                </TouchableOpacity> */}
              </View>
            </Header>
            <StatusBar backgroundColor='#020304' barStyle="light-content" />
            <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
            <SafeAreaView style={{ flex: 1 }}>

              {this.props.private ? <View style={{ backgroundColor: '#f37c26', paddingVertical: 2 }}>
                <Text style={{ color: 'white', textAlign: 'center', fontSize: 12 }}>Private Group</Text>
              </View> : null}
              {this._renderChat()}
              {this.props.private ?
                <View style={{ borderTopWidth: 1, borderColor: '#c9c7c6', flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                  {this.state.recording ?
                    <View style={{ width: '65%', flexDirection: 'row', alignItems: "center", paddingHorizontal: 10 }}>
                      <Image source={require('../../assets/images/Audiomicrophone_icon.png')} style={{ height: 25, width: 15 }} />
                      <Text style={{ marginLeft: 10 }}>{this.state.recordTime} Recording</Text>
                    </View>
                    : <View style={{ width: '60%' }}><TextInput
                      style={{ paddingLeft: 10 }}
                      placeholder='Type a Message'
                      multiline
                      onChangeText={(text) => this.setState({ message: text })}
                      value={this.state.message}
                    /></View>}
                  <View style={{ flexDirection: 'row', width: '40%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => this.setState({ recording: !this.state.recording }, () => this.state.recording ? this.onStartRecord() : this.onStopRecord())} ><Image source={require('../../assets/images/Audio_btn.png')} style={{ width: 40, height: 40, tintColor: this.state.recording ? '#f37c26' : null }} /></TouchableOpacity>
                    <TouchableOpacity onPress={() => this._selectAttachment(2)} ><Image source={require('../../assets/images/Image_btn.png')} style={{ width: 40, height: 40 }} /></TouchableOpacity>
                    <TouchableOpacity onPress={() => this._sendMessage()} ><Image source={require('../../assets/images/Chat_btn.png')} style={{ width: 40, height: 40 }} /></TouchableOpacity>
                  </View>
                </View>
                :
                <View style={{ borderTopWidth: 1, borderColor: '#c9c7c6', flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                  <TextInput
                    style={{ width: '85%', paddingLeft: 10 }}
                    placeholder='Type a Message'
                    onChangeText={(text) => this.setState({ message: text })}
                    value={this.state.message}
                  />
                  <TouchableOpacity onPress={() => this._sendMessage()} style={{ width: '15%', alignItems: 'center' }}><Image source={require('../../assets/images/Chat_btn.png')} style={{ width: 40, height: 40 }} /></TouchableOpacity>
                </View>}
              {this._imageView()}
              {this.props._eventData ? this._notifypopup() : null}
              {this._reportModal()}
            </SafeAreaView>
          </KeyboardAvoidingView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
});
