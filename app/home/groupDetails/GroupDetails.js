
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Dimensions,
  FlatList,
  AsyncStorage,
  ImageBackground,
  SafeAreaView,
  ActivityIndicator,
  KeyboardAvoidingView,
  Keyboard,
  Alert
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, AnimatedRegion } from 'react-native-maps';
import Api from '../../componnents/api';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
import { Container, Header, Left, Body, Right, Button, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import Moment from 'moment'
import OptionsMenu from "react-native-options-menu";
import Modal from "react-native-modal";
import DateTimePicker from 'react-native-modal-datetime-picker';
import MessageBar from '../../componnents/messageBar'
import firebase from '../../componnents/Firebase';
const myIcon = (<Icon name='dots-three-vertical' type='Entypo' style={{ color: 'white', fontSize: 22, padding: 10 }} />)
const screen = Dimensions.get('window');

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE = 21.1926;
const LONGITUDE = 72.7997;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class GroupDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groupList: [],
      markerList: [{
        latitude: 21.170240,
        longitude: 72.831062,
      }],
      adminData: [],
      deviceLanguage: 'en',
      appLanguage: 'en',
      addMemberModal: false,
      memberList: [],
      memberListID: [],
      alreadyAddedMember: [],
      cityName: this.props.groupData.cityName,
      cityLatlong: '',
      removePermisson: false,
      addAdminModal: false,
      subAdminData: {},
      groupId: '',
      isAdmin: false,
      ems: false,
      indicator: true,
      GroupNotificationMessageModal: false,
      isDateTimePickerVisible: false,
      eventDate: new Date(),
      pinLocation: '',
      EventMessage: '',
      eventPin: '',
      eventTime: new Date(),
      mode: '',
      eventTitle: '',
      latitudeDelta: 0.19061583341495592,
      longitudeDelta: 0.28823353350162506,
    }
    this.addMembers = this.addMembers.bind(this)
    this.removeGroup = this.removeGroup.bind(this);
    this.leftGroup = this.leftGroup.bind(this);
    this.onBackEms = this.onBackEms.bind(this);
    this.GroupNotificationMessage = this.GroupNotificationMessage.bind(this)
    this.usermarker = []
  }
  onBackEms() {
    this.setState({ ems: false })
  }
  _showDateTimePicker = (pickerMode) => this.setState({ isDateTimePickerVisible: true, mode: pickerMode });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false, mode: '' });

  _handleDatePicked = (date) => {
    // console..log('A date has been picked: ', date);
    if (this.state.mode == 'date') {
      this.setState({ eventDate: date })
    }
    else {
      this.setState({ eventTime: date })
    }
    this._hideDateTimePicker();
  };
  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal()
      .then((place) => {
        // console..log(place);
      })
      .catch(error => console.log(error.message));  // error is a Javascript Error object
  }

  addMembers() {
    this.setState({ addMemberModal: true })
  }
  getAllUser() {
    AsyncStorage.getItem('token').then(token => {
      memberdata = []
      Api.postWithToken('/api/users', null, token).then(res => {
        if (res.status == 'Success') {
          // console..log("Total member ", res.data.length)
          res.data.map((member, index) => {
            memberdata[index] = {
              id: member._id,
              name: member.fullName,
              profile: member.image
            }
          })
          // console..log("data -------: ", memberdata)
          this.setState({ memberList: memberdata })
        }
      })
    })
  }
  getGroupData() {
    AsyncStorage.getItem('userData').then(user => {
      var user = JSON.parse(user)
      AsyncStorage.getItem('token').then(token => {
        memberdata = []
        data = []
        addedMember = []
        Api.postWithToken('/api/getgroup', { groupId: this.props.groupData.id }, token).then(res => {
          // console..log("groups Details---- : ", res)
          // // console..log("user : ", user)
          if (res.data.status == 'Success') {
            this.setState({ eventPin: res.data.event, })
            res.data.data.map((member, index) => {

              addedMember.push(member.userData._id)
              if (index == 0) {
                var lat = Number(member.userData.location.latitude)
                var long = Number(member.userData.location.longitude)
                this.setState({
                  pinLocation: this.state.pinLocation ? this.state.pinLocation : {
                          latitude: lat,
                          longitude: long,
                          latitudeDelta: this.state.latitudeDelta,
                          longitudeDelta: this.state.longitudeDelta,
                  },
                  removePermisson: member.userData._id == user._id,
                })
                
              }
              if(member.userData._id == user._id && member.membersList.blockUser ){
                clearInterval(interval)
                Alert.alert(
                  this.props.groupData.name,
                  'You have been blocked by admin',
                  [
                    {text: 'OK', onPress: () => Actions.reset('Home')},
                  ],
                  {cancelable: false},
                );
              }
              // if (member.membersList.isSuperAdmin) {
              //   var lat = Number(member.userData.location.latitude)
              //   var long = Number(member.userData.location.longitude)
              //   this.setState({
              //     adminData: [{
              //       id: member.userData._id,
              //       name: member.userData.fullName,
              //       last_seen: member.userData.lastseen,
              //       currently: member.userData.online,
              //       profile: member.userData.image,
              //       status: member.userData.status,
              //       location: new AnimatedRegion({
              //         latitude: lat,
              //         longitude: long
              //       }),
              //     }],
              //     pinLocation: this.state.pinLocation ? this.state.pinLocation : {
              //       latitude: lat,
              //       longitude: long
              //     },
              //     removePermisson: member.userData._id == user._id,
              //     cityLatlong: {
              //       latitude: lat,
              //       longitude: long,
              //       latitudeDelta: 0.19061583341495592,
              //       longitudeDelta: 0.28823353350162506,
              //     }
              //   })
              //   if (Platform.OS === 'android') {
              //     if (this.adminmarker) {
              //       this.adminmarker._component.animateMarkerToCoordinate({
              //         latitude: lat,
              //         longitude: long
              //       }, 500);
              //     }
              //   } else {
              //     this.state.adminData[0].location.timing({
              //       latitude: lat,
              //       longitude: long
              //     }).start();
              //   }
              // }
              // else {
                var lat = Number(member.userData.location.latitude)
                var long = Number(member.userData.location.longitude)
                data.push({
                  id: member.userData._id,
                  name: member.userData.fullName,
                  location: new AnimatedRegion({
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: this.state.latitudeDelta,
                    longitudeDelta: this.state.longitudeDelta,
                  }),
                  isSuperAdmin:member.membersList.isSuperAdmin,
                  isAdmin: member.membersList.isAdmin,
                  isBlocked : member.membersList.blockUser,
                  last_seen: member.userData.lastseen,
                  currently: member.userData.online,
                  profile: member.userData.image,
                  status: member.userData.status
                })
                if (Platform.OS === 'android') {
                  if (this.usermarker[member.userData._id]) {
                    this.usermarker[member.userData._id]._component.animateMarkerToCoordinate({
                      latitude: lat,
                      longitude: long
                    }, 500);
                  }
                } else {
                  this.state.groupList.map(item => {
                    item.location.timing({
                      latitude: lat,
                      longitude: long
                    }).start();
                  })

                }
              // }
              if (member.userData._id == user._id) {
                var lat = Number(member.userData.location.latitude)
                var long = Number(member.userData.location.longitude)
                this.setState({
                  isAdmin: member.membersList.isAdmin,
                  isSuperAdmin: member.membersList.isSuperAdmin,
                  cityLatlong: {
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: this.state.latitudeDelta,
                    longitudeDelta: this.state.longitudeDelta,
                  },
                })
              }
            })
          }
          this.setState({ groupList: data, alreadyAddedMember: addedMember, indicator: false })
          // console..log("user data : ",data)
        })
      })
    })
  }
  componentWillMount() {

    this.setState({ groupId: this.props.groupData.id })
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
    this.getGroupData()
    this.getAllUser()

  }
  componentDidMount() {
    interval = setInterval(() => {
       this.getGroupData()
    }, 5000)
  }
  componentWillUnmount() {
    clearInterval(interval)
  }
  addMember(memberId) {

    if (this.state.memberListID.includes(memberId)) {
      index = this.state.memberListID.indexOf(memberId)
      this.state.memberListID.splice(index, 1)
      this.setState({ add: false })
      AsyncStorage.getItem('token').then(token => {
        Api.delete('/api/removeuser', {
          groupId: this.props.groupData.id,
          memberId: memberId
        }, token).then(res => {
          // console..log("member delete successfully : ", res)
        })
      })
    }
    else {
      this.state.memberListID.push(memberId)
      this.setState({ add: true })
      AsyncStorage.getItem('token').then(token => {

        Api.postWithToken('/api/adduser', {
          groupId: this.props.groupData.id,
          memberId: memberId
        }, token).then(res => {
          // console..log("member added successfully : ", res)
          if (res.status == 'Success') {
          }
        })
      })
    }
  }
  renaderListItem(item, index, admin) {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)

    return (
      <TouchableOpacity
        disabled={admin ? !admin : !this.state.removePermisson ? true : !this.state.isAdmin ? true : this.props.groupData.creater == 'Developer'}
        onLongPress={() => this.setState({
          addAdminModal: true,
          subAdminData: {
            memberid: item.id,
            isAdmin: item.isAdmin,
            isBlocked : item.isBlocked
          }
        })}
        style={[styles.listMainView, { paddingHorizontal: admin ? 10 : 20, flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8' }]}>
        <View style={{ width: '20%' }}>
          <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={item.profile ? { uri: item.profile } : require('../../assets/images/defaultprofile.png')} style={[styles.logo, item.profile ? null : { tintColor: '#ff7a00' }]} />
        </View>

        <View style={{ width: '40%', marginLeft: 5 }}>
          <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
          <Text style={{ color: item.currently ? '#ff7a00' : '#aca7a2', fontSize: 12, paddingRight: chnageLayout ? 0 : 10 }}>
            {item.currently ? I18n.t('online') : I18n.t('last seen')+' '+Moment(item.last_seen).fromNow()} 
          </Text>
        </View>
        <View style={{ width: '40%', alignItems: 'flex-end' }}>
          {admin ? <Text style={{ color: '#ff7a00', fontSize: 10, marginRight: 5, fontWeight: 'bold' }}>{I18n.t("group") + ' ' + I18n.t("admin")}</Text> : null}
          <View>
            {
              !admin ? this.state.removePermisson || this.state.isAdmin ? <TouchableOpacity
                onPress={() => this.removeMember(item.id)}
                style={{ alignSelf: 'center' }}>
                <Text style={{ color: '#ff7a00', fontSize: 15, fontWeight: 'bold', marginBottom: 5 }}>{I18n.t('remove')}</Text>
              </TouchableOpacity> : null : null
            }
            {
              item.isAdmin ?
                <Text style={{ color: '#ff7a00', fontSize: 10, fontWeight: 'bold', textAlign: 'center' }}>{I18n.t("admin")}</Text> : null}
            <View style={{ backgroundColor: item.currently ? '#ff7a00' : '#dcd9d7', padding: 1, paddingHorizontal: 5, borderRadius: 50, alignItems: 'center' }}>
              <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{item.currently ? I18n.t('currently on') : I18n.t('currently off')} </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
  renaderListItemModal(item, index) {
    let { memberList } = this.state

    var add = this.state.memberListID.includes(item.id)
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    if (!this.state.alreadyAddedMember.includes(item.id))
      return (
        <View style={[styles.listItemview, { backgroundColor: 'white', flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center' }]}>
          <View style={[styles.col1, { paddingLeft: chnageLayout ? 15 : 0, backgroundColor: 'white', width: '30%' }]}>
            <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={item.profile ? { uri: item.profile } : require('../../assets/images/defaultprofile.png')} style={[styles.logo2, !item.profile ? { tintColor: '#ff7a00' } : null]} />
          </View>
          <View style={[styles.col2, { backgroundColor: 'white', width: '45%' }]}>
            <Text style={{ color: 'black', fontSize: 16, textAlign: chnageLayout ? 'left' : 'right', marginRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
          </View>
          <View style={styles.col3}>
            <TouchableOpacity
              onPress={() => this.addMember(item.id)}
              style={[{ backgroundColor: add ? 'white' : '#ff7a00' }, styles.addbutton]}>
              <Text style={[{ color: add ? '#ff7a00' : 'white' }, styles.addbtntxt]}>{I18n.t('add')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
  }

  removeMember(memberId, added) {

    AsyncStorage.getItem('token').then(token => {

      Api.delete('/api/removeuser', {
        groupId: this.props.groupData.id,
        memberId: memberId
      }, token).then(res => {

        this.getGroupData()
      })
    })
  }
  added() {
    this.setState({ addMemberModal: false, memberListID: [] })
    this.getGroupData()
  }
  givePerimssion(isAdmin) {
    AsyncStorage.getItem('token').then(token => {
      Api.put('/api/updateuser', {
        groupId: this.props.groupData.id,
        memberId: this.state.subAdminData.memberid,
        isAdmin: isAdmin
      }
        , token).then(res => {
          if (res.data.status == 'Success') {
            this.setState({ addAdminModal: false })
            this.getGroupData()
          }
        })
    })
  }
  block(block){
    // console..log('Group Id',this.props.groupData.id)
    // console..log('block member id',this.state.subAdminData.memberid)
    AsyncStorage.getItem('token').then(token => {
      Api.put('/api/blockUser', {
        groupId: this.props.groupData.id,
        memberId: this.state.subAdminData.memberid,
        blockUser:block
      }
        , token).then(res => {
          // console..log('Block Use Api ',res)
          if (res.status == 'Success') {
            this.setState({ addAdminModal: false })
            this.getGroupData()
          }
        })
    })
  }
  addMemberModalView() {
    return (
      <Modal
        isVisible={this.state.addMemberModal}
        animationIn='slideInUp'
        animationOut='slideOutDown'
        onBackdropPress={() => this.setState({ addMemberModal: false, memberListID: [] })}
        onBackButtonPress={() => this.setState({ addMemberModal: false, memberListID: [] })}
      >
        <View style={{ flex: 1, backgroundColor: 'white', borderRadius: 5, padding: 10 }}>
          <View style={{ backgroundColor: '#ff7a00', padding: 10, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.state.memberListID.length} Member Added</Text>
            <TouchableOpacity
              onPress={() => this.added()}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.state.memberListID.length > 0 ? 'Done' : 'Cancel'}</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            extraData={this.state}
            data={this.state.memberList}
            renderItem={({ item, index }) =>
              this.state.memberId != item.id ? this.renaderListItemModal(item, index) : null}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </Modal>
    )
  }
  cancle() { }
  leftGroup() {
    AsyncStorage.getItem('token').then(token => {
      // console..log("token : ", this.state.groupId)
      Api.delete('/api/leftGroup', { groupId: this.state.groupId }, token).then(res => {
        // console..log("member delete successfully : ", res)
        Actions.reset('Home')
      })
    })
  }
  removeGroup() {
    AsyncStorage.getItem('token').then(token => {
      // console..log("token : ", this.state.groupId)
      Api.delete('/api/deletegroup', { groupId: this.state.groupId }, token).then(res => {
        // console..log("member delete successfully : ", res)
        firebase.database().ref().child('groupMessage/' + this.state.groupId+ '/messages').remove()
        Actions.reset('Home')
      })
    })
  }
  addAdminModal() {
    return (
      <Modal
        isVisible={this.state.addAdminModal}
        animationIn='slideInUp'
        animationOut='slideOutDown'
        onBackdropPress={() => this.setState({ addAdminModal: false })}
        onBackButtonPress={() => this.setState({ addAdminModal: false })}
      >
        <View style={{ backgroundColor: 'white', borderRadius: 5, padding: 5 }}>
          {!this.state.subAdminData.isAdmin ? <TouchableOpacity
            onPress={() => this.givePerimssion(true)}
            style={{ padding: 10,marginVertical:2 }}>
            <Text>{I18n.t('give admin rights')}</Text>
          </TouchableOpacity> :
            <TouchableOpacity
              onPress={() => this.givePerimssion(false)}
              style={{ padding: 10,marginVertical:2 }}>
              <Text>{I18n.t('remove admin rights')}</Text>
            </TouchableOpacity>
          }{
            !this.state.subAdminData.isBlocked ? 
            <TouchableOpacity
            onPress={() => this.block(true)}
            style={{ padding: 10,marginVertical:2 }}>
            <Text>{I18n.t('block')}</Text>
          </TouchableOpacity>
          : 
          <TouchableOpacity
          onPress={() => this.block(false)}
          style={{ padding: 10,marginVertical:2 }}>
          <Text>{I18n.t('unblock')}</Text>
        </TouchableOpacity>
          }
         
        </View>
      </Modal>
    )
  }
  GroupNotificationMessage() {
    this.setState({ GroupNotificationMessageModal: true })
  }
  saveEvent() {
    if (this.state.EventMessage && this.state.eventTitle) {
      AsyncStorage.getItem('token').then(token => {
        // console..log("token : ", this.state.groupId)
        Api.put('/api/groupNotificationMessage',
          {
            groupId: this.state.groupId,
            eventDate: this.state.eventDate,
            message: this.state.EventMessage,
            eventLocation: this.state.pinLocation,
            eventTime: this.state.eventTime,
            title: this.state.eventTitle
          }, token).then(res => {
            // console..log("save event successfully : ", res)
            if (res.status == 'Success') {
              this.setState({ GroupNotificationMessageModal: false })
            }
          })
      })
    }
    else {
      this.msgBarRef._animateMessage()
    }
  }
  GroupNotificationMessageAlert() {
    //// console..log("date : ", this.state.date)
    //// console..log("user latlong : ", this.state.pinLocation)
    return (
      <Modal
        isVisible={this.state.GroupNotificationMessageModal}
        animationIn='slideInUp'
        animationOut='slideOutDown'
        onBackdropPress={() => this.setState({ GroupNotificationMessageModal: false })}
        onBackButtonPress={() => this.setState({ GroupNotificationMessageModal: false })}
       >
        <KeyboardAvoidingView behavior='position'>
          <View style={{ borderRadius: 5, overflow: 'hidden' }}>
            <MessageBar ref={(ref) => this.msgBarRef = ref} error='Please Enter Message' />
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={{ height: Platform.OS == 'ios' ? Dimensions.get('window').height / 2.5 : Dimensions.get('window').height / 1.8, width: '100%' }}
              initialRegion={this.state.cityLatlong}
              loadingEnabled={true}
            >
              {this.state.pinLocation ? <Marker coordinate={this.state.pinLocation}
                draggable={true}
                onDragEnd={(e) => this.setState({ pinLocation: e.nativeEvent.coordinate })}
              >
              </Marker> : null}
            </MapView>
            <View style={{ paddingVertical: 20, backgroundColor: 'white' }}>
              <View style={{ flexDirection: 'row', width: '90%', paddingVertical: Platform.OS == 'ios' ? 10 : 5, justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '20%', alignItems: 'center' }}><Icon name='title' type='MaterialIcons' style={{ fontSize: 30, color: '#f37c26' }} /></View>
                <TextInput placeholder='Title' style={{ width: '80%', borderBottomWidth: 1, padding: Platform.OS == 'ios' ? 10 : 0 }}
                  onSubmitEditing={() => Keyboard.dismiss()}
                  onChangeText={(text) => this.setState({ eventTitle: text })} />
              </View>
              <TouchableOpacity onPress={() => this._showDateTimePicker()} style={{ width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                <View style={{ width: '20%', alignItems: 'center' }}><Icon name='md-calendar' style={{ color: '#f37c26' }} /></View>
                <View style={{ borderBottomWidth: 1, width: '80%', paddingVertical: 5 }}>
                  <Text style={{ color: 'black', marginLeft: 10 }}>{this.state.eventDate.toLocaleDateString()}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._showDateTimePicker('time')} style={{ width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 5 }}>
                <View style={{ width: '20%', alignItems: 'center' }}><Icon name='access-time' type='MaterialIcons' style={{ color: '#f37c26' }} /></View>
                <View style={{ borderBottomWidth: 1, width: '80%', paddingVertical: 5 }}>
                  <Text style={{ color: 'black', marginLeft: 10 }}>{Moment(this.state.eventTime).format('hh:mm a')}</Text>
                </View>
              </TouchableOpacity>
              <View style={{ flexDirection: 'row', width: '90%', paddingVertical: Platform.OS == 'ios' ? 10 : 5, justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ width: '20%', alignItems: 'center' }}><Icon name='message' type='Entypo' style={{ fontSize: 30, color: '#f37c26' }} /></View>
                <TextInput placeholder='Message' style={{ width: '80%', borderBottomWidth: 1, padding: Platform.OS == 'ios' ? 10 : 0 }}
                  onSubmitEditing={() => Keyboard.dismiss()}
                  onChangeText={(text) => this.setState({ EventMessage: text })} />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this.saveEvent()}
              style={{ alignItems: 'center', backgroundColor: '#f37c26', padding: 10, width: '100%' }}>
              <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>{I18n.t('send')}</Text>
            </TouchableOpacity>
            <DateTimePicker
              mode={this.state.mode}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
          </View>
        </KeyboardAvoidingView>
      </Modal>
    )
  }

  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    let { adminData } = this.state
    if (this.state.indicator) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color='#000000' size='large' />
        </View>
      )
    }
    else
      return (
        <View style={styles.container}>
          <Header private={this.props.private} style={styles.header}>
            <View style={{ width: '10%', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => Actions.reset("Home")}>
                <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
              </TouchableOpacity>
            </View>
            <View style={{ width: '80%', alignItems: 'center' }}>
              <Text style={{ color: 'white', fontSize: 20 }}>{this.props.groupData.name}</Text>
            </View>
            <View style={{ width: '10%', alignItems: 'center', flexDirection: 'row', justifyContent: "space-around" }}>
              {this.props.groupData.creater == 'Developer' ? null :
                this.state.removePermisson ?
                  <OptionsMenu
                    customButton={myIcon}
                    destructiveIndex={2}
                    options={Platform.OS == 'ios' ? [I18n.t("group notification message"), I18n.t("add member"), I18n.t("remove group"), I18n.t("cancel")] : [I18n.t("group notification message"), I18n.t("add member"), I18n.t("remove group")]}
                    actions={Platform.OS == 'ios' ? [this.GroupNotificationMessage, this.addMembers, this.removeGroup, this.cancle] : [this.GroupNotificationMessage, this.addMembers, this.removeGroup]} /> :
                  this.state.isAdmin ?
                    <OptionsMenu
                      customButton={myIcon}
                      destructiveIndex={1}
                      options={Platform.OS == 'ios' ? [I18n.t("add member"), I18n.t("left group"), I18n.t("cancel")] : [I18n.t("add member"), I18n.t("left group")]}
                      actions={Platform.OS == 'ios' ? [this.addMembers, this.leftGroup, this.cancle] : [this.addMembers, this.leftGroup]} /> :
                    <OptionsMenu
                      customButton={myIcon}
                      destructiveIndex={0}
                      options={Platform.OS == 'ios' ? [I18n.t("left group"), I18n.t("cancel")] : [I18n.t("left group")]}
                      actions={Platform.OS == 'ios' ? [this.leftGroup, this.cancle] : [this.leftGroup]} />
              }
            </View>
          </Header>
          <StatusBar backgroundColor='#020304' barStyle="light-content" />
          <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              {this.props.private ? <View style={styles.privateGroupView}>
                <Text style={styles.privateGroupText}>{I18n.t('private') + ' ' + I18n.t('group')}</Text>
              </View> : null}
              {this.state.cityLatlong ? <MapView
               // showsUserLocation={ true }
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={{ height: Dimensions.get('window').height / 2.5, width: '100%' }}
                initialRegion={this.state.cityLatlong}
                region={this.state.cityLatlong}
                //loadingEnabled={true}
                onRegionChangeComplete={(region) => {
                  // // console..log('Region Change',region)
                  this.setState({
                    cityLatlong:{
                      latitude: this.state.cityLatlong.latitude,
                      longitude: this.state.cityLatlong.longitude,
                      latitudeDelta: region.latitudeDelta,
                      longitudeDelta: region.longitudeDelta,
                    },
                     latitudeDelta: region.latitudeDelta,
                     longitudeDelta: region.longitudeDelta,
                  })
                }
                }
              >
                {
                  this.state.eventPin.eventLocation ?
                    <Marker coordinate={this.state.eventPin.eventLocation}
                      title={"Event Date : " + Moment(this.state.eventPin.eventDate).format('MMMM Do YYYY')}
                      description={this.state.eventPin.message}>
                    </Marker> : null
                }
                {/* {this.state.adminData.length > 0 ? this.state.adminData[0].currently ?
                  <Marker.Animated
                    ref={marker => { this.adminmarker = marker; }}
                    coordinate={this.state.adminData[0].location}>
                    <View>
                      <ImageBackground source={
                        this.state.adminData[0].status == 'red' ? require('../../assets/images/red.png')
                          : this.state.adminData[0].status == 'green' ? require('../../assets/images/green.png')
                            : require('../../assets/images/orange.png')} style={{ height: 65, width: 65 }} >
                        {this.state.adminData[0].profile ? <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={{ uri: this.state.adminData[0].profile }} style={{ height: 32, width: 32, borderRadius: 16, left: 16.5, top: 6, position: 'absolute' }} />
                          : <Image source={require('../../assets/images/defaultprofile.png')} style={{ height: 32, width: 32, borderRadius: 16, left: 16.5, top: 6, position: 'absolute', tintColor: '#ff7a00' }} />}
                      </ImageBackground>
                    </View>
                  </Marker.Animated> : null : null} */}
                {
                  this.state.groupList.length > 0 ? this.state.groupList.map(coordinate =>
                    coordinate.currently ? <Marker.Animated
                      ref={marker => { this.usermarker[coordinate.id] = marker; }}
                      coordinate={coordinate.location}>
                      <View>
                        <ImageBackground source={
                          coordinate.status == 'red' ? require('../../assets/images/red.png')
                            : coordinate.status == 'green' ? require('../../assets/images/green.png')
                              : require('../../assets/images/orange.png')} style={{ height: 65, width: 65 }} >
                          {coordinate.profile ? <Image defaultSource={require('../../assets/images/defaultprofile.png')} source={{ uri: coordinate.profile }} style={{ height: 32, width: 32, borderRadius: 16, left: 16.5, top: 6, position: 'absolute' }} />
                            : <Image source={require('../../assets/images/defaultprofile.png')} style={{ height: 32, width: 32, borderRadius: 16, left: 16.5, top: 6, position: 'absolute', tintColor: '#ff7a00' }} />}
                        </ImageBackground>
                      </View>
                    </Marker.Animated> : null
                  ) : null
                }
              </MapView> : null}
              <View style={styles.groupAdminMainView}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={styles.col1}>
                    <Image source={require('../../assets/images/Groupmember_icon.png')} style={{ width: 20, height: 20 }} />
                    <Text style={styles.colTitle}>{this.state.groupList.length} {I18n.t('group members')}</Text>
                  </View>
                  <View style={styles.col2}>
                    <Image source={require('../../assets/images/Location_icon.png')} style={{ width: 15, height: 20 }} />
                    <Text style={styles.colTitle}>{this.state.cityName}</Text>
                  </View>
                </View>
                { this.props.groupData.creater == 'User' ? this.renaderListItem(this.state.groupList[0], 0, true) : null }
              </View>
              <FlatList
                contentContainerStyle={{ paddingBottom: '20%' }}
                extraData={this.state}
                data={this.state.groupList}
                renderItem={({ item, index }) => this.props.groupData.creater == 'User' ? index > 0 ? this.renaderListItem(item, index ) : null : this.renaderListItem(item,index)}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={styles.chatButtonMainView}>
              <TouchableOpacity
                onPress={() => {  
                  clearInterval(interval)
                  Actions.Chat({userList:this.state.groupList, private: this.props.private, groupData: this.props.groupData,members:this.state.alreadyAddedMember }) 
                }}
                style={styles.chatButton}>
                <Text style={{ color: 'white' }}>{I18n.t('chat')}</Text>
              </TouchableOpacity>
            </View>
            {this.addMemberModalView()}
            {this.addAdminModal()}
            {this.GroupNotificationMessageAlert()}
          </SafeAreaView>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  logo2: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  privateGroupView: {
    backgroundColor: '#f37c26',
    paddingVertical: 2
  },
  privateGroupText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 12
  },
  mapImage: {
    width: '100%',
    height: Dimensions.get("screen").height / 3.5
  },
  groupAdminMainView: {
    width: '90%',
    alignSelf: 'center',
    marginTop: -20,
    borderRadius: 5,
    overflow: 'hidden',
    elevation: 2
  },
  col1: {
    width: '60%',
    backgroundColor: '#f37c26',
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  col2: {
    width: '40%',
    backgroundColor: '#020304',
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  colTitle: {
    color: 'white',
    marginLeft: 5,
    fontSize: 12,
    width: '80%',
  },
  listMainView: {
    height: Dimensions.get('screen').height / 3,
  },
  chatButtonMainView: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: 'rgba(350,350,350,0.6)',
    padding: 10
  },
  chatButton: {
    width: '20%',
    borderRadius: 50,
    backgroundColor: '#f37c26',
    padding: 8,
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 8,
    marginBottom: 10
  },
  listMainView: {
    padding: 10, alignItems: 'center', width: '100%'
  },
  addbutton: {
    padding: 2,
    width: '60%',
    borderWidth: 1,
    borderColor: '#ff8a2a',
    borderRadius: 50,
    alignItems: 'center'
  },
  addbtntxt: {
    fontSize: 12,
    fontWeight: 'bold'
  },
  col3: {
    width: '25%',
    alignItems: 'center'
  },

});
