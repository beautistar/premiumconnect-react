import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  Alert,
  Linking,
  TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../../componnents/Layout'
import { Icon } from 'native-base';
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
import Api from '../../componnents/api'
import Geocoder from 'react-native-geocoding';
import RNSettings from 'react-native-settings'
import * as RNIap from 'react-native-iap';
var cityName, userLocation, location;

export default class CreateGroupStep4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberList: [],
      searched:[],
      memberListID: [],
      addedMemberList: [],
      deviceLanguage: 'en',
      appLanguage: 'en',
      memberId: '',
      isAdmin:false,
    }
  }
  _openLocationSetting() {
    if (Platform.OS == 'android') {
      RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
        then((result) => {
          if (result === RNSettings.ENABLED) {
            // console..log('location is enabled')
          }
        })
    }
    else {
      Linking.openURL('app-settings:')
        .then((result) =>
           console.log('location', result))
    }
  }
  _getLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      // console..log('Current Location', position.coords.latitude, position.coords.longitude)
      Geocoder.init("AIzaSyCJDz_lrzpIdEqfkW4TH1UHzc_55Z-lxy0"); // use a valid API key
      userLocation = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }

      Geocoder.from({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      })
        .then(json => {
          // console..log(json.results[json.results.length - 3].address_components[0].long_name);
          cityName = json.results[json.results.length - 3].address_components[0].long_name
          Geocoder.from(cityName)
            .then(ltn => {
              // console..log(ltn.results[0].geometry.location)
              location = ltn.results[0].geometry.location
            })

        })
        .catch(error => console.warn(error));



    }, (error) => {
      // console..log(error)
      Alert.alert(
        'Location Service Disable',
        'Please enable location service to use this App',
        [
          { text: 'OK', onPress: () => { this._openLocationSetting() } },
        ],
        { cancelable: false }
      )
    },
      { enableHighAccuracy: false, timeout: 20000 }
    )
  }
  componentWillMount() {
    AsyncStorage.getItem('isAdmin').then((isadmin)=>{this.setState({isAdmin:JSON.parse(isadmin)})})
    this._getLocation()
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
    AsyncStorage.getItem('userData').then(user => {
      this.setState({ memberId: JSON.parse(user)._id })
    })
    // console..log("image : ", this.props)
  }
  componentDidMount() {
    AsyncStorage.getItem('token').then(token => {
      Api.postWithToken('/api/users', null, token).then(res => {
        data = []
        if (res.status == 'Success') {
          res.data.map((member, index) => {
            data[index] = {
              id: member._id,
              name: member.fullName,
              profile: member.image
            }
          })
        }
        this.setState({ memberList: data })
      })
    })
  }
  addMember(memberId) {
    var plan = this.props.selectPlan
    if(this.props.groupType == 'private'  && this.state.isAdmin){
      if (this.state.memberListID.includes(memberId)) {
        index = this.state.memberListID.indexOf(memberId)
        this.state.memberListID.splice(index, 1)
        this.setState({ add: false })
      }
      else {
        this.state.memberListID.push(memberId)
        this.setState({ add: true })
      }
    }
   else if (this.props.groupType == 'public' || this.state.memberListID.length < plan.maxuser) {
      if (this.state.memberListID.includes(memberId)) {
        index = this.state.memberListID.indexOf(memberId)
        this.state.memberListID.splice(index, 1)
        this.setState({ add: false })
      }
      else {
        this.state.memberListID.push(memberId)
        this.setState({ add: true })
      }
    }
    else {
      if (this.state.memberListID.includes(memberId)) {
        index = this.state.memberListID.indexOf(memberId)
        this.state.memberListID.splice(index, 1)
        this.setState({ add: false })
      }
    }
    // console..log("data : ", this.state.addedMemberList)
  }
  _buyProduct() {
    // if (this.props.groupType == 'public') {
    //   this.createGroup()
    // }
     if(this.state.isAdmin){
      this.createGroup()
    }
    else {
      var plan = this.props.selectPlan
      RNIap.buyProduct(plan.productId).then(res => {
        // console..log("result : ", res)
        RNIap.consumePurchase(res.purchaseToken)
        this.createGroup(res)

      }).catch(e =>
         console.log('error : ', e))
    }
  }
  createGroup(receipt) {
    // console..log('Reciept', receipt)
    var plan = this.props.selectPlan
    AsyncStorage.getItem('token').then(token => {
      var formdata = new FormData()
      if (this.props.groupType == 'public') {
        formdata.append('groupName', this.props.groupName)
        formdata.append('cityName', cityName)
        formdata.append('city_latitude', location.lat.toString())
        formdata.append('city_longitude', location.lng.toString())
        formdata.append('groupType', 'PUBLIC')
        formdata.append('memberIds', JSON.stringify(this.state.memberListID))
        formdata.append('avatar', this.props.imageData)
        if(!this.state.isAdmin){
          formdata.append('receipt', JSON.stringify(receipt))
          formdata.append('Amount', plan.price)
        }
      }
      else {
        formdata.append('groupName', this.props.groupName)
        formdata.append('cityName', cityName)
        formdata.append('city_latitude', location.lat.toString())
        formdata.append('city_longitude', location.lng.toString())
        formdata.append('groupType', 'PRIVATE')
        formdata.append('memberIds', JSON.stringify(this.state.memberListID))
        formdata.append('avatar', this.props.imageData)
        // console..log("privated group")
        if(!this.state.isAdmin){
          formdata.append('receipt', JSON.stringify(receipt))
          formdata.append('Amount', plan.price)
        }
       
      }
      // console..log("params : ", formdata)
      fetch('http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/api/creategroup', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
          'token': token
        },
        body: formdata
      }).then((result) => result.json())
        .then((res) => {
          // console..log("response : ", res)
          if (res.data.status == "Success") {
              Actions.reset('Home', { groupType: this.props.groupType })
          }
        })


    })


  }
  renaderListItem(item, index) {
    let { memberList } = this.state
    var add = this.state.memberListID.includes(item.id)
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={[styles.listItemview, { backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8', flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
        <View style={[styles.col1, { paddingLeft: chnageLayout ? 15 : 0 }]}>
          <Image source={item.profile ? { uri: item.profile } : require('../../assets/images/defaultprofile.png')} style={[styles.logo, !item.profile ? { tintColor: '#ff7a00' } : null]} />
        </View>
        <View style={styles.col2}>
          <Text style={{ color: 'black', fontSize: 16, textAlign: chnageLayout ? 'left' : 'right', marginRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
        </View>
        <View style={styles.col3}>
          <TouchableOpacity
            onPress={() => this.addMember(item.id)}
            style={[{ backgroundColor: add ? 'white' : '#ff7a00' }, styles.addbutton]}>
            <Text style={[{ color: add ? '#ff7a00' : 'white' }, styles.addbtntxt]}>ADD</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  _search(text){
    var filtered
    if(text){
      filtered = this.state.memberList.filter((item)=>item.name.toLowerCase().includes(text.toLowerCase()))
      this.setState({searched:filtered})
    }
    else{
      this.setState({searched:[]})
    }
  }
  render() {
    I18n.locale = this.state.appLanguage
    var plan = this.props.selectPlan
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
  //  // console..log("plan : ", this.props)
    return (
      <View style={styles.container}>
        <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:5,justifyContent:'space-around',borderBottomWidth:0.5,borderColor:'#ff7a00'}}>
            <Icon name='search' type='MaterialIcons' style={{color:'#ff7a00'}} />
            <TextInput placeholder='Search' style={{width:'90%',height:Platform.OS == 'ios' ? 45 : null, marginLeft:Platform.OS == 'ios' ? 5 : 0}} onChangeText={(text)=>this._search(text)} />
        </View>
        <TouchableOpacity 
          onPress={()=> this.props.groupType == 'public'?this.props.button('step2'):this.props.button('step3')}
          style={{alignSelf:'flex-start',flexDirection:'row',alignItems:'center'}}>
          <Icon name='chevron-small-left' type='Entypo' />
          <Text style={{color:'black'}}>
            {I18n.t('back')}
          </Text>
        </TouchableOpacity>
        <Text style={[styles.title, { padding: this.props.groupType == 'public' ? 0 : 2 }]}>{this.props.groupName}</Text>
        {this.props.groupType == 'public' ?
          <Text style={styles.member}>{this.state.memberListID.length} {I18n.t('members added')}</Text>
          :
           this.state.isAdmin?
          <Text style={styles.member}>{this.state.memberListID.length} {I18n.t('members added')}</Text>
           :
          <View style={styles.card}>
            <View style={[styles.tagcol]}>
              <Text style={[styles.addmember, { alignSelf: 'flex-start' }]}>{this.state.memberListID.length} {I18n.t('members added')}</Text>
              <View style={styles.tagview}>
                <Text style={styles.tagtext}>You can add {plan?plan.users:null} Members</Text>
              </View>
            </View>
            <View style={styles.tagcol2}>
              <Text style={styles.price}>
                {plan?plan.price:null}</Text>
              <Text style={styles.month}>/MONTH</Text>
            </View>
          </View>
        }
        <FlatList
          contentContainerStyle={{ paddingBottom: '22%' }}
          extraData={this.state}
          data={this.state.searched.length > 0 ? this.state.searched : this.state.memberList}
          renderItem={({ item, index }) =>
            this.state.memberId != item.id ? this.renaderListItem(item, index) : null}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={styles.footerview}>
          <TouchableOpacity
            onPress={() => this._buyProduct()}
            style={styles.button}>
            <Text style={styles.buttontxt}>{this.state.isAdmin ? I18n.t('create your group') :
              I18n.t('pay and create your group')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  listItemview: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    width: '100%',
  },
  col1: {
    width: '25%',
  },
  col2: {
    width: '50%',
    paddingLeft: 10
  },
  col3: {
    width: '25%',
    alignItems: 'center'
  },
  addbutton: {
    padding: 5,
    width: '50%',
    borderWidth: 1,
    borderColor: '#ff8a2a',
    borderRadius: 50,
    alignItems: 'center'
  },
  addbtntxt: {
    fontSize: 12,
    fontWeight: 'bold'
  },
  title: {
    color: 'black',
    fontSize: 20,
   // paddingTop: 10
  },
  member: {
    color: '#ff8a2a',
    fontSize: 13,
    paddingBottom: 10
  },
  card: {
    backgroundColor: '#ff8a2a',
    padding: 10,
    flexDirection: 'row',
    width: '90%',
    borderRadius: 5,
    alignItems: 'center',
    marginBottom: 10,
  },
  tagcol: {
    width: '60%',
    paddingLeft: 5
  },
  addmember: {
    color: 'white'
  },
  tagview: {
    padding: 5,
    borderRadius: 50,
    backgroundColor: 'white',
    width: '70%',
    alignItems: 'center'
  },
  tagtext: {
    fontSize: 8,
    color: 'black'
  },
  tagcol2: {
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  price: {
    color: 'white',
    fontSize: 21
  },
  month: {
    color: 'white',
    fontSize: 8,
    marginTop: 5
  },
  footerview: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(350,350,350,0.6)'
  },
  button: {
    backgroundColor: '#ff7a00',
    padding: 8,
    paddingHorizontal: 10,
    borderRadius: 50,
    alignItems: 'center'
  },
  buttontxt: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold'
  }

});