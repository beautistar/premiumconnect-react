import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import { Icon } from 'native-base'
import I18n from 'react-native-i18n';
import ImagePicker from 'react-native-image-crop-picker';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
export default class CreateGroupStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProfileUri: '',
      imageData: '',
      chooseOption: false,
      deviceLanguage: 'en',
      appLanguage: 'en',
    }
  }
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  open(name) {
    if (name == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        // console..log(image);
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
    else {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true
      }).then(image => {
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
  }
  _uploadProfile(imageData) {
    var name = imageData.path.split('/')
    var filename = name[name.length - 1]
    var imageData = {
      uri: imageData.path,
      type: imageData.mime,
      name: filename
    }
    this.setState({ imageData: imageData })
  }
  getProfile() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.chooseOption}
        onRequestClose={() => { }}>
        <TouchableWithoutFeedback onPress={() => this.setState({ chooseOption: false })}>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
            <View style={{ backgroundColor: 'white', width: '70%', padding: 10, borderRadius: 5 }}>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(1)}>
                <Icon name='camera-iris' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(2)}>
                <Icon name='folder-image' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Galary</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  render() {
    I18n.locale = this.state.appLanguage
    let { ProfileUri } = this.state
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.setState({ chooseOption: true })}>
          {ProfileUri ? <Image source={{ uri: ProfileUri }} style={styles.image2} /> :
            <Image source={require('../../assets/images/groups.png')} style={styles.image} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.button('step2', this.state.imageData)}
          style={styles.button}>
          <Text style={styles.buttontxt}>{I18n.t('create group')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.button('grouplist', this.state.imageData)}
          style={styles.button}>
          <Text style={styles.buttontxt}>{I18n.t('back')}</Text>
        </TouchableOpacity>
        {this.getProfile()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    tintColor: '#dedede'
  },
  image2: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  button: {
    width: '40%',
    marginTop: 15,
    borderRadius: 50,
    backgroundColor: '#ff8a2a',
    padding: 8,
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 8
  },
  buttontxt: {
    color: 'white'
  }
});
