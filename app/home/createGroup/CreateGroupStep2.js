
import React, { Component } from 'react';
import {
  Platform,
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { setLayout } from '../../componnents/Layout'
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
import MessageBar from '../../componnents/messageBar'

export default class CreateGroupStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groupType: 'public',
      deviceLanguage: 'en',
      appLanguage: 'en',
      groupName: '',
      isAdmin:false,
    }
  }
  componentWillMount() {
    AsyncStorage.getItem('isAdmin').then((isadmin)=>{this.setState({isAdmin:JSON.parse(isadmin)})})
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  nextbutton() {
    if (this.state.groupName) {
      if (this.state.groupType == 'public') {
        this.props.button('step3', this.props.imageData, this.state.groupType, this.state.groupName)
      }
      else {
      
        if(this.state.isAdmin){
          this.props.button('step4', this.props.imageData, this.state.groupType, this.state.groupName)
        }
        else{
          this.props.button('step3', this.props.imageData, this.state.groupType, this.state.groupName)
        }
       
      }
    }
    else {
      this.setState({ errorMsg: 'Please Enter GroupName' })
      this.msgBarRef._animateMessage()
    }
  }
  render() {
    let { groupType } = this.state
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} />
        <Text style={styles.title}>{I18n.t('create your group')}</Text>
        <View style={styles.row}>
          <TouchableOpacity
            onPress={() => this.setState({ groupType: 'private' })}
            style={groupType == 'private' ? styles.activebutton : styles.button}>
            <Text style={groupType == 'private' ? styles.activebuttontxt : styles.buttontxt}>{I18n.t('private group')}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({ groupType: 'public' })}
            style={[groupType == 'public' ? styles.activebutton : styles.button, { marginLeft: 10 }]}>
            <Text style={groupType == 'public' ? styles.activebuttontxt : styles.buttontxt}>{I18n.t('public group')}</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.groupnameview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
          <Image source={require('../../assets/images/Groupname_icon.png')} style={styles.groupicon} />
          <TextInput
            onChangeText={(value) => this.setState({ groupName: value })}
            style={[styles.inputbox, { marginRight: chnageLayout ? 0 : 10, textAlign: chnageLayout ? 'left' : 'right' }]}
            placeholder={I18n.t('group name')} />
        </View>
        <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
        <TouchableOpacity
          onPress={() => this.props.button('create')}
          style={styles.nextbutton}>
          <Image source={require('../../assets/images/Next_btn.png')} style={[styles.nextbuttonicon,{transform:([{ rotateY: '180deg' }])}]} />
          <Text style={styles.nextbuttontxt}>{I18n.t('back')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.nextbutton()}
          style={styles.nextbutton}>
          <Image source={require('../../assets/images/Next_btn.png')} style={styles.nextbuttonicon} />
          <Text style={styles.nextbuttontxt}>{I18n.t('next')}</Text>
        </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: 'black',
    fontSize: 20,
    marginBottom: 25
  },
  row: {
    flexDirection: 'row'
  },
  activebutton: {
    width: 90,
    height: 90,
    borderRadius: 50,
    backgroundColor: '#ff8a2a',
    justifyContent: 'center',
    elevation: 8
  },
  button: {
    width: 90,
    height: 90,
    borderRadius: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    elevation: 8,
    borderWidth: Platform.OS == 'ios' ? 1 : 0,
    borderColor: '#c5c5c5'
  },
  activebuttontxt: {
    color: 'white'
    , textAlign: 'center'
  },
  buttontxt: {
    color: '#ff8a2a'
    , textAlign: 'center'
  },
  groupnameview: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    width: '80%',
    borderBottomWidth: 1,
    borderColor: '#ff8a2a',
    paddingVertical: Platform.OS == 'ios' ? 10 : 0
  },
  groupicon: {
    width: 20,
    height: 25
  },
  inputbox: {
    marginLeft: 5
  },
  nextbutton: {
    marginTop: Dimensions.get('screen').height / 5
  },
  nextbuttonicon: {
    width: 30,
    height: 30
  },
  nextbuttontxt: {
    color: '#ff8a2a',
    marginTop: 10
  }

});
