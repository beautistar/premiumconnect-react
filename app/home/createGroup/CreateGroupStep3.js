
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { Icon } from 'native-base'
import { setLayout } from '../../componnents/Layout'
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../../componnents/Language'
import * as RNIap from 'react-native-iap';
var itemSkus = Platform.select({
  ios: [
    'plan1', 'plan2', 'plan3', 'plan4'
  ],
  android: [
    'plan1', 'plan2', 'plan3', 'plan4'
  ],
});
export default class createGroupStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectPlan: 1,
      deviceLanguage: 'en',
      appLanguage: 'en',
      selectedPlan: { users: '1 - 25', price: 20, maxuser: 25, productId: 'plan1' },
      plan: []
    }
  }
  componentDidMount() {
    RNIap.getProducts(itemSkus)
      .then((product) => {
        // console..log('product', product)
        var productlist = product.filter(item => {
          return itemSkus.includes(item.productId)
        })
        // console..log('product is :::: ', productlist)
        this.setState({ plan: productlist, selectedPlan: { users: '1 - 25', price: productlist[0].localizedPrice, maxuser: 25, productId: 'plan1' } })

      })
  }
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  renderPlan(plan, price, planid, onPress) {
    let { selectPlan } = this.state
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.listItem, { backgroundColor: planid == selectPlan ? '#ff8a2a' : 'white' }, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
        <View style={styles.col}>
          <Text style={[styles.plan, { color: planid == selectPlan ? 'white' : 'black', textAlign: chnageLayout ? 'left' : 'right' }]}>
            {plan}
          </Text>
        </View>
        <View style={styles.col2}>
          <Text style={[styles.price, { color: planid == selectPlan ? 'white' : '#ff8a2a' }]}>{price}</Text>
          <Text style={[styles.month, { color: planid == selectPlan ? 'white' : '#ff8a2a', marginTop: 5 }]}>/MONTH</Text>
          <Icon name='ios-checkmark-circle-outline' type='Ionicons' style={[styles.doneicon, { color: planid == selectPlan ? 'white' : '#d9d9d9' }]} />
        </View>
      </TouchableOpacity>
    )
  }
  choosePlan(index, plan) {
    this.setState({ selectPlan: index, selectedPlan: plan })
  }
  render() {
    I18n.locale = this.state.appLanguage
    let { plan } = this.state
    if (plan.length == 0)
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='large' color='black' />
        </View>
      )
    else
      return (
        <View style={styles.container}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <Text style={styles.title}>{I18n.t('choose your plan')}</Text>
          {this.renderPlan(' 1 - 25 Users', plan[0].localizedPrice, 1, () => this.choosePlan(1, { users: '1 - 25', price: plan[0].localizedPrice, maxuser: 25, productId: 'plan1' }))}
          {this.renderPlan(' 25 - 49 Users', plan[1].localizedPrice, 2, () => this.choosePlan(2, { users: '25 - 49', price: plan[1].localizedPrice, maxuser: 49, productId: 'plan2' }))}
          {this.renderPlan(' 49 - 75 Users', plan[2].localizedPrice, 3, () => this.choosePlan(3, { users: '49 - 75', price: plan[2].localizedPrice, maxuser: 75, productId: 'plan3' }))}
          {this.renderPlan(' 75 - 100 Users', plan[3].localizedPrice, 4, () => this.choosePlan(4, { users: '75 - 100', price: plan[3].localizedPrice, maxuser: 100, productId: 'plan4' }))}
          </View>
          <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between',marginBottom:20}}>
              <TouchableOpacity
                onPress={() => this.props.button('step2')}
                style={styles.nextbutton}>
                <Image source={require('../../assets/images/Next_btn.png')} style={[styles.nextbuttonicon,{transform:([{ rotateY: '180deg' }])}]} />
                <Text style={styles.nextbuttontxt}>{I18n.t('back')}</Text>
              </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.button('step4', this.props.imageData, this.props.groupType, this.props.groupName, this.state.selectedPlan, this.props.imageData)}
                  style={styles.nextbutton}>
                  <Image source={require('../../assets/images/Next_btn.png')} style={styles.nextbuttonicon} />
                  <Text style={styles.nextbuttontxt}>{I18n.t('next')}</Text>
                </TouchableOpacity>
          </View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor:'#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 60,
    tintColor: '#dedede'
  },
  listItem: {
    flexDirection: 'row',
    width: '90%',
    marginBottom: 8,
    alignItems: 'center',
    padding: 5,
    paddingVertical: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#d9d9d9'
  },
  col: {
    width: '60%'
  },
  plan: {
    color: 'black'
  },
  col2: {
    width: '40%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  price: {
    color: '#ff8a2a'
  },
  month: {
    fontSize: 8,
    color: '#ff8a2a'
  },
  col3: {
    width: '10%'
  },
  doneicon: {
    marginLeft: 5,
    color: '#d9d9d9'
  },
  title: {
    color: 'black',
    fontSize: 20,
    marginBottom: 20
  },
  nextbutton: {
    //position: 'absolute',
   // bottom: 20
  },
  nextbuttonicon: {
    width: 30,
    height: 30
  },
  nextbuttontxt: {
    color: '#ff8a2a',
    marginTop: 10,
  }

});
