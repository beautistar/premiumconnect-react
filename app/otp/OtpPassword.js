
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import firebase  from 'react-native-firebase';

import Api from '../componnents/api'
import MessageBar from '../componnents/messageBar'

var confirm
export default class OtpPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      num1: '',
      num2: ' ',
      num3: ' ',
      num4: ' ',
      num5: ' ',
      num6: ' ',
      errorMsg:'',
      otp:'',
      confirmResult:'',
      verificationId:'',
      autoVerify:false,
    }
  }
 
   componentWillMount() {

    this.phoneAuth(this.props.phoneNumber)
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {  this.setState({ appLanguage: lng }) })

    firebase.auth().onAuthStateChanged(user=>{
      console.log('user : ',user)
      if(user){
        setTimeout(()=>{
           this.setState({autoVerify:false})
           firebase.auth().signOut()
           this.PageNav()
        },2000)
      
      }
    })

  }
  PageNav(){
           if (this.props._type == 'register') {
                Api.put('/api/updateVerify', null, this.props.token)
                  .then((result) => {
                    if (result.status == "Success") {
                       Actions.UploadProfile({ userData: result.user, token: this.props.token })
                    }
                    else {
                      this.setState({ errorMsg: result.data.message })
                      this.msgBarRef._animateMessage()
                    }
                  })
              }
              else {
                Api.postWithToken('/api/createDevGroup', null, this.props.token)
                  .then((result) => {
                    AsyncStorage.setItem('token', this.props.token)
                    AsyncStorage.setItem('userData', JSON.stringify(this.props.userData))
                    Actions.reset('Home')
                  })
              }
            
  }
  phoneAuth(phoneNumber){
    
    firebase.auth()
  .verifyPhoneNumber(phoneNumber)
  .on('state_changed', (phoneAuthSnapshot) => {
     console.log('phone state : ',phoneAuthSnapshot)
    switch (phoneAuthSnapshot.state) {

      case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
        console.log('code sent');
          this.setState({verificationId:phoneAuthSnapshot.verificationId})
        break;
      case firebase.auth.PhoneAuthState.ERROR: // or 'error'
        console.log('verification error');
        console.log(phoneAuthSnapshot.error);
        this.setState({ errorMsg: 'The Code is Invalid or Exprired. Please Resend'})
        this.msgBarRef._animateMessage()
        break;
      case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
        console.log('auto verify on android timed out');
        // this.setState({ errorMsg: 'Auto verify  timed out'})
        // this.msgBarRef._animateMessage()
       
        break;
      case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
       console.log('auto verified on android');
       this.setState({autoVerify:true})
        console.log(phoneAuthSnapshot);
      
         const { verificationId, code } = phoneAuthSnapshot;
         const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);
        this.setState({num1:code[0],num2:code[1],num3:code[2],num4:code[3],num5:code[4],num6:code[5]})

        // Do something with your new credential, e.g.:
         firebase.auth().signInWithCredential(credential)
        // firebase.auth().currentUser.linkWithCredential(credential);
        
        // etc ...
        break;
    }
  }, (error) => {
   
    console.log(error);
    // verificationId is attached to error if required
    console.log(error.verificationId);
  }, (phoneAuthSnapshot) => {
    console.log(phoneAuthSnapshot);
  });

  }
  ressendPassword() {
    this.setState({ num1: '', num2: '', num3: '', num4: '', num5: '', num6: '' })
     //console.log("phone number : ", this.props.phoneNumber)
   
    var phoneNumber = this.props.phoneNumber
    this.phoneAuth(phoneNumber)
  
  }
  async verifyOtp() {
    let { num1, num2, num3, num4, num5, num6 } = this.state
    var code = num1 + num2 + num3 + num4 + num5 + num6
    // console.log("code : ",)
    if (code) {
      const credential = firebase.auth.PhoneAuthProvider.credential(this.state.verificationId, code);
      firebase.auth().signInWithCredential(credential) 
   
    }
    else{
      // console.log("error ")
      this.setState({ errorMsg: 'Please Enter OTP'})
      this.msgBarRef._animateMessage()
    }
  }
  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <ImageBackground source={require('../assets/images/OTP_password_bg.png')} resizeMode='cover' style={styles.container}>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.subcontainer}>
            <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
            <Image source={require('../assets/images/OTP_icon.png')} style={{ width: 55, height: 60, marginVertical: 20, marginLeft: 15 }} />
            <View style={styles.box}>
              <Text style={styles.title}>{I18n.t('enter')} OTP</Text>
              <Text style={styles.title2}>{I18n.t('sent') + " " + I18n.t('to') + " " + I18n.t('your') + " " + I18n.t('mobile-number')}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '85%' }}>
                <View style={styles.textboxview}>
                  <TextInput
                    style={styles.inputbox}
                    maxLength={1}
                    keyboardType='number-pad'
                    onChangeText={(value) => { value ? this.txt2.focus() : null, this.setState({ num1: value }) }}
                    value={this.state.autoVerify?this.state.num1:null}
                 />
                </View>
                <View style={styles.textboxview}>
                  <TextInput
                    ref={ref => this.txt2 = ref}
                    maxLength={1}
                    keyboardType='number-pad'
                    style={styles.inputbox}
                    onChangeText={(value) => { value ? this.txt3.focus() : null, this.setState({ num2: value }) }}
                    value={this.state.autoVerify?this.state.num2:null}
                  />
                </View>
                <View style={styles.textboxview}>
                  <TextInput
                    keyboardType='number-pad'
                    ref={ref => this.txt3 = ref}
                    maxLength={1}
                    style={styles.inputbox}
                    onChangeText={(value) => { value ? this.txt4.focus() : null, this.setState({ num3: value }) }}
                    value={this.state.autoVerify?this.state.num3:null}
                  />
                </View>
                <View style={styles.textboxview}>
                  <TextInput
                    keyboardType='number-pad'
                    maxLength={1}
                    ref={ref => this.txt4 = ref}
                    style={styles.inputbox}
                    onChangeText={(value) => { value ? this.txt5.focus() : null, this.setState({ num4: value }) }}
                    value={this.state.autoVerify?this.state.num4:null}
                    />
                </View>
                <View style={styles.textboxview}>
                  <TextInput
                    keyboardType='number-pad'
                    maxLength={1}
                    ref={ref => this.txt5 = ref}
                    style={styles.inputbox}
                    onChangeText={(value) => { value ? this.txt6.focus() : null, this.setState({ num5: value }) }}
                    value={this.state.autoVerify?this.state.num5:null}
                  />
                </View>
                <View style={styles.textboxview}>
                  <TextInput
                    keyboardType='number-pad'
                    maxLength={1}
                    ref={ref => this.txt6 = ref}
                    style={styles.inputbox}
                    onChangeText={(value) => this.setState({ num6: value })}
                    value={this.state.autoVerify?this.state.num6:null}
                 />
                </View>
              </View>
              <TouchableOpacity
                onPress={() => this.ressendPassword()}
                style={{ marginTop: 10 }}>
                <Text style={styles.title4} >{I18n.t('resend') + " " + I18n.t('password')}</Text>
              </TouchableOpacity>
             {this.state.autoVerify?
             <View style={{justifyContent:'center',alignItems:'center'}}>
               <ActivityIndicator size='large' color='white' />
             </View>
              :<TouchableOpacity
                onPress={() => this.verifyOtp()}
              >
                <Image source={require('../assets/images/otp_btn2.png')} style={styles.otpbutton} />
              </TouchableOpacity>}
              <Text style={styles.title3}>{I18n.t('welcome')}</Text>
            </View>
            <View style={styles.fotterview}>
              <TouchableOpacity
                onPress={() => Actions.SignUp()}
                style={styles.signupbutton}>
                <Text style={styles.signupbuttontitle} >{I18n.t('sign-up') + " " + I18n.t('now')}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} />
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: Dimensions.get('window').height - 24,

  },
  logo: {
    width: 200,
    height: 120
  },
  box: {
    alignItems: 'center',
    width: '100%',
    paddingBottom: 20
  },
  title: {
    color: 'white',
    fontSize: 22
  },
  title2: {
    color: '#ffa232',
    fontSize: 15,
    fontWeight: 'bold'
  },
  title4: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  textboxview: {
    backgroundColor: 'white',
    width: 45,
    height: 45,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputbox: {
    fontWeight: 'bold',
    padding: 10,
    fontSize: 20,
    width: '100%',
    textAlign: 'center',
    color: 'black'
  },
  otpbutton: {
    width: 50,
    height: 58,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 15,
    fontWeight: 'bold'
  }

});
