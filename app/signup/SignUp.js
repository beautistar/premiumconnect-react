
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  Animated,
  AsyncStorage,
  Modal,
  Linking,
  Alert,
  AppState,
  PermissionsAndroid,
} from 'react-native';
import {CheckBox} from 'native-base'
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import Api from '../componnents/api';
import firebase from 'react-native-firebase';
import FCM from 'react-native-fcm'
import MessageBar from '../componnents/messageBar';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager, AccessToken } from "react-native-fbsdk";
import RNSettings from 'react-native-settings'
import CountryCodeList from 'react-native-country-code-list'
import Geocoder from 'react-native-geocoding';
var cityName, userLocation, location, fcmToken;

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      fullName: '',
      phoneNumber: '',
      email: '',
      password: '',
      errorMsg: '',
      height: new Animated.Value(0),
      opacity: new Animated.Value(0),
      countryCode: '+1',
      openModal: false,
      appState: AppState.currentState,
      color:'red',
      deviceToken:'',
      checked:false
    }
    // FCM.getFCMToken()
    //   .then((token) => {
    //     // console.log('FCM TOken', token)
    //     fcmToken = token
    //   })
  }
  _openLocationSetting() {
    if (Platform.OS == 'android') {
      RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
        then((result) => {
          if (result === RNSettings.ENABLED) {
            // console.log('location is enabled')
          }
        })
    }
    else {
      Linking.openURL("App-Prefs:root=Privacy&path=LOCATION_SERVICES")
    }
  }
  _getLocation() {


    navigator.geolocation.getCurrentPosition((position) => {
      // console.log('Current Location', position.coords.latitude, position.coords.longitude)
      Geocoder.init("AIzaSyCJDz_lrzpIdEqfkW4TH1UHzc_55Z-lxy0"); // use a valid API key
      userLocation = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }

      Geocoder.from({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      })
        .then(json => {
          var _filter = json.results.filter((item) => {
            if (item.types.includes('administrative_area_level_1')) {
              return item
            }
          })
          // console.log('Filter', _filter)
          // console.log(_filter[0].address_components[0].long_name);
          cityName = _filter[0].address_components[0].long_name
          // console.log(_filter[0].geometry.location)
          location = _filter[0].geometry.location

        })
        .catch(error =>  console.warn(error));

    }, (error) => {
      // console.log(error)
      if (error.code == 2)
        Alert.alert(
          'Location Service Disable',
          'Please enable location service to use this App',
          [
            { text: 'OK', onPress: () => { this._openLocationSetting() } },
          ],
          { cancelable: false }
        )
    },
      { enableHighAccuracy: false, timeout: 20000 }
    )
  }
  _checkPermission() {
    PermissionsAndroid.check('android.permission.ACCESS_FINE_LOCATION')
      .then((result) => {
        // console.log('Permission result', result)
        if (result != 'granted') {
          PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
            .then((result) => {
              if (result == 'granted') {
                this._getLocation()

              }
              else {
                this._checkPermission()
              }
            })
        }
        else {
          this._getLocation()

        }
      })
  }
  componentWillMount() {
    if (Platform.OS == 'android') {
      this._checkPermission()
    }
    else {
      this._getLocation()
    }
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  componentDidMount() {
    FCM.getFCMToken()
    .then((token) => {
      // console.log('FCM TOken', token)
      fcmToken = token
      if(token){
        this.setState({deviceToken:token})
      }
      else{
        FCM.getFCMToken()
        .then((token) => {
          // console.log('FCM TOken33333', token)
          this.setState({deviceToken:token})
        })
      }
      
    })
    AppState.addEventListener('change', this._handleAppStateChange);
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }
  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      // console.log('App has come to the foreground!');
      this._getLocation()
    }
    this.setState({ appState: nextAppState });
  };
  _signUp() {
    if (this.state.fullName && this.state.email && this.state.phoneNumber && this.state.password) {
      if(this.state.checked == false){
        this.setState({ errorMsg: 'Please accept license and agreement' })
        this.msgBarRef._animateMessage()
      }
      else{
      var data = {
        latitude: userLocation.latitude,
        longitude: userLocation.longitude,
        countryCode: this.state.countryCode,
        fullName: this.state.fullName,
        phoneNumber: this.state.phoneNumber,
        emailId: this.state.email,
        password: this.state.password,
        signUp_with: 'phone',
        cityName: cityName,
        city_latitude: location.lat,
        city_longitude: location.lng,
        deviceId:this.state.deviceToken
      }
      var _phoneNumber = this.state.countryCode + this.state.phoneNumber
      // console.log("phone number : ", _phoneNumber)

      Api.post('/register', data)
        .then((result) => {
          // console.log('Register api result=-====', result)
          if (!result.data.data.verify) {
            // firebase.auth().signInWithPhoneNumber(_phoneNumber)
            //   .then(confirmResult => {
            //     // console.log('result', confirmResult)
            //     Actions.OtpPassword({ confirmResult: confirmResult, token: result.data.token, phoneNumber: _phoneNumber, _type: 'register' })
            //   })
            //   .catch(error => this.setState({ message: `Sign In With Phone Number Error: ${error.message}` }))
            this.setState({errorMsg:'Verification E-mail Successfully Sent.',color:'green'})
            this.msgBarRef._animateMessage()
            setTimeout(()=>{
              Actions.reset('Login')
            },5000)
          }
          else if (result.data.data.verify) {
            this.setState({ errorMsg: result.data.message, })
            this.msgBarRef._animateMessage()
          }
        })
      }
    }
    else {
      this.setState({ errorMsg: 'Please fill all the details' })
      this.msgBarRef._animateMessage()
    }

  }
  _socialLogin() {
    var data = {
      latitude: userLocation.latitude,
      longitude: userLocation.longitude,
      cityName: cityName,
      city_latitude: location.lat,
      city_longitude: location.lng,
      deviceId: this.state.deviceToken,
      phoneNumber: this.state.phone,
      emailId: this.state.email,
      signUp_with: this.state.loginWith,
      socialId: this.state.socialID,
      fullName: this.state.name,
      image: this.state.profile
    }
    // console.log('Api data', data)
    Api.post('/register', data)
      .then((result) => {
        // console.log('Socila login api ', result)
        if (result.data.status == "Success") {
          Api.postWithToken('/api/createDevGroup', null, result.data.token)
          .then((result1) => {
            // console.log('Create Group api', result1)
              AsyncStorage.setItem('token', result.data.token)
              AsyncStorage.setItem('userData', JSON.stringify(result.data.user))
              Actions.reset('Home')
          })  
        }
      })
  }
  async _googleSignUp() {
    try {
      GoogleSignin.signIn()
        .then((result) => {
          // console.log('google', result.user)
          if (result.user.id) {
            this.setState({ socialID: result.user.id, loginWith: 'google', email: result.user.email, name: result.user.name, profile: result.user.photo, phone: null }, () => this._socialLogin())
          }
        })
    } catch (error) {
      // console.log('error', error)
    }
  }
  _facebookSignUp() {
    LoginManager.logOut()
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
          alert('Login cancelled');
        }
        else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let token = data.accessToken;
            //// console.log(token)
            fetch('https://graph.facebook.com/v3.0/me?fields=email,name,picture&access_token=' + token)
              .then((resp) => resp.json())
              .then((result) => {
                // console.log('facebook', result)
                if (result.id) {
                  this.setState({ socialID: result.id, loginWith: 'facebook', email: result.email, name: result.name, profile: result.picture.data.url }, () => this._socialLogin())
                }
              })
          })
        }
      })
  }
  render() {
    //// console.log("country code ",countryCode)
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <ImageBackground source={require('../assets/images/signup_bg.png')} resizeMode='cover' style={styles.container}>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.subcontainer}>
            <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
            <View style={styles.box}>
              <Text style={styles.title}>{I18n.t('sign-up')}</Text>
              <Text style={styles.title2}>{I18n.t('and') + " " + I18n.t('create') + " " + I18n.t('your') + " " + I18n.t('own') + " " + I18n.t('group')}</Text>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/Name_icon.png')} style={styles.nameicon} />
                <TextInput
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ fullName: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('full') + " " + I18n.t('name')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/Otp_Phone_icon.png')} style={styles.phoneicon} />
                <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this.setState({ openModal: true })}>
                  <Text>{this.state.countryCode}</Text>
                </TouchableOpacity>
                <TextInput
                  keyboardType='phone-pad'
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ phoneNumber: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '58%' }]}
                  placeholder={I18n.t('phone') + " " + I18n.t('number')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/email_icon.png')} style={styles.mailicon} />
                <TextInput
                  keyboardType='email-address'
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ email: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('email') + I18n.t('address')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/lock.png')} style={styles.loackicon} />
                <TextInput
                  //keyboardType='visible-password'
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ password: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('password')}
                />
              </View>
              <View style={{width:'80%',marginTop:10,flexDirection:'row',padding:5,justifyContent:'center',alignItems:'center'}}>
                  <CheckBox onPress={()=>this.setState({checked:!this.state.checked})} color='#ffa433' checked={this.state.checked} style={{marginRight:20}} />
                  <View style={{flexDirection:'row',width:'90%',flexWrap:'wrap'}}>
                    <Text style={styles.title3}>i agree to your <Text style={[styles.title3,{color:'#ffa433'}]} onPress={()=>Linking.openURL("https://www.eulatemplate.com/live.php?token=awpTeAfERZyCdr3ncl1iOFT95kh1iygS")}>End-User License and Agreement</Text></Text>
                  </View>
              </View>
              <TouchableOpacity
                onPress={() => this._signUp()}
              >
                <Image source={require('../assets/images/signup_btn.png')} style={styles.otpbutton} />
              </TouchableOpacity>
              <Text style={styles.title3}>{I18n.t('sign-up')}</Text>
              <Image source={require('../assets/images/or_icon.png')} style={styles.orbutton} />
            </View>
           
            <View style={styles.fotterview}>
              <View style={[styles.logincontainer, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Text style={[styles.buttontitle, { marginLeft: chnageLayout ? 0 : 10 }]}>{I18n.t('sign-up') + " " + I18n.t('with')}</Text>
                <TouchableOpacity onPress={() => this._facebookSignUp()}><Image source={require('../assets/images/facebook_icon.png')} style={styles.facebookicon} /></TouchableOpacity>
                <TouchableOpacity onPress={() => this._googleSignUp()}><Image source={require('../assets/images/google_icon.png')} style={styles.googleicon} /></TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => Actions.pop()}
                style={styles.signupbutton}>
                <Text style={styles.signupbuttontitle} >
                  {I18n.t('already') + " " + I18n.t('a') + " " + I18n.t('member') + " ? " + I18n.t('login')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>Linking.openURL('https://premiumconnectapp.com/terms-and-conditions')} style={{justifyContent:'center',alignItems:'center',margin:3}}>
              <Text style={{color:'#ffa433',fontWeight:'bold',fontSize:12}}>Terms And Conditions</Text>
            </TouchableOpacity>
            </View>
          </View>
          
        </ScrollView>
        <Modal
          transparent={true}
          visible={this.state.openModal}
          onRequestClose={() => { }}
        >
          <View style={{ backgroundColor: 'white', flex: 1 }}>
            <TouchableOpacity
              style={{ right: 10, padding: 10, marginTop: Platform.OS == 'ios' ? 30 : 20, position: 'absolute' }}
              onPress={() => this.setState({ openModal: false })}>
              <Text>Cancel</Text>
            </TouchableOpacity>
            <CountryCodeList
              sectionHeaderHeight={0}
              onClickCell={(country) => {
                this.setState({
                  countryCode: country.code,
                  openModal: false
                })
                // console.log(country)
              }
              }
            />
          </View>
        </Modal>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    height: Platform.OS == 'ios' ? Dimensions.get('window').height : Dimensions.get('window').height - 24  ,
    justifyContent: 'center',
    paddingBottom: '15%'
  },
  logo: {
    width: 170,
    height: 100
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 18
  },
  title2: {
    color: '#ffa232',
    fontSize: 10,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '80%',
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  loackicon: {
    width:45,
    height: 30,
    tintColor: '#ffa433'
    //marginHorizontal: 10
  },
  nameicon: {
    width: 20,
    height: 20,
    marginHorizontal: 10
  },
  mailicon: {
    width: 25,
    height: 20,
    marginHorizontal: 10
  },
  inputbox: {
    fontWeight: 'bold',
    fontSize: 12,
    height:35,
    width: '65%',
    color: 'black'
  },
  otpbutton: {
    width: 40,
    height: 45,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold'
  },
  orbutton: {
    width: 25,
    height: 25,
    marginTop: 10
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  logincontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 8,
    backgroundColor: 'black'
  },
  buttontitle: {
    color: 'white',
    fontSize: 15
  },
  facebookicon: {
    width: 25,
    height: 25,
    marginLeft: 10
  },
  googleicon: {
    width: 25,
    height: 25,
    marginLeft: 10
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 13,
    fontWeight: 'bold'
  }

});
