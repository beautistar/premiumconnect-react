
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  StatusBar,
  Modal,
  TouchableWithoutFeedback,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import Api from '../componnents/api'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon } from 'native-base'
import ImagePicker from 'react-native-image-crop-picker';
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chooseOption: false,
      profile: '',
      ProfileUri: '',
      deviceLanguage: 'en',
      appLanguage: 'en',
    }
  }
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  open(name) {
    if (name == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        // console.log(image);
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
    else {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true
      }).then(image => {
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
  }
  _uploadProfile(imageData) {
    var name = imageData.path.split('/')
    var filename = name[name.length - 1]
    var formData = new FormData()
    formData.append("id", this.props.userData._id)
    formData.append("avatar", {
      uri: imageData.path,
      type: imageData.mime,
      name: filename
    })
    fetch(
      'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/api/uploadimage',
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
          'token': this.props.token
        },
        body: formData
      }).then((result) => result.json())
      .then((res) => {
        // console.log('API---------', res)
        var userData = this.props.userData
        userData.image = res.data.image
        AsyncStorage.setItem('token', this.props.token)
        AsyncStorage.setItem('userData', JSON.stringify(userData))
        Api.postWithToken('/api/createDevGroup', null, this.props.token)
          .then((result) => {
            // console.log('Create Group api', result)
          })
      })
  }
  _login() {
    Actions.reset('Home')
  }
  getProfile() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.chooseOption}
        onRequestClose={() => { }}>
        <TouchableWithoutFeedback onPress={() => this.setState({ chooseOption: false })}>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
            <View style={{ backgroundColor: 'white', width: '70%', padding: 10, borderRadius: 5 }}>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(1)}>
                <Icon name='camera-iris' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(2)}>
                <Icon name='folder-image' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Galary</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  render() {
    let { ProfileUri } = this.state
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header style={{ backgroundColor: '#020304' }}>
          <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <View style={styles.subcontainner}>
          <Text style={styles.title}>
            {I18n.t('hi') + " ! " + this.props.userData.fullName}
          </Text>
          <Text style={styles.title2}>
            {I18n.t('please') + " " + I18n.t('upload') + " " + I18n.t('profile') + " " + I18n.t('picture')}
          </Text>
          <TouchableOpacity
            onPress={() => this.setState({ chooseOption: true })}
            style={styles.profileBtnview}>
            <Image source={ProfileUri ? { uri: ProfileUri } : require('../assets/images/Upload_Profile_icon.png')} style={{ width: 130, height: 130, borderRadius: 65 }} />
            {
              ProfileUri ? null :
                <Image source={require('../assets/images/upload_btn.png')} style={styles.uploadbutton} />
            }

          </TouchableOpacity>
          {this.getProfile()}
        </View>
        <TouchableOpacity onPress={() => this._login()}
          disabled={ProfileUri ? false : true}
          style={[styles.startbutton, { opacity: ProfileUri ? 1 : 0.5 }]}>
          <Text style={styles.buttontext}>{I18n.t('lets') + " " + I18n.t('start')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  logo: {
    marginTop: Platform.OS == 'ios' ? -3 : 3,
    width: 50,
    height: 50
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  uploadbutton: {
    width: 55,
    height: 55,
    alignSelf: 'center',
    marginTop: -40,
    borderRadius: 25
  },
  startbutton: {
    backgroundColor: '#ff8a2a',
    width: '30%',
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: 60,
    padding: 8,
    borderRadius: 50,
  },
  buttontext: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  }
});
