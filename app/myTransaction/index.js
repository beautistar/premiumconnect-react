
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Animated,
  AsyncStorage,
  StatusBar,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import I18n from 'react-native-i18n';
import Api from '../componnents/api';
import {Actions}from 'react-native-router-flux'
import {Icon,Header} from 'native-base'
import { setLayout } from '../componnents/Layout'
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      errorMsg: '',
      transactions:[],
      height: new Animated.Value(0),
      opacity: new Animated.Value(0),
      loading:true
    }
  }
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
 componentDidMount(){

  AsyncStorage.getItem('token').then(token => {
    Api.get('/api/transactions', token).then(res => {
      // console.log("result : ",res)
      if(res.data.status== 'Success'){
        this.setState({transactions:res.data.data,loading:false})
      }
      else{
        this.setState({loading:false})
      }
    })
  })
 }
  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    // console.log("state :",this.state.transactions)
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color='#000000' size='large' />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => Actions.pop()}
            >
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '90%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('my') + ' ' + I18n.t('transaction')}</Text>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        {this.state.transactions.length==0?<View style={{flex:1,justifyContent:'center'}}><Text style={{alignSelf:'center',color:'black',fontWeight:'bold'}}>There are no transactions from your account yet</Text></View>:
        <View style={{ flex: 1,width:'100%',justifyContent:'center' }}>
          <FlatList
          extraData={this.state}
          data={this.state.transactions}
          renderItem={({item})=>
          <View style={{borderBottomWidth:1,padding:10}}>
            {item.payFor=='Group'?
              <Text style={{fontSize:20,color:'black',fontWeight:'bold',marginBottom:5}}>{item.groupName}</Text>:
              <Text style={{fontSize:20,color:'black',fontWeight:'bold',marginBottom:5}}>{item.subscriptionType} Subscription </Text>
            }
            {item.payFor=='Group'? 
              <View style={{flexDirection:'row'}}>
              <Text style={{color:'black',fontWeight:'bold',}}>Price : </Text>
              <Text>{item.Amount}</Text>
            </View> 
            :null}
          <View style={{flexDirection:'row'}}>
            <Text style={{color:'black',fontWeight:'bold',}}>Transaction Date : </Text>
            <Text>{new Date(item.payDate).toLocaleDateString()}</Text>
          </View> 
          <View style={{flexDirection:'row'}}>
            <Text style={{color:'black',fontWeight:'bold',}}>Expire Date : </Text>
            <Text>{new Date(item.payExpireDate).toLocaleDateString()}</Text>
          </View> 
          </View>
        }
          />
       
        </View>}
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    height: Dimensions.get('window').height - 24,
    justifyContent: 'center',
    paddingBottom: '15%'
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 150,
    height: 120
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 20
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '70%',
    height: 40,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  nameicon: {
    width: 20,
    height: 20,
    marginHorizontal: 10
  },
  mailicon: {
    width: 25,
    height: 20,
    marginHorizontal: 10
  },
  inputbox: {
    fontWeight: 'bold',
    fontSize: 14,
    width: '100%',
    color: 'black'
  },
  otpbutton: {
    width: 50,
    height: 55,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  orbutton: {
    width: 30,
    height: 30,
    marginTop: 10
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  logincontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 8,
    backgroundColor: 'black'
  },
  buttontitle: {
    color: 'white',
    fontSize: 18
  },
  facebookicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  googleicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 15,
    fontWeight: 'bold'
  }

});
