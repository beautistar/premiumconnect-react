import React, { Component } from 'react'
import { AppState, AsyncStorage, Modal, View, Text, TouchableOpacity,Platform } from 'react-native'
import { Root } from "native-base";
import moment from 'moment'
import { Scene, Router, Stack } from 'react-native-router-flux'
import Login from './login/Login'
import OtpPassword from './otp/OtpPassword'
import SignUp from './signup/SignUp'
import UploadProfile from './signup/UploadProfile'
import Home from './home/index'
import Ems from './sidebar/Ems';
import EmsCall from './sidebar/EmsCall';
import EmsMessage from './sidebar/EmsMessage';
import EmsServices from './sidebar/EmsServices'
import GroupDetail from './home/groupDetails/GroupDetails'
import Chat from './home/chat/Chat'
import Api from './componnents/api'
import Profile from '../app/profile'
import Contact from '../app/contact'
import Transactions from '../app/myTransaction'
import KeepAwake from 'react-native-keep-awake';
import SharedLocation from '../app/sidebar/sharedLocation'
import ChnagePassword from './sidebar/ChangePassword'
import ResetPassword from './login/ResetPassword'
import Call from './sidebar/Call'
import SubscriptionDetails from './sidebar/SubscriptionDetails'
import AddContact from './sidebar/AddContact'
import FCM, { FCMEvent } from 'react-native-fcm'
import RNIap from 'react-native-iap'
// symbol polyfills
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');

// collection fn polyfills
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

var itemSkus = Platform.select({
    ios: [
        'sub.plan.premium.1month', 'sub.plan.premium.3month', 'sub.plan.premium.6month', 'sub.plan.premium.12month',
    ],
    android: [
        'plan.premium.1month', 'plan.premium.3month', 'plan.premium.6month', 'plan.premium.12month'
    ],
});

export default class Route extends Component {
    constructor(props) {
        super(props)
        this.state = {
            expireSubscription: false,
            remainingDays: ''
        }
    }
    componentWillMount() {
        AsyncStorage.getItem('token')
            .then((token) => {
                if (token) {
                    Api.get('/api/EmsExpireDate', token)
                        .then((result) => {
                            if (result.status == "Success") {
                                // console.log('Expire Date', moment(result.data).format('DD-MM-YYYY'))
                                var duration = moment(result.data).diff(moment(), 'days')
                                // console.log('duration', duration)
                                if (duration < 8 && duration > 0) {
                                    this.setState({ expireSubscription: true, remainingDays: duration })
                                }
                            }
                        })
                }
            })
    }
    _EMSServiceExpire() {
        return (
            <Modal onRequestClose={()=>{}} animationType='slide' visible={this.state.expireSubscription} transparent={true}  >
                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', alignItems: 'center' }}  >
                    <View style={{ backgroundColor: 'white', width: '90%', alignItems: 'center', borderRadius: 5, overflow: 'hidden' }}>
                        <View style={{ backgroundColor: '#ffa433', width: '100%' }}>
                            <Text style={{ fontSize: 18, color: 'white', paddingHorizontal: 10, textAlign: 'center', marginVertical: 10, backgroundColor: '#ffa433' }}>EMS Service Subscription Expire Alert</Text>
                        </View>
                        <Text style={{ margin: 20 }}>Your EMS Subscription will Expire in {this.state.remainingDays} days make sure to renew your service if you wish to countinue EMS service</Text>
                        <TouchableOpacity onPress={() => this.setState({ expireSubscription: false })} style={{ padding: 10, borderWidth: 0.5, width: '50%', alignItems: 'center', borderColor: '#ffa433', borderRadius: 5, marginVertical: 10 }}>
                            <Text style={{ color: '#ffa433' }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
    async isSubscriptionActive() {
        if (Platform.OS === 'ios') {
          const availablePurchases = await RNIap.getAvailablePurchases();
          const sortedAvailablePurchases = availablePurchases.sort(
            (a, b) => b.transactionDate - a.transactionDate
          );
          const latestAvailableReceipt = sortedAvailablePurchases[0].transactionReceipt;
       
          const isTestEnvironment = __DEV__;
          const decodedReceipt = await RNIap.validateReceiptIos(
            {
              'receipt-data': latestAvailableReceipt,
              password: ITUNES_CONNECT_SHARED_SECRET,
            },
            isTestEnvironment
          );
          const {latest_receipt_info: latestReceiptInfo} = decodedReceipt;
          const isSubValid = !!latestReceiptInfo.find(receipt => {
            const expirationInMilliseconds = Number(receipt.expires_date_ms);
            const nowInMilliseconds = Date.now();
            return expirationInMilliseconds > nowInMilliseconds;
          });
          return isSubValid;
        }
       
        if (Platform.OS === 'android') {
          // When an active subscription expires, it does not show up in
          // available purchases anymore, therefore we can use the length
          // of the availablePurchases array to determine whether or not
          // they have an active subscription.
          const availablePurchases = await RNIap.getAvailablePurchases();
          console.log('availablePurchases data : ',availablePurchases)
          for (let i = 0; i < availablePurchases.length; i++) {
              console.log('purchase data : ',availablePurchases[i])
            if (itemSkus.includes(availablePurchases[i].productId)) {
              return true;
            }
          }
          return false;
        }
       }
    componentDidMount() {
      //  console.log("subscribe : ",this.isSubscriptionActive())
        FCM. setBadgeNumber(0)
        KeepAwake.activate();
        AppState.addEventListener('change', this._handleAppStateChange)
        
    }
    componentWillUnmount() {
        KeepAwake.deactivate();
        AppState.removeEventListener('change', this._handleAppStateChange)
    }
    _handleAppStateChange = (nextAppState) => {
        var appState = false
        //// console.log('App state======', nextAppState)
        if (nextAppState == 'active') {
            appState = true
        }
        if (nextAppState == 'background' || nextAppState == 'inactive') {
            appState = false
        }
        //// console.log('App State------',appState)
        AsyncStorage.getItem('token')
            .then((token) => {
                if (token) {
                    Api.put('/api/updatelastseen', { online: appState }, token)
                        .then((result) => console.log('Online status api', result))
                }
            })
    }
    render() {
        return (
            <Root>
                <Router>
                    <Stack key="root" hideNavBar>
                        <Scene key='Login' component={Login} initial  />
                        <Scene key='OtpPassword' component={OtpPassword}  />
                        <Scene key='SignUp' component={SignUp} />
                        <Scene key='UploadProfile' component={UploadProfile} />
                        <Scene key='Home' component={Home} />
                        <Scene key='Ems' component={Ems} />
                        <Scene key='EmsCall' component={EmsCall} />
                        <Scene key='EmsMessage' component={EmsMessage} />
                        <Scene key='EmsServices' component={EmsServices} />
                        <Scene key='GroupDetail' component={GroupDetail} />
                        <Scene key='Chat' component={Chat} />
                        <Scene key='Profile' component={Profile} />
                        <Scene key='Contact' component={Contact} />
                        <Scene key='Transactions' component={Transactions} />
                        <Scene key='SharedLocation' component={SharedLocation} />
                        <Scene key='ChnagePassword' component={ChnagePassword} />
                        <Scene key='ResetPassword' component={ResetPassword} />
                        <Scene key='SubscriptionDetails' component={SubscriptionDetails} />
                        <Scene key='AddContact' component={AddContact} />
                    </Stack>
                </Router>
                {this._EMSServiceExpire()}
            </Root>
        )
    }
}
