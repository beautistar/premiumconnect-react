
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Animated,
  AsyncStorage,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import Api from '../componnents/api';
import MessageBar from '../componnents/messageBar';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import ImagePicker from 'react-native-image-crop-picker';
import CountryCodeList from 'react-native-country-code-list'
import { Container, Header, Left, Body, Right, Button, Icon, Picker } from 'native-base';
var countryCode = require('../assets/countryCode.json')
var userData;
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      fullName: '',
      phoneNumber: '',
      email: '',
      errorMsg: '',
      profile: '',
      chooseOption: false,
      countryCode: '+91',
      height: new Animated.Value(0),
      opacity: new Animated.Value(0),
      openModal: false,
      color: 'red'
    }
  }
  componentWillMount() {

    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  componentDidMount() {
    AsyncStorage.getItem('token')
      .then((token) => {
        if (token) {
          Api.get('/api/userprofile', token)
            .then((result) => {
              // console.log('Profile Api ', result)
              this.setState({
                fullName: result.data[0].fullName,
                phoneNumber: result.data[0].phoneNumber ? result.data[0].phoneNumber.toString() : null,
                email: result.data[0].emailId, profile: result.data[0].image,
                countryCode: result.data[0].countryCode ? '+' + result.data[0].countryCode : '+1'
              })
            })
        }
      })

  }
  open(name) {
    if (name == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then(image => {
        // console.log(image);
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
    else {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        includeBase64: true
      }).then(image => {
        this.setState({ ProfileUri: image.path, chooseOption: false }, () => this._uploadProfile(image))
      });
    }
  }
  _uploadProfile(imageData) {
    AsyncStorage.getItem('userData')
      .then((Data) => {
        var user = JSON.parse(Data)
        userData = user
        AsyncStorage.getItem('token')
          .then((token) => {
            var name = imageData.path.split('/')
            var filename = name[name.length - 1]
            var formData = new FormData()
            formData.append("id", userData._id)
            formData.append("avatar", {
              uri: imageData.path,
              type: imageData.mime,
              name: filename
            })
            fetch(
              'http://ec2-18-222-223-24.us-east-2.compute.amazonaws.com/api/uploadimage',
              {
                method: 'PUT',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'multipart/form-data',
                  'token': token
                },
                body: formData
              }).then((result) => result.json())
              .then((res) => {
                // console.log('API---------', res)
                userData.image = res.data.image
                this.setState({ profile: res.data.image })
                // console.log("user data : ", userData)
                this.props.updateProfile(userData)
                AsyncStorage.setItem('userData', JSON.stringify(userData))
              })
          })
      })
  }
  getProfile() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.chooseOption}
        onRequestClose={() => { }}>
        <TouchableWithoutFeedback onPress={() => this.setState({ chooseOption: false })}>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
            <View style={{ backgroundColor: 'white', width: '70%', padding: 10, borderRadius: 5 }}>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(1)}>
                <Icon name='camera-iris' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ margin: 5, flexDirection: 'row', alignItems: 'center' }} onPress={() => this.open(2)}>
                <Icon name='folder-image' type='MaterialCommunityIcons' />
                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Galary</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  _update() {
    var apiData = {
      countryCode: this.state.countryCode,
      fullName: this.state.fullName,
      phoneNumber: this.state.phoneNumber,
      emailId: this.state.email,
    }
    AsyncStorage.getItem('token')
      .then((token) => {
        Api.put('/api/updateprofile', apiData, token)
          .then((result) => {
            // console.log('REslult', result)
            if (result.status == "Success") {
              this.props.updateProfile(result.data)
              AsyncStorage.setItem('userData', JSON.stringify(result.data))
              this.setState({ errorMsg: result.message, color: '#4CAF50' })
              this.msgBarRef._animateMessage()
            }
            else {
              this.setState({ errorMsg: result.message })
              this.msgBarRef._animateMessage()
            }

          })
      })

  }
  setCountryCode(code) {
    countryCode.map(country => {
      if ("+" + country.calling_code == code)
        this.setState({ countryCode: code, countryFlag: country.flag_base64 })
    })

  }
  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => Actions.pop()}
            >
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '90%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('profile')}</Text>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.subcontainer}>
            <TouchableOpacity onPress={() => this.setState({ chooseOption: true })}>
              <Image defaultSource={require('../assets/images/defaultprofile.png')} source={this.state.profile ? { uri: this.state.profile } : require('../assets/images/defaultprofile.png')} style={[styles.logo, { tintColor: this.state.profile ? null : '#ffa433' }]} />
            </TouchableOpacity>
            <View style={styles.box}>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/Name_icon.png')} style={styles.nameicon} />
                <TextInput
                  value={this.state.fullName}
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ fullName: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('full') + " " + I18n.t('name')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/Otp_Phone_icon.png')} style={styles.phoneicon} />
                <TouchableOpacity style={{ marginRight: 10 ,paddingHorizontal:10}} onPress={() => this.setState({ openModal: true })}>
                  <Text style={{fontSize:12}}>{this.state.countryCode}</Text>
                </TouchableOpacity>

                <TextInput
                  value={this.state.phoneNumber}
                  autoCapitalize = 'none'
                  keyboardType='phone-pad'
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '58%' }]}
                  placeholder='Registered Phone Number'
                  onChangeText={(value) => this.setState({ phoneNumber: value })}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/email_icon.png')} style={styles.mailicon} />
                <TextInput
                  value={this.state.email}
                  keyboardType='email-address'
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ email: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('email') + I18n.t('address')}
                />
              </View>
              <TouchableOpacity onPress={() => this._update()} style={{ backgroundColor: '#ffa433', marginVertical: 20, padding: 10, width: '50%', alignItems: 'center', borderRadius: 5 }}>
                <Text style={{ color: 'white', fontSize: 18 }}>{I18n.t('update')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {this.getProfile()}
        <Modal
          transparent={true}
          visible={this.state.openModal}
          onRequestClose={() => { }}
        >
          <View style={{ backgroundColor: 'white', flex: 1 }}>
            <TouchableOpacity
              style={{ right: 10, padding: 10, marginTop: Platform.OS == 'ios' ? 30 : 20, position: 'absolute', }}
              onPress={() => this.setState({ openModal: false })}>
              <Text>Cancel</Text>
            </TouchableOpacity>
            <CountryCodeList
              sectionHeaderHeight={0}
              onClickCell={(country) => {
                this.setState({
                  countryCode: country.code,
                  openModal: false
                })
                // console.log(country)
              }
              }
            />
          </View>
        </Modal>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    padding: 20
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 20
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '85%',
    height: 40,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  nameicon: {
    width: 20,
    height: 20,
    marginHorizontal: 10
  },
  mailicon: {
    width: 25,
    height: 20,
    marginHorizontal: 10
  },
  inputbox: {
    fontWeight: 'bold',
    fontSize: 12,
    width: '100%',
    color: 'black'
  },
  otpbutton: {
    width: 50,
    height: 55,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  orbutton: {
    width: 30,
    height: 30,
    marginTop: 10
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  logincontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 8,
    backgroundColor: 'black'
  },
  buttontitle: {
    color: 'white',
    fontSize: 18
  },
  facebookicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  googleicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 15,
    fontWeight: 'bold'
  },
  loackicon: {
    width: 45,
    height: 45,
    tintColor: '#ffa433'
  },

});
