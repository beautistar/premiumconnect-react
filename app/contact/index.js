
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon, } from 'native-base';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
    }
  }
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }

  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => Actions.pop()}
            >
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '90%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('contact')}</Text>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../assets/images/Logo1.png')} style={styles.logo} />
          <Text style={styles.appname}>PREMIUM CONNECT</Text>
          <Text style={{ marginTop: 5, color: 'black', fontSize: 18 }}>
           info@premiumconnectapp.com
                    </Text>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },

  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 170,
    height: 100
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },

});
