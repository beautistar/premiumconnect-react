import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList,ActivityIndicator, TouchableOpacity,AsyncStorage,Share,Linking,Dimensions,ScrollView} from 'react-native';
import I18n from 'react-native-i18n';
import { Icon } from 'native-base';
import { setLayout } from './Layout'
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import MapView, { PROVIDER_GOOGLE,Marker } from 'react-native-maps'; 
import { LoginManager ,AccessToken} from "react-native-fbsdk";
import { getAppLanguage, getDeviceLanguage, setAppLanguage,getLoginData } from './Language'
import { Actions } from 'react-native-router-flux';
import Modal from "react-native-modal";
import firebase from '../componnents/Firebase'
import Api from './api'
export default class WorkAreasDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage:'',
      loginType:'',
      user:'',
      userLocation:{},
      shareModal:false,
      subscriber:false,
      memberList:[],
      memberListID:[],
      addMemberModal:false,
      isAdmin:false,
      changeLanguage:false,
    }
    this.updateProfile=this.updateProfile.bind(this)
    this._getLocation()

  }
  componentWillMount() {
    AsyncStorage.getItem('isAdmin').then((isadmin)=>{
      // console..log('is admin : ',JSON.parse(isadmin))
      this.setState({isAdmin:JSON.parse(isadmin)})
    })
    AsyncStorage.getItem('token')
    .then((token)=>{
      Api.get('/api/EmsSubscription',token)
      .then((result)=>{
        // console..log('Check Subscription Api Result',result)
        this.setState({subscriber:result.data.subscription,subscription:result.data})
      })
    }) 
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      // console..log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng })
    })
    AsyncStorage.getItem('userData').then(user=>{
      //// console..log("data0------sidebaer : ",JSON.parse(user))
      this.setState({user:JSON.parse(user)})
    })
    
  }
  updateProfile(user){
    this.setState({user:user})
  }
  reanderButton(name,icon,onPress){
    return(
      <TouchableOpacity
      onPress={onPress}
      style={{flexDirection:'row',width:'85%',borderColor:'#ffa049',borderBottomWidth:1,alignItems:'center',alignSelf:'center',paddingVertical:10}}>
        <View style={{width:'15%',alignItems:'center'}}>
        {icon?<Image source={icon} style={{width:25,height:25}}/>:
        <Icon name='profile' type='AntDesign' style={{color:'white',fontSize:18}}/>}
       </View>
       <View style={{width:'85%',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
        <Text style={{paddingLeft:10,color:'white',fontSize:13,fontWeight:'bold'}}>{ name}</Text>
        {name == I18n.t('ems') && this.state.subscriber ?<View style={{backgroundColor:'white',borderRadius:3,alignSelf:'flex-end',padding:5}}><Text style={{fontSize:12,color:'green'}}>Subscribed</Text></View> : null}
       </View>
       
      </TouchableOpacity>
    )
  }
  open(screen,language){
    if (screen == 'transaction')
      Actions.Transactions()
    if (screen == 'contact')
      Actions.Contact()
    if (screen == 'profile')
      Actions.Profile({updateProfile:this.updateProfile})
    if (screen == 'ems')
      Actions.EmsServices({subscription:this.state.subscription})
   
    this.props.closeDrawer
  }
  _logout() {
    AsyncStorage.getItem('userData').then(userData => {
      // console..log("user data : ", userData)
      userdata = JSON.parse(userData)
      AsyncStorage.getItem('token')
        .then((token) => {
          // console..log("user token : ", token)
          Api.postWithToken('/api/logout',{memberId:userData.id}, token)
            .then((result) => {
              // console..log('Logout Api Result', result)
              if (result.data.status == 'Success') {
                if (userdata.signUp_with == 'google')
                  GoogleSignin.signOut()
                else if (userdata.signUp_with == 'facebook')
                  LoginManager.logOut()
              //  this.props.clearInterval()
                AsyncStorage.clear()
                Actions.reset('Login')
              }
 
            })
        })
    })
 
  }
  componentDidMount(){
    this.getAllUser()
  }
  getAllUser(){
    AsyncStorage.getItem('token').then(token=>{
      memberdata=[]
     
         Api.postWithToken('/api/users',null,token).then(res=>{
           // console..log("data : ",res)
           if(res.status=='Success'){
           
               res.data.map((member,index)=>{
                       memberdata[index]={
                         id:member._id,
                         name:member.fullName,
                         profile:member.image
                       }
                  })
                  // console..log("data -------: ",memberdata)
                  this.setState({memberList:memberdata})
           }
         })
      })
   }
  _getLocation(){
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({userLocation:{
        latitude : position.coords.latitude,
        longitude : position.coords.longitude,
        latitudeDelta: 0.19061583341495592,
        longitudeDelta: 0.28823353350162506,
      }})
  }, (error) => {
      // console..log(error)
  },
      { enableHighAccuracy: false, timeout:20000 }
  )
  }
  _shareLocationModal(){
    return(
      <Modal 
      isVisible={this.state.shareModal}
      animationIn='slideInUp'
      animationOut='slideOutDown'
      onBackdropPress={()=>this.setState({shareModal:false})}
      onBackButtonPress={()=>this.setState({shareModal:false})}
      >
          <View style={{borderRadius:5,overflow:'hidden'}}>
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={{height:Dimensions.get('window').height/1.5,width:'100%'}}
              region={this.state.userLocation}
              loadingEnabled={true}
            >
            <Marker coordinate={this.state.userLocation}
          
            />
            </MapView>
            <TouchableOpacity style={{backgroundColor:'#f37c26',justifyContent:'center',padding:10,flexDirection:'row'}} onPress={()=>this.setState({addMemberModal:true})}>
              <Icon name='location' type='Entypo' style={{color:'white',marginHorizontal:5}} />
              <Text style={{color:'white',fontSize:20,fontWeight:'bold',marginHorizontal:5}}>{I18n.t('share location')}</Text>
            </TouchableOpacity>
          </View>
          {this.addMemberModalView()}
      </Modal>
    )
  }

 addMember(memberId){
  
    if(this.state.memberListID.includes(memberId)){
      index=this.state.memberListID.indexOf(memberId)
      this.state.memberListID.splice(index,1)
      this.setState({add:false})
    }
    else{
      this.state.memberListID.push(memberId)
      this.setState({add:true})
    }
 }
 renaderListItemModal(item,index){
  let {memberList}=this.state
  var add=this.state.memberListID.includes(item.id)
  let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
  return(
      <View style={[styles.listItemview,{backgroundColor:'white',flexDirection:chnageLayout?'row':'row-reverse',alignItems:'center'}]}>
      <View style={[styles.col1,{paddingLeft:chnageLayout?15:0,backgroundColor:'white',width:'30%'}]}>
         <Image defaultSource={require('../assets/images/defaultprofile.png')} source={item.profile?{uri:item.profile}:require('../assets/images/defaultprofile.png')} style={[styles.logo2,!item.profile?{tintColor:'#ff7a00'}:null]} />
      </View>
      <View style={[styles.col2,{backgroundColor:'white',width:'45%'}]}>
          <Text style={{color:'black',fontSize:16,textAlign:chnageLayout?'left':'right',marginRight:chnageLayout?0:10}}>{item.name}</Text>
     </View>
     <View style={styles.col3}>
          <TouchableOpacity
          onPress={()=>this.addMember(item.id)}
           style={[{backgroundColor:add?'white':'#ff7a00'},styles.addbutton]}>
              <Text style={[{color:add?'#ff7a00':'white'},styles.addbtntxt]}>{I18n.t("add")}</Text>
          </TouchableOpacity>
     </View>
   </View>
  )
}
added(){
  if(this.state.memberListID.length > 0){
    var shareRef = firebase.database().ref().child('/shareLocation')
    this.state.memberListID.map((item)=>{
      shareRef.child(item).push({
        id:item,
        name:this.state.user.fullName,
        location:this.state.userLocation
      })
    })
  }
  this.setState({addMemberModal:false,memberListID:[]})
}
 addMemberModalView(){
  return(
  <Modal
    isVisible={this.state.addMemberModal}
    animationIn='slideInUp'
    animationOut='slideOutDown'
    onBackdropPress={()=>this.setState({addMemberModal:false,memberListID:[]})}
    onBackButtonPress={()=>this.setState({addMemberModal:false,memberListID:[]})}
    >
    <View style={{ flex: 1 ,backgroundColor:'white',borderRadius:5,padding:10}}>
      <View style={{backgroundColor:'#ff7a00',padding:10,borderRadius:5,flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'white',fontWeight:'bold'}}>{this.state.memberListID.length} Member Added</Text>
        <TouchableOpacity
        onPress={()=>this.added()}>
        <Text style={{color:'white',fontWeight:'bold'}}>{this.state.memberListID.length>0?'Done':'Cancel'}</Text>
        </TouchableOpacity>
      </View>
      <FlatList
            showsVerticalScrollIndicator={false}
            extraData={this.state}
            data={this.state.memberList}
            renderItem={({item,index})=>
            this.state.memberId!=item.id?this.renaderListItemModal(item,index):null}
            keyExtractor={(item,index)=>index.toString()}
        />
    </View>
  </Modal>
  )
}
chnageLangauge(language){
  I18n.locale = language;
  setAppLanguage(language)
  Actions.reset("Home")
}
  render() {
    let {user}=this.state
      return (
        <View style={styles.container}>
            <View style={{flexDirection:'row',width:'100%',flex:0.2,backgroundColor:'#f37c26',alignItems:'center',paddingHorizontal:20}}>
                <View style={{marginTop:10,backgroundColor:'white',borderRadius:50,justifyContent:'center',padding:1}}>
                    <Image source={{uri:user.image}} style={styles.logo} />
                </View>
                <Text style={{paddingLeft:10,color:'white',fontSize:18}}>
                 {user.fullName}
                </Text>
            </View>
            <View style={{backgroundColor:'#ff8a2a',flex:0.8,paddingVertical:15}}>
              <ScrollView contentContainerStyle={{paddingBottom:'20%'}}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}>
               {this.reanderButton(I18n.t('profile'),require('../assets/images/Profile_icon.png'),()=>this.open('profile'))}
               {this.reanderButton(I18n.t('change password'),require('../assets/images/Changepassword_icon.png'),()=>Actions.ChnagePassword())}
               {this.state.isAdmin?null:this.reanderButton(I18n.t('my transaction'),require('../assets/images/Mytransaction_icon.png'),()=>this.open('transaction'))}
               {/* {this.reanderButton(I18n.t('change language'),require('../assets/images/Languagechange_icon.png'),()=>this.open('changeLanguage',this.state.appLanguage == 'en' ? 'ar' : 'en'))} */}
               <View style={{width:'85%',borderColor:'#ffa049',borderBottomWidth:1,alignItems:'center',alignSelf:'center'}}>
                 <TouchableOpacity
                    onPress={()=>this.setState({changeLanguage:!this.state.changeLanguage})}
                    style={{flexDirection:'row',width:'100%',alignItems:'center',alignSelf:'center',paddingVertical:10}}>
                      <View style={{width:'15%',alignItems:'center'}}>
                      <Image source={require('../assets/images/Languagechange_icon.png')} style={{width:25,height:25}}/>
                    </View>
                    <View style={{width:'85%',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{paddingLeft:10,color:'white',fontSize:13,fontWeight:'bold'}}>{I18n.t('change language')}</Text>
                  </View>
                  </TouchableOpacity>
                  { this.state.changeLanguage?
                  <View style={{width:'85%',alignSelf:'flex-end'}}>
                  <TouchableOpacity
                    onPress={()=>this.chnageLangauge('en')}
                    style={{flexDirection:'row',width:'85%',alignItems:'center',alignSelf:'center',paddingVertical:10}}>
                    <View style={{width:'85%',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{paddingLeft:10,color:'white',fontSize:13,fontWeight:'bold'}}>ENGLISH</Text>
                  </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={()=>this.chnageLangauge('ar')}
                    style={{flexDirection:'row',width:'85%',alignItems:'center',alignSelf:'center',paddingVertical:10}}>
                    <View style={{width:'85%',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{paddingLeft:10,color:'white',fontSize:13,fontWeight:'bold'}}>ARABIC</Text>
                  </View>
                  </TouchableOpacity>
                  </View>:null}
               </View>
               {this.reanderButton(I18n.t('contact us'),require('../assets/images/contactus_icon.png'),()=>this.open('contact'))}
               {this.reanderButton(I18n.t('privacy policy'),null,()=>{Linking.openURL('https://premiumconnectapp.com/privacy-and-policy')})}
               {this.reanderButton(I18n.t('ems'),require('../assets/images/ems_icon.png'),()=>this.open('ems'))}
               {this.reanderButton(I18n.t('share location'),require('../assets/images/share_icon.png'),()=>this.setState({shareModal:true}))}
               </ScrollView>
               <TouchableOpacity onPress={()=>this._logout()} style={{width:'40%',borderRadius:50,backgroundColor:'white',padding:8,alignItems:'center',alignSelf:'center',position:'absolute',bottom:25,elevation:8}}> 
                  <Text style={{color:'#f37c26'}}>{I18n.t('signout')}</Text>
               </TouchableOpacity>
            </View>
          
            {this._shareLocationModal()}
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius:30
  },
  col1:{
    width:'60%',
    backgroundColor:'#f37c26',
    padding:8,
    flexDirection:'row',
    alignItems:'center'
  },
  col2:{
    width:'40%',
    backgroundColor:'#020304',
    padding:8,
    flexDirection:'row',
    alignItems:'center'
  },
  col3:{
    width:'25%',
    alignItems:'center'
  },
  logo2: {
    width: 50,
    height: 50,
    borderRadius:25
  },
  addbutton:{
    padding:2,
    width:'60%',
    borderWidth:1,
    borderColor:'#ff8a2a',
    borderRadius:50,
    alignItems:'center'
  },
  addbtntxt:{
    fontSize:12,
    fontWeight:'bold'
  },
});

