import React, { Component } from 'react';
import { Platform, StyleSheet, StatusBar, View, Image, TouchableOpacity } from 'react-native';
import { Header, Icon,} from 'native-base';

export default class Head extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDrawer: true
    }
  }

  openDrawer() {
    if (this.state.openDrawer) {
      this.props.openDrawer()
      this.setState({ openDrawer: false })
    }
    else {
      this.props.closeDrawer()
      this.setState({ openDrawer: true })
    }
  }
  render() {
    return (
      <View style={styles.Container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={this.props.openDrawer}
            >
              <Icon name='menu' type='Entypo' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '80%', alignItems: 'center' }}>
            <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
          </View>
          <View style={{ width: '10%', alignItems: 'center' }}>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  Container: {
    width: '100%',
  },
  logo: {
    marginTop: Platform.OS == 'ios' ? -3 : 3,
    width: 80,
    height: 50,
  },

});
