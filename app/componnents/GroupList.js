
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Animated
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { Icon, InputGroup, Tabs, Picker } from 'native-base'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import Moment from 'moment'
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'


export default class GroupList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      data: this.props.data,
      allData: this.props.data,
    }
   
  }
  componentWillMount() {
 
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

  }
  componentWillReceiveProps(nextProps){
   // // // console...log('Will recieve',this.props)
    this.setState({data:this.props.data})
  }

  renaderListItem(item, index, chnageLayout) {
  //  // console..log('data===============> ',item)
    return (
      item?
      <TouchableOpacity
        onPress={() => Actions.GroupDetail({ private: this.props.private, groupData: item })}
        style={[styles.itemView, { backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8' }, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
        <View style={styles.col1}>
          <Image source={{ uri: item.profile }} style={styles.logo} />
        </View>
        <View style={styles.col2}>
          <Text style={[styles.name, { textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }]}>{item.name}</Text>
          <Text style={[styles.member, { paddingRight: chnageLayout ? 0 : 10 }]}>{item.members + " " + I18n.t('member')}</Text>
        </View>
        <View style={styles.col4}>
        {item.notification? <Icon name='dot-single' type='Entypo' style={{color:'red',fontSize:45}}/>:null}
        </View>
        {this.props.mygroup ?
          <View style={[styles.col3, { alignItems: chnageLayout ? 'flex-end' : 'flex-start', padding: 10 }]}>
            <Text style={styles.grouptypetxt}>{item.groupType == 'PUBLIC' ? 'Public' : 'Private'} Group</Text>
          </View>
          : <View style={[styles.col3, { alignItems: chnageLayout ? 'flex-end' : 'flex-start' }]}>
            <Text style={styles.createdDate}>{Moment(item.createdDate).format("DD MMM")}</Text>
            {item.onlineMembers > 0 ? <View style={styles.tagView}>
              <Text style={styles.tagtxt}>{item.onlineMembers}{" " + I18n.t('member online')}</Text>
            </View> : null}
          </View>
        }
      </TouchableOpacity>:null
    )
  }
  render() {
   // // console..log('data===============> ',this.props.data)
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <View style={styles.subcontainner} >
          <View style={{ width: '100%', borderColor: '#c5c5c5', backgroundColor: 'white', elevation: 1, flexDirection: chnageLayout ? 'row' : 'row-reverse', paddingVertical: Platform.OS == 'ios' ? 10 : 0, paddingHorizontal: 5, borderWidth: 1, alignItems: 'center' }}>
            <TextInput
              style={[styles.searchInput, { textAlign: chnageLayout ? 'left' : 'right' }]}
              placeholder={I18n.t('search') + ".."}
              onChangeText={(text) => this.props.searchFilterFunction(text)}
            />
            <Icon name='search' type='FontAwesome' style={styles.searchicon} />
          </View>
          <FlatList
            extraData={this.state}
            data={this.state.data}
            renderItem={({ item, index }) => this.renaderListItem(item, index, chnageLayout)}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  itemView: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    width: '100%',
  },
  col1: {
    width: '20%'
  },
  col2: {
    width: '40%', marginLeft: 5
  },
  col3: {
    width: '30%',
    alignItems: 'flex-end'
  },
  col4: {
    width: '10%',
  },
  name: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold'
  },
  member: {
    color: '#ff7a00',
    fontSize: 12
  },
  createdDate: {
    fontSize: 12,
    margin: 2
  },
  tagView: {
    backgroundColor: '#ff7a00',
    padding: 1,
    paddingHorizontal: 5,
    borderRadius: 50,
    alignItems: 'center'
  },
  tagtxt: {
    color: 'white',
    fontSize: 10,
    fontWeight: 'bold'
  },
  grouptypetxt: {
    color: '#ff7a00',
    fontSize: 10,
    fontWeight: 'bold',
  },
  searchInput: {
    width: '90%'
  },
  subcontainner: {
    width: '100%',
    flex: 1
  },
  searchicon: {
    color: '#ff7a00'
  }
});
