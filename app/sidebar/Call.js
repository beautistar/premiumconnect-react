
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import Api from '../componnents/api'
import moment from 'moment'
import Contacts from 'react-native-contacts';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';

import uuid from 'uuid';
const defaultPic = require('../assets/images/defaultprofile.png')
export default class EMSCall extends Component {
  constructor(props) {
    super(props);
    this.state = {
   
    }
    
 }


  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header private={this.props.private} style={styles.header}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '90%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20, paddingLeft: -'10%' }}>{I18n.t('ems') + ' ' + I18n.t('call')}</Text>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <View style={{ flex: 1 }}>
        </View>
            <View style={styles.chatButtonMainView}>
            <TouchableOpacity
                onPress={() =>{}}
                style={styles.chatButton}>
                <Icon name='call' type='MaterialIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  privateGroupView: {
    backgroundColor: '#f37c26', paddingVertical: 4
  },
  privateGroupText: {
    color: 'white', textAlign: 'center', fontSize: 12
  },
  mapImage: {
    width: '100%', height: Dimensions.get("screen").height / 3.5
  },
  groupAdminMainView: {
    width: '90%', alignSelf: 'center', marginTop: -20, borderRadius: 5, overflow: 'hidden', elevation: 2
  },
  col1: {
    width: '60%', backgroundColor: '#f37c26', padding: 8, flexDirection: 'row'
  },
  col2: {
    width: '40%', backgroundColor: '#020304', padding: 8, flexDirection: 'row'
  },
  colTitle: {
    color: 'white', marginLeft: 5, fontSize: 12
  },

  chatButtonMainView: {
    position: 'absolute', bottom: 0, width: '100%', backgroundColor: 'rgba(350,350,350,0.6)', padding: 10
  },
  chatButton: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#f37c26',
    padding: 8,
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 8,
    marginBottom: 10
  },
  listMainView: {
    padding: 10, alignItems: 'center', width: '100%'
  }


});
