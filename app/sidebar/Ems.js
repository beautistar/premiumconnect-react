
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    StatusBar,
    AsyncStorage,
    PermissionsAndroid,
    Linking
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import {  Header, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import firebase from '../componnents/Firebase';
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import Api from '../componnents/api';
import MessageBar from '../componnents/messageBar'

var count=0, location
export default class Ems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deviceLanguage: 'en',
            appLanguage: 'en',
            name: 'Ricky Bahl',
            speed: '150',
            user: {},
            groupId:[]
        }
        navigator.geolocation.getCurrentPosition((position) => {
            // console.log('Current Location', position.coords.latitude, position.coords.longitude)
            location={latitude:position.coords.latitude,longitude:position.coords.longitude}
        }, (error) => {
            // console.log(error)
          },
            { enableHighAccuracy: false, timeout: 20000 }
          )
    }
   
    componentWillMount() {
        AsyncStorage.getItem('token')
        .then((token)=>{
            Api.get('/api/groupIds',token)
            .then((result)=>{
                // console.log('Get group ids',result)
                this.setState({groupId:result.data})
            })
        })
        AsyncStorage.getItem('userData').then(user => {
            // console.log("data0------sidebaer : ", JSON.parse(user))
            this.setState({ user: JSON.parse(user) })
        })
        getDeviceLanguage().then(lng => {
            this.setState({ deviceLanguage: lng })
        })
        getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
    }
    componentWillUnmount() {
        clearTimeout(timeout)
    }

    _sendEmergencyMessage() {
        // console.log('Count value==', count)

        if (this.state.groupId.length > 0) {
            var chatRef = firebase.database().ref().child('groupMessage')
            var ApiData = { groupId: this.state.groupId[count], message: I18n.t('emergencyMessage') }
            // console.log('Emergency message Data', ApiData)
            AsyncStorage.getItem('token')
                .then((token) => {
                    Api.postWithToken('/api/getDeviceId', { groupId: this.state.groupId[count]},token)
                    .then((result)=>{
                      // console.log('user id ',result) 
                      if(result.status=='Success'){
                          Api.postWithToken('/api/pushNotification', {
                           deviceids:result.deviceId,
                           title:result.title,
                           groupId: result.groupId,
                           groupType:result.groupType,
                           message:I18n.t('emergencyMessage')
                          },token)
                        .then((result)=>{
                            // console.log('Push notification api', result)
                            chatRef.child(this.state.groupId[count]+'/messages').push({
                                message: I18n.t('emergencyMessage'),
                                senderId: this.state.user._id,
                                profile: this.state.user.image,
                                fileType: 'location',
                                file: location,
                                createdAt: Date.now(),
                                id: Date.now()
                            })
                            if (this.state.groupId.length - 1 == count) {
                                count = 0
                                Api.get('/api/sendSms', token)
                                    .then((res) => {
                                        // console.log('SMS API', res)
                                        this.setState({ errorMsg: 'Your Location and SMS sent', color: 'green' }, () => this.msgBarRef._animateMessage())
                                    })
                            }
                            else {
                                count += 1
                                this._sendEmergencyMessage()
                            }

                        })
                      }
                    })
                })

        }
    }
    componentDidMount() {
        timeout = setTimeout(() => {
            this._sendEmergencyMessage()
        }, 10000)
    }

    render() {
        let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
        let { user } = this.state;
        return (
            <View style={styles.container}>
                <Header style={[styles.header]}>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()}
                        >
                            <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('ems')}</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                       
                    </View>
                </Header>
                <StatusBar backgroundColor='#020304' barStyle="light-content" />
                <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
                <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                    <Image source={user ? { uri: user.image } : require('../assets/images/defaultprofile.png')} style={{ height: 140, width: 140, borderRadius: 70, tintColor: user.image ? null : '#ff8a2a' }} />
                    <View style={{ alignItems: 'center', width: '80%', margin: 10 }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', margin: 5, color: 'black' }}>{user.fullName}</Text>
                        <Text style={{ color: '#ff8a2a', fontSize: 16, margin: 5 }}>Was Riding at a Speed of {this.props.speed}km/h</Text>
                        <View style={{ padding: 8, paddingHorizontal: 20, backgroundColor: '#ff8a2a', borderRadius: 50, width: '50%', alignItems: "center" }}>
                            <Text style={{ color: 'white' }}>Sudden STOP</Text>
                        </View>
                    </View>
                    <View style={{ width: '90%', alignItems: 'center', paddingVertical: 20 }}>
                        <Text style={{ fontSize: 30, color: 'black' }}>{I18n.t('are you alright !')}</Text>
                    </View>
                    <View style={{ marginVertical: 20, flexDirection: 'row', width: '40%', justifyContent: 'space-between' }}>
                        <TouchableOpacity
                            onPress={() => { clearTimeout(timeout), Actions.EmsMessage({ subscription: this.props.subscription }) }}
                            style={{ alignItems: 'center' }}>
                            <View style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: '#ff8a2a', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon name='ios-chatbubbles' type='Ionicons' style={{ color: 'white', fontSize: 30 }} />
                            </View>
                            <Text style={{ color: 'black', fontSize: 12 }}>{I18n.t('message')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => { clearTimeout(timeout), Actions.EmsCall({ subscription: this.props.subscription }) }}
                            style={{ alignItems: 'center' }}>
                            <View style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: '#ff8a2a', alignItems: 'center', justifyContent: 'center' }}>
                                <Icon name='phone-in-talk' type='MaterialCommunityIcons' style={{ color: 'white', fontSize: 30 }} />
                            </View>
                            <Text style={{ color: 'black', fontSize: 12 }}>{I18n.t('call')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        width: '100%',
        backgroundColor: '#020304',
        alignItems: 'center',
    },
    title: {
        color: 'black',
        fontSize: 20,
        paddingTop: 10,
        textAlign: 'center'
    },
    title2: {
        color: '#ff8a2a',
        fontSize: 20,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        width: '90%',
        backgroundColor: 'white',
        alignSelf: 'center',
        elevation: 3,
        borderRadius: 5,
        shadowOffset: { width: 0.5, height: 0.8 },
        shadowRadius: 4,
        shadowColor: 'black',
        shadowOpacity: 0.5,
    },
    cardheaderrow: {
        flexDirection: 'row',
        backgroundColor: '#ff8a2a',
        padding: 10,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    col: {
        width: '70%',
        paddingLeft: 5
    },
    title3: {
        fontSize: 15,
        color: 'white'
    },
    col2: {
        width: '30%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-around'
    },
    title4: {
        color: 'white',
        fontSize: 20
    },
    cardcontainer: {
        padding: 15,
        backgroundColor: 'white',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    listitemview: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    title5: {
        color: 'black',
        fontSize: 12,
        marginLeft: 10
    },
    subscribebtn: {
        width: '38%',
        borderRadius: 50,
        backgroundColor: '#f37c26',
        padding: 8,
        alignItems: 'center',
        alignSelf: 'center',
        elevation: 8,
        position: 'absolute',
        bottom: 30,
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowRadius: 4,
        shadowOpacity: 0.5,
    }
});
