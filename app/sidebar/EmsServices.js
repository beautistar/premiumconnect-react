
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    AsyncStorage,
    ActivityIndicator,
    Switch,
    PermissionsAndroid,
    Linking,
    Modal,
    Animated
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { Container, Header, Left, Body, Right, Button, Icon, Picker, } from 'native-base';
import I18n from 'react-native-i18n';
import firebase from '../componnents/Firebase';
import moment from 'moment'
import { setLayout } from '../componnents/Layout';
import Api from '../componnents/api';
import EMS from './Ems';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import MessageBar from '../componnents/messageBar'
import * as RNIap from 'react-native-iap';


var itemSkus = Platform.select({
    ios: [
        'sub.plan.premium.1month', 'sub.plan.premium.3month', 'sub.plan.premium.6month', 'sub.plan.premium.12month',
    ],
    android: [
        'plan.premium.1month', 'plan.premium.3month', 'plan.premium.6month', 'plan.premium.12month'
    ],
});

var userId, profile, location, count = 0
export default class EmsServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deviceLanguage: 'en',
            appLanguage: 'en',
            plan: '1month',
            subscriptions: [],
            basicAmount: '',
            premiumAmount: '',
            indicator: true,
            subscriber: false,
            user: {},
            emsService: false,
            subscriptionDetailsModal: false,
            selectedSubscription: '',
            selectedSubscriptionPrice: '',
            serviceModal: false,
            groupId: [],
            countDown: 5,
        }
        this.animatedValue = new Animated.Value(0)
        AsyncStorage.getItem('userData')
            .then((data) => {
                // console.log('UserData', JSON.parse(data))
                var userData = JSON.parse(data)
                userId = userData._id
                profile = userData.image
            })
        navigator.geolocation.getCurrentPosition((position) => {
            // console.log('Current Location', position.coords.latitude, position.coords.longitude)
            location = { latitude: position.coords.latitude, longitude: position.coords.longitude }
        }, (error) => {
            // console.log(error)
        },
            { enableHighAccuracy: false, timeout: 20000 }
        )
        this._buy = this._buy.bind(this)
    }
    _sendEmergencyMessage() {
        // console.log('Count value==', count)

        if (this.state.groupId.length > 0) {
            var chatRef = firebase.database().ref().child('groupMessage')

            var ApiData = { groupId: this.state.groupId[count], message: I18n.t('emergencyMessage') }
            // console.log('Emergency message Data', ApiData)
            AsyncStorage.getItem('token')
                .then((token) => {
                    Api.postWithToken('/api/getDeviceId', { groupId: this.state.groupId[count] }, token)
                        .then((result) => {
                            // console.log('user id ', result)
                            if (result.status == 'Success') {
                                Api.postWithToken('/api/pushNotification', {
                                    deviceids: result.deviceId,
                                    title: result.title,
                                    groupId: result.groupId,
                                    groupType: result.groupType,
                                    message: I18n.t('emergencyMessage')
                                }, token)
                                    .then((result) => {
                                        // console.log('send Notification ', result)
                                        chatRef.child(this.state.groupId[count] + '/messages').push({
                                            message: I18n.t('emergencyMessage'),
                                            senderId: userId,
                                            profile: profile,
                                            fileType: 'location',
                                            file: location,
                                            createdAt: Date.now(),
                                            id: Date.now()
                                        })
                                        if (this.state.groupId.length - 1 == count) {
                                            count = 0

                                        }
                                        else {
                                            count += 1
                                            this._sendEmergencyMessage()
                                        }
                                    })
                            }
                        })
                    // Api.postWithToken('/api/pushNotification', ApiData, token)
                    //     .then((result) => {
                    //         // console.log('Push notification api', result)


                    //     })
                })

        }
    }
    _timer() {
        // console.log('Timer start', this.state.subscriber)
        interval = setInterval(() => {
            this.setState({ countDown: this.state.countDown -= 1 })
        }, 1000)
        timeout = setTimeout(() => {
            this._sendEmergencyMessage()
            AsyncStorage.getItem('token')
                .then((token) => {
                    Api.postWithToken('/api/sendSms', { lat: location.latitude, long: location.longitude }, token)
                        .then((res) => {
                            // console.log('SMS API', res)
                            this.setState({ errorMsg: 'Your Location and SMS sent', color: 'green' }, () => this.msgBarRef._animateMessage())
                        })
                })
            clearInterval(interval)
            this.setState({ countDown: '' })
        }, 6000)
    }
    _animateTimeout() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 1000,

            }
        ).start(() => {
            if (this.state.countDown > 0) {
                this._animateTimeout()
            }
        })
    }
    async  componentDidMount() {
        const availablePurchases = await RNIap.getAvailablePurchases();
        // console.log("avialable subscription : ", availablePurchases)

        RNIap.getSubscriptions(itemSkus)
            .then((sub) => {
                // console.log('SUB', sub)
                var productlist = sub.filter(item => {
                    return itemSkus.includes(item.productId)
                })
                this.setState({ subscriptions: productlist })
                productlist.map((item) => {
                    var _subscriptionBasic, _subscriptionPremium
                    _subscriptionBasic = Platform.OS == 'ios' ? 'sub.plan.basic.' + this.state.plan : 'plan.basic.' + this.state.plan
                    _subscriptionPremium = Platform.OS == 'ios' ? 'sub.plan.premium.' + this.state.plan : 'plan.premium.' + this.state.plan

                    if (item.productId == _subscriptionBasic) {
                        this.setState({ basicAmount: item.localizedPrice })
                    }
                    if (item.productId == _subscriptionPremium) {
                        this.setState({ premiumAmount: item.localizedPrice })
                    }
                })
            })
        AsyncStorage.getItem('receipt')
            .then((data) => {
                if (data) {
                    // console.log('async receipt', data)
                    var rec = JSON.parse(data)
                    const receiptBody = {
                        'receipt-data': rec.transactionReceipt,
                        'password': 'be0040a4df734dafa66044acb1892286'
                    };
                    // console.log('reciept Body', receiptBody)
                    RNIap.validateReceiptIos(receiptBody, true)
                        .then((res) => {
                            // console.log('Validate', res.latest_receipt_info[res.latest_receipt_info.length - 1])
                            var time = moment(res.latest_receipt_info[res.latest_receipt_info.length - 1]).toISOString()
                            var _remainingTime = moment(time)
                            // console.log('Time', _remainingTime)
                        })
                }
            })
    }
    componentWillMount() {
        AsyncStorage.getItem('userData').then(user => {
            // console.log("data0------sidebaer : ", JSON.parse(user))
            this.setState({ user: JSON.parse(user) })
        })
        getDeviceLanguage().then(lng => {
            this.setState({ deviceLanguage: lng })
        })
        getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
        AsyncStorage.getItem('token')
            .then((token) => {
                Api.get('/api/groupIds', token)
                    .then((result) => {
                        // console.log('Get group ids', result)
                        this.setState({ groupId: result.data })
                    })
                Api.get('/api/EmsSubscription', token)
                    .then((result) => {
                        // console.log('Check Subscription Api Result', result)
                        if (result.data.subscription) {
                            this._timer()
                            this._animateTimeout()
                        }
                        this.setState({ subscriber: result.data.subscription, emsService: result.data.activeEmsService, indicator: false })
                    })
            })
        // setTimeout(()=>{
        //     this.setState({indicator:false,subscriber:true})
        // },5000)
    }
    renderListItem() {
        let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
        return (
            <View style={[styles.listitemview, { flexDirection: chnageLayout ? 'row' : 'row-reverse', }]}>
                <Icon name='ios-checkmark-circle-outline' type='Ionicons' style={{ color: '#ff8a2a', fontSize: 20 }} />
                <Text style={[styles.title5, { marginRight: chnageLayout ? 0 : 10 }]}>{I18n.t('service') + ' ' + I18n.t('list') + ' ' + I18n.t('one')}</Text>
            </View>
        )
    }
    async _buySubsctiption(type) {
        var _subscription = ''
        if (Platform.OS == 'ios') {
            _subscription = type == 'basic' ? 'sub.plan.basic.' + this.state.plan : 'sub.plan.premium.' + this.state.plan
        }
        else {
            _subscription = type == 'basic' ? 'plan.basic.' + this.state.plan : 'plan.premium.' + this.state.plan
        }

        // console.log('Subscription', _subscription)
        var purchase = await RNIap.buySubscription(_subscription)
        if (purchase) {
            // console.log('Subscription buy result', purchase)
            if (purchase) {
                AsyncStorage.getItem('token')
                    .then((token) => {
                        Api.put('/api/EmsPayment', { receipt: purchase,platform:Platform.OS=='ios'?'apple':'android' }, token)
                            .then((result) => {
                                AsyncStorage.setItem('receipt', JSON.stringify(purchase))
                                // console.log("Receipt Api Result", result)
                                if (result) {
                                    Actions.reset('Home')
                                }
                            })
                    })
            }
        }
    }
    _buy(buy) {
        if (buy) {
            this._buySubsctiption()
        }
    }
    _renderSubscription() {
        return (
            <View style={{ alignItems: 'center', marginVertical: 40 }}>
                {/* <View style={styles.planCard}>
                    <Text style={{ marginVertical: 10, color: 'black' }}>EMS Basic</Text>
                    <Text style={{ marginVertical: 10, color: '#ff8a2a', fontSize: 20 }}>{this.state.basicAmount}</Text>
                    <Text style={{ marginVertical: 10, textAlign: 'center', width: '80%', color: 'black' }}>Contact 112</Text>
                    <TouchableOpacity
                        //onPress={() => Actions.Ems()}
                        onPress={() => this.setState({subscriptionDetailsModal:true,selectedSubscription:'EMS Basic',selectedSubscriptionPrice:this.state.basicAmount})}
                        style={styles.subscribebtn}>
                        <Text style={{ color: 'white', fontSize: 12 }}>{I18n.t('subscribe') + ' ' + I18n.t('now')}</Text>
                    </TouchableOpacity>
                </View> */}
                <View style={styles.planCard}>
                    <Text style={{ marginVertical: 10, color: 'black' }}>EMS Premium</Text>
                    <Text style={{ marginVertical: 10, color: '#ff8a2a', fontSize: 20 }}>{this.state.premiumAmount}</Text>
                    <Text style={{ marginVertical: 10, textAlign: 'center', width: '80%', color: 'black' }}>SMS and Location will Sent Automatically</Text>
                    <TouchableOpacity
                        onPress={() => Actions.SubscriptionDetails({ selectedSubscription: 'EMS Premium', selectedSubscriptionPrice: this.state.premiumAmount, buy: this._buy })}
                        style={styles.subscribebtn}>
                        <Text style={{ color: 'white', fontSize: 12 }}>{I18n.t('subscribe') + ' ' + I18n.t('now')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    _selectSubscription(plan) {
        // console.log("plan : ", plan)
        this.setState({ plan: plan })
        this.state.subscriptions.map((item) => {
            // // console.log('iddd : ',Platform.OS=='ios'?'sub.plan.basic.'+plan:'plan.basic.'+plan)
            var basic = Platform.OS == 'ios' ? 'sub.plan.basic.' + plan : 'plan.basic.' + plan
            var premium = Platform.OS == 'ios' ? 'sub.plan.premium.' + plan : 'plan.premium.' + plan
            if (item.productId == basic) {
                // console.log('peice : ', item.localizedPrice)
                this.setState({ basicAmount: item.localizedPrice })
            }
            if (item.productId == premium) {
                this.setState({ premiumAmount: item.localizedPrice })
            }
        })

    }
    _changeServiceStatus() {
        AsyncStorage.getItem('token')
            .then((token) => {
                Api.put('/api/ActiveEmsService', { activeEmsService: this.state.emsService }, token)
                    .then((result) => {
                        // console.log('Active Ems service Api result', result)
                    })
            })
    }
    _selectServiceModal() {
        return (
            <Modal visible={this.state.serviceModal} transparent onRequestClose={() => { }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'white', padding: 10, justifyContent: 'space-between', alignItems: 'center', width: '70%', borderRadius: 5 }}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ serviceModal: false })
                            if (Platform.OS == 'android') {
                                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CALL_PHONE)
                                    .then(() =>
                                        PermissionsAndroid.request(
                                            PermissionsAndroid.PERMISSIONS.READ_CONTACTS
                                        ).then(() => {
                                            Actions.EmsCall({ message: 'I Need Help', subscription: this.props.subscription })
                                        })
                                    )
                            }
                            else {
                                Actions.EmsCall({ message: 'I Need Help', subscription: this.props.subscription })
                            }

                        }} style={{ borderWidth: 1, borderRadius: 5, padding: 10, width: '100%', borderColor: '#ff8a2a', alignItems: 'center', marginVertical: 3 }} >
                            <Text>Call</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.setState({ serviceModal: false }), Actions.EmsMessage({ message: 'I Need Help', subscription: this.props.subscription }) }} style={{ borderWidth: 1, borderRadius: 5, padding: 10, width: '100%', borderColor: '#ff8a2a', alignItems: 'center', marginVertical: 3 }}>
                            <Text>Message</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.setState({ serviceModal: false }) }} style={{ marginTop: 5 }}>
                            <Text style={{ color: '#ff8a2a', fontSize: 16 }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
    _imFine() {
        clearInterval(interval)
        this.setState({ countDown: '' })
        clearTimeout(timeout)
        this.setState({ errorMsg: 'OK', color: 'green' })
        this.msgBarRef._animateMessage()
    }
    _iNeedHelp() {
        clearInterval(interval)
        this.setState({ countDown: '' })
        clearTimeout(timeout)
        this.setState({ serviceModal: true })
    }
    render() {
        const opacity = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 1, 0]
        })
        const fontSize = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [65, 110, 65]
        })
        let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
        let { user } = this.state
        if (this.state.indicator) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator color='#000000' size='large' />
                </View>
            )
        }
        else {
            if (this.state.subscriber) {
                return (
                    // <EMS />
                    <View style={styles.container}>
                        <Header style={[styles.header]}>
                            <View style={{ width: '20%', }}>
                                <TouchableOpacity
                                    onPress={() => Actions.pop()}
                                >
                                    <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '60%', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('ems')}</Text>
                            </View>
                            <View style={{ width: '20%', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => Actions.AddContact()}><Text style={{ color: 'white', fontSize: 18 }}>{I18n.t('contact')}</Text></TouchableOpacity>
                            </View>
                        </Header>
                        <StatusBar backgroundColor='#020304' barStyle="light-content" />
                        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, justifyContent: 'space-between', backgroundColor: '#ff8a2a' }}>
                            <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>EMS Service</Text>
                            <Switch ios_backgroundColor='white' onValueChange={() => this.setState({ emsService: !this.state.emsService }, () => this._changeServiceStatus())} value={this.state.emsService} />
                        </View>
                        <View style={{ padding: 5, alignItems: 'center', justifyContent: 'center', height: Platform.OS == 'ios' ? 110 : 100, marginTop: 20 }}>
                            <Animated.Text style={{ fontSize: fontSize, fontWeight: 'bold', color: 'red', opacity: opacity }}>{this.state.countDown}</Animated.Text>
                        </View>
                        <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                            <Image source={user ? { uri: user.image } : require('../assets/images/defaultprofile.png')} style={{ height: 120, width: 120, borderRadius: 60, tintColor: user.image ? null : '#ff8a2a' }} />
                            <View style={{ alignItems: 'center', width: '80%', margin: 10 }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', margin: 5, color: 'black' }}>{user.fullName}</Text>
                            </View>
                            <View style={{ marginVertical: 20, width: '80%', alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this._imFine()}
                                    style={{ padding: 10, backgroundColor: 'green', width: '80%', alignItems: 'center', borderRadius: 5, margin: 5 }}>
                                    <Text style={{ fontSize: 18, color: 'white' }}>{I18n.t('i am fine')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this._iNeedHelp()} style={{ padding: 10, backgroundColor: 'red', width: '80%', alignItems: 'center', borderRadius: 5, margin: 5 }}>
                                    <Text style={{ fontSize: 18, color: 'white' }}>{I18n.t('i need help')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this._selectServiceModal()}
                    </View>
                )
            }
            else {
                return (
                    <View style={styles.container}>
                        <Header style={[styles.header]}>
                            <View style={{ width: '10%', alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => Actions.pop()}
                                >
                                    <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '80%', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('ems') + ' ' + I18n.t('service')}</Text>
                            </View>
                            <View style={{ width: '10%', alignItems: 'center' }}>
                            </View>
                        </Header>
                        <StatusBar backgroundColor='#020304' barStyle="light-content" />

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 20 }}>
                            <View style={{ width: '90%', alignItems: 'center', paddingLeft: '10%' }}>
                                <Text style={{ fontSize: 18, color: 'black', fontWeight: 'bold' }}>Subscribe to our</Text>
                                <Text style={{ fontSize: 18, color: "#ff8a2a", fontWeight: 'bold' }}>{I18n.t('ems') + ' ' + I18n.t('service')}</Text>
                            </View>
                            <Icon name='ios-checkmark-circle-outline' type='Ionicons' style={{ color: '#ff8a2a', fontSize: 20 }} />
                        </View>
                        <View style={{ paddingHorizontal: 10, marginVertical: 10 }}>
                            <View style={{ borderBottomWidth: 1, borderColor: '#ff8a2a', flexDirection: 'row', alignItems: 'center' }} >
                                <Icon name='calendar-clock' type='MaterialCommunityIcons' style={{ color: '#ff8a2a' }} />
                                <Picker mode='dropdown' selectedValue={this.state.plan} onValueChange={(plan) => this._selectSubscription(plan)}>
                                    <Picker.Item label='Choose Time Frame' value='Choose Time Frame' />
                                    <Picker.Item label='Monthly' value='1month' />
                                    <Picker.Item label='3 Months' value='3month' />
                                    <Picker.Item label='6 Months' value='6month' />
                                    <Picker.Item label='Annual' value='12month' />
                                </Picker>
                            </View>
                        </View>
                        {this._renderSubscription()}
                    </View>
                );
            }
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    header: {
        width: '100%',
        backgroundColor: '#020304',
        alignItems: 'center',
    },
    title: {
        color: 'black',
        fontSize: 20,
        paddingTop: 10,
        textAlign: 'center'
    },
    title2: {
        color: '#ff8a2a',
        fontSize: 20,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        width: '90%',
        backgroundColor: 'white',
        alignSelf: 'center',
        elevation: 3,
        borderRadius: 5,
        shadowOffset: { width: 0.5, height: 0.8 },
        shadowRadius: 4,
        shadowColor: 'black',
        shadowOpacity: 0.5,
    },
    cardheaderrow: {
        flexDirection: 'row',
        backgroundColor: '#ff8a2a',
        padding: 10,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    col: {
        width: '70%',
        paddingLeft: 5
    },
    title3: {
        fontSize: 15,
        color: 'white'
    },
    col2: {
        width: '30%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-around'
    },
    title4: {
        color: 'white',
        fontSize: 20
    },
    cardcontainer: {
        padding: 15,
        backgroundColor: 'white',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    listitemview: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    title5: {
        color: 'black',
        fontSize: 12,
        marginLeft: 10
    },
    subscribebtn: {
        width: '80%',
        borderRadius: 50,
        backgroundColor: '#f37c26',
        padding: 8,
        marginVertical: 10,
        alignItems: 'center',
        elevation: 8,
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowRadius: 4,
        shadowOpacity: 0.5,
    },
    planCard: {
        width: '45%',
        backgroundColor: '#f5f5f5',
        alignItems: 'center',
        elevation: 5,
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowRadius: 4,
        shadowOpacity: 0.5,
        justifyContent: 'space-between',
        borderRadius: 5,
        paddingHorizontal: 5
    }
});
