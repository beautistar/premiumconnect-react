
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  Dimensions,
  FlatList,
  SafeAreaView,
  KeyboardAvoidingView,
  Keyboard,
  AsyncStorage,
  PermissionsAndroid,
  Linking
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Container, Header, Left, Body, Right, Button, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import Api from '../componnents/api'
import Contacts from 'react-native-contacts';
const defaultPic = require('../assets/images/defaultprofile.png')

var location
export default class EMSCall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      selectNumber: '',
      Keyboard: false,
      phoneList: [],
      allMember: [],
      searched:[],
      message: this.props.message ? this.props.message : ''
    }
    navigator.geolocation.getCurrentPosition((position) => {
      // console.log('Current Location', position.coords.latitude, position.coords.longitude)
      location={latitude:position.coords.latitude,longitude:position.coords.longitude}
  }, (error) => {
      // console.log(error)
    },
      { enableHighAccuracy: false, timeout: 20000 }
    )
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
  }
  getContact() {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        // error
      } else {
        // console.log("contact : ", contacts)
        // contacts returned in Array
        contacts.map(contact => {
          if (contact.phoneNumbers.length > 0) {
            data.push({
              name: contact.givenName,
              phone: contact.phoneNumbers[0].number,
              profile: contact.hasThumbnail ? contact.thumbnailPath : false
            })
          }
        })
        data.sort((first,second)=>{
          var textA = first.name.toUpperCase();
          var textB = second.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })
        this.setState({ allMember: data })
      }
    })
  }
  componentWillMount() {
    data = []
    AsyncStorage.getItem('token').then(token => {
      //  data=[]
      Api.get('/api/phoneUsers', token).then(res => {
        // console.log("phone number ---- : ", res)
        if (res.status == 'Success') {
          res.data.map(member => {
            data.push({
              name: member.fullName,
              phone: member.phoneNumber,
              profile: member.image
            })
          })
          if (Platform.OS == 'android') {
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
              {
                'title': 'Contacts',
                'message': 'This app would like to view your contacts.'
              }
            ).then(() => {
              this.getContact()
            })
          }
          else {
            this.getContact()
          }

        }
      })
    })

    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

  }
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

  }
  _keyboardDidShow() {
    this.setState({ Keyboard: true })
  }
  _keyboardDidHide() {
    this.setState({ Keyboard: false })
  }

  addPhoneNumber(phone) {
    var phone = String(phone)
    var phoneList = this.state.phoneList
    // console.log("data4555656 ", this.state.phoneList)
    data = []
    data.push(this.state.phoneList)
    // console.log("hgeksgkkegjs ", this.state.phoneList)
    if (phoneList.includes(phone)) {
      phoneList.splice(phoneList.indexOf(phone), 1)
    }
    else {
      phoneList.push(phone)
    }
    this.setState({ phoneList: phoneList })
  }
  componentWillReceiveProps(nextprops) {
    // console.log('next props', nextprops)
  }
  renaderListItem(item, index, admin) {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    let { selectNumber } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.addPhoneNumber(item.phone)}
        style={[styles.listMainView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8', alignSelf: 'center' }]}>
        <View style={{ width: '20%' }}>
          <Image source={item.profile ? { uri: item.profile } : defaultPic} style={styles.logo} />
        </View>
        <View style={{ width: '50%', marginLeft: 5 }}>
          <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
        </View>
        <View style={{ width: '30%', alignItems: 'flex-end', paddingHorizontal: 22 }}>
          <Icon name='ios-checkmark-circle-outline' style={{ color: this.state.phoneList.includes(String(item.phone)) ? '#ff7a00' : 'grey' }} />
        </View>
      </TouchableOpacity>
    )
  }
  _preMessages(message) {
    return (
      <TouchableOpacity
        onPress={() => this.setState({ message: message })}
        style={{ borderRadius: 50, borderWidth: 1, borderColor: '#ff7a00', padding: 5, paddingHorizontal: 10, marginRight: 2, marginBottom: 5 }}>
        <Text>{message}</Text>
      </TouchableOpacity>
    )
  }
  _sendMessage() {
    // console.log("phone number list : ", this.state.phoneList)
    /// Linking.openURL(`sms:/open?addresses=923335251661,9231213341&body=${this.state.message}`)
    var phoneNumbers = ''
    this.state.phoneList.map((item, index) => {
      if (index == this.state.phoneList.length - 1)
        phoneNumbers = phoneNumbers + item
      else
        phoneNumbers = phoneNumbers + item + ','
    })
    // console.log("phone list : ", phoneNumbers)
    if (phoneNumbers) {
      if (Platform.OS == 'ios') {
        //  Communications.text(['+9184012090','112'],this.state.message)
        Linking.openURL(`sms:/open?addresses=${phoneNumbers}&body=${this.state.message} \n https://www.google.com/maps/dir//${location.latitude},${location.longitude}`)
      }
      else {
        Linking.openURL(`sms:${phoneNumbers}?body=${this.state.message}  \n https://www.google.com/maps/dir//${location.latitude},${location.longitude}`)
        // SendSMS.send(123, "+9184012090", this.state.message,
        // (msg)=>{
        //   // console.log("msg : ",msg)
        // }) 
      }
    }
  }
  _search(text){
    var filtered
    if(text){
      filtered = this.state.allMember.filter((item)=>item.name.toLowerCase().includes(text.toLowerCase()))
      this.setState({searched:filtered})
    }
    else{
      this.setState({searched:[]})
    }
  }
  render() {
    // console.log("data : ", this.state.all)
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header private={this.props.private} style={styles.header}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '80%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20, paddingLeft: -'10%' }}>{I18n.t('ems') + ' ' + I18n.t('message')}</Text>
          </View>
          <View style={{ width: '10%', alignItems: 'center' }}>
            {/* <TouchableOpacity onPress={()=> Actions.pop()}>
                <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{color:'white'}} />
            </TouchableOpacity> */}
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:5,justifyContent:'space-around'}}>
            <Icon name='search' type='MaterialIcons' style={{color:'#ff7a00'}} />
            <TextInput placeholder='Search' style={{width:'90%',height:Platform.OS == 'ios' ? 45 : null, marginLeft:Platform.OS == 'ios' ? 5 : 0}} onChangeText={(text)=>this._search(text)} />
        </View>
        <SafeAreaView style={{ flex: 1 }}>
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={styles.privateGroupView}>
                  <Text style={styles.privateGroupText}>Select Contact To Send Message</Text>
                </View>
                {/* <View style={{ width: '95%', alignSelf: 'center', marginTop: 15 }}>
                  <View
                    style={[styles.listMainView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: '#ff7a00', borderRadius: 25, paddingHorizontal: 10 }]}>
                    <View style={{ width: '70%' }}>
                      <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>Emergency Number</Text>
                    </View>
                    <View style={{ width: '30%', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
                      <Text style={{ color: 'white' }}>112</Text>
                      <Icon name='ios-checkmark-circle-outline' style={{ color: 'white' }} />
                    </View>
                  </View>
                </View> */}
                  <FlatList
                    contentContainerStyle={{ paddingBottom: this.state.Keyboard ? '20%' : Platform.OS == 'android' ? '40%' : '50%' }}
                    extraData={this.state}
                    data={this.state.searched.length > 0 ? this.state.searched : this.state.allMember}
                    renderItem={({ item, index }) => this.renaderListItem(item, index)}
                    keyExtractor={(item, index) => index.toString()}
                  /> 
              </View>

              <View style={{ borderTopWidth: 1, borderColor: '#c9c7c6', backgroundColor: 'white', position: 'absolute', bottom: Platform.OS == 'ios' ? this.state.Keyboard ? 65 : 0 : 0 }}>
                {!this.state.Keyboard ? <View style={{ paddingHorizontal: 10, paddingTop: 10, width: '100%', flexDirection: 'row', flexWrap: 'wrap', borderBottomWidth: 1, borderColor: '#ff7a00', paddingBottom: 5 }}>
                  {this._preMessages(I18n.t('msg1'))}
                  {this._preMessages(I18n.t('msg2'))}
                </View> : null}
                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 5 }}>
                  <TextInput
                    style={{ width: '85%', paddingLeft: 10 }}
                    placeholder='Type a Message'
                    onChangeText={(text) => this.setState({ message: text })}
                    value={this.state.message}
                  />
                  <TouchableOpacity onPress={() => this._sendMessage()} style={{ width: '15%', alignItems: 'center' }}><Image source={require('../assets/images/Chat_btn.png')} style={{ width: 40, height: 40 }} /></TouchableOpacity>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor:'#ffffff',
    // justifyContent: 'center',
    backgroundColor: 'white',
    //alignItems: 'center',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  privateGroupView: {
    backgroundColor: '#f37c26', paddingVertical: 4
  },
  privateGroupText: {
    color: 'white', textAlign: 'center', fontSize: 12
  },
  mapImage: {
    width: '100%', height: Dimensions.get("screen").height / 3.5
  },
  groupAdminMainView: {
    width: '90%', alignSelf: 'center', marginTop: -20, borderRadius: 5, overflow: 'hidden', elevation: 2
  },
  col1: {
    width: '60%', backgroundColor: '#f37c26', padding: 8, flexDirection: 'row'
  },
  col2: {
    width: '40%', backgroundColor: '#020304', padding: 8, flexDirection: 'row'
  },
  colTitle: {
    color: 'white', marginLeft: 5, fontSize: 12
  },
  chatButtonMainView: {
    position: 'absolute', bottom: 0, width: '100%', backgroundColor: 'rgba(350,350,350,0.6)', padding: 10
  },
  chatButton: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#f37c26',
    padding: 8,
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 8,
    marginBottom: 10
  },
  listMainView: {
    padding: 10, alignItems: 'center', width: '100%'
  }


});
