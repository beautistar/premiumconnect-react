
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Animated,
  AsyncStorage,

} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import Api from '../componnents/api';
import MessageBar from '../componnents/messageBar';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon } from 'native-base';

export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      errorMsg: '',
      profile: '',
      height: new Animated.Value(0),
      opacity: new Animated.Value(0),
      openModal: false,
      color: 'red'
    }
  }
  componentWillMount() {

    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

    AsyncStorage.getItem('userData').then(user => {
      // console.log("data0------sidebaer : ", JSON.parse(user))
      user = JSON.parse(user)
      this.setState({ profile: user.image })
    })
  }

  _Save() {

    if (this.state.oldPassword && this.state.newPassword && this.state.confirmPassword) {
      if (this.state.newPassword == this.state.confirmPassword) {
        var apiData = {
          password: this.state.oldPassword,
          newPassword: this.state.newPassword
        }
        AsyncStorage.getItem('token')
          .then((token) => {
            Api.put('/api/changePassword', apiData, token)
              .then((result) => {
                // console.log('REslult', result)
                if (result.status == "Success") {
                  this.setState({ errorMsg: 'Password Changed Successfully.', color: 'green', oldPassword: '', newPassword: '', confirmPassword: '' })
                  this.msgBarRef._animateMessage()
                }
                else {
                  this.setState({ errorMsg: result.message, color: 'red' })
                  this.msgBarRef._animateMessage()
                }

              })
          })
      }
      else {
        this.setState({ errorMsg: 'Confirm Password & New Password must be same', color: 'red' })
        this.msgBarRef._animateMessage()
      }
    }
    else {
      this.setState({ errorMsg: 'Please fill all the details', color: 'red' })
      this.msgBarRef._animateMessage()
    }
  }

  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => Actions.pop()}
            >
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '90%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('change password')}</Text>
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.subcontainer}>

            <Image defaultSource={require('../assets/images/defaultprofile.png')} source={this.state.profile ? { uri: this.state.profile } : require('../assets/images/defaultprofile.png')} style={[styles.logo, { tintColor: this.state.profile ? null : '#ffa433' }]} />

            <View style={styles.box}>

              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/lock.png')} style={styles.loackicon} />
                <TextInput
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  value={this.state.oldPassword}
                  onChangeText={(text) => this.setState({ oldPassword: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('old password')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/lock.png')} style={styles.loackicon} />
                <TextInput
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  value={this.state.newPassword}
                  onChangeText={(text) => this.setState({ newPassword: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('new password')}
                />
              </View>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/lock.png')} style={styles.loackicon} />
                <TextInput
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  value={this.state.confirmPassword}
                  onChangeText={(text) => this.setState({ confirmPassword: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                  placeholder={I18n.t('confirm password')}
                />
              </View>

              <TouchableOpacity onPress={() => this._Save()} style={{ backgroundColor: '#ffa433', marginVertical: 20, padding: 10, width: '50%', alignItems: 'center', borderRadius: 5 }}>
                <Text style={{ color: 'white', fontSize: 18 }}>{I18n.t('change')}</Text>
              </TouchableOpacity>

            </View>
          </View>
        </ScrollView>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    //height: Dimensions.get('window').height - 24,
    justifyContent: 'center',
    padding: 20
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 20
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '85%',
    height: 40,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  loackicon: {
    width: 45,
    height: 35,
    tintColor: '#ffa433'
  },


});
