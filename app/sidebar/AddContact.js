
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
  TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import Api from '../componnents/api'
import Contacts from 'react-native-contacts';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import Modal from "react-native-modal";
import CountryCodeList from 'react-native-country-code-list'
import MessageBar from '../componnents/messageBar'

const defaultPic = require('../assets/images/defaultprofile.png')

export default class AddContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      addcontactmodal:false,
      countryCode: '+1',
      countryCode2:'+1',
      openModal:false,
      openModal2:false,
      selectNumber: '',
      phoneNumber:'',
      phoneNumber2:'',
      fullName:'',
      fullName2:'',
      errorMsg: '',
      color:'red',
      contacts:[],
     
    }
  }
  
  getContact(){
    AsyncStorage.getItem('token').then(token => {
      Api.get('/api/GetEmergencyContact',token).then(res => {
        // console.log("get number ---- : ", res)
        if(res.status=="Success"){
          this.setState({contacts:res.data})
          var contacts = res.data
            var l = contacts.length
            if(l==2){
              this.setState({
                fullName:contacts[0].name,
                countryCode:contacts[0].countryCode,
                phoneNumber:contacts[0].phoneNumber,
                fullName2:contacts[1].name,
                countryCode2:contacts[1].countryCode,
                phoneNumber2:contacts[1].phoneNumber
              })
            }
            else if(l==1){
              this.setState({
                fullName:contacts[0].name,
                countryCode:contacts[0].countryCode,
                phoneNumber:contacts[0].phoneNumber,
                fullName2:'',
                countryCode2:'+1',
                phoneNumber2:''
              })
            }
     
        }
        else{
          this.setState({contacts:[]})
        }
      })
    })
  }
  componentWillMount() {
   this.getContact()
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

  }
   addNumber(){
     let {phoneNumber,phoneNumber2,fullName,fullName2,countryCode,countryCode2}=this.state
     if(phoneNumber && phoneNumber2 && fullName && fullName2){
       if(this.state.contacts.length ==1){
        data=[
          {name:fullName2, countryCode:countryCode2,phoneNumber:phoneNumber2}
        ]
       }
       else{
        data=[
          {name:fullName, countryCode:countryCode,phoneNumber:phoneNumber},
          {name:fullName2, countryCode:countryCode2,phoneNumber:phoneNumber2}
        ]
       }
       
      AsyncStorage.getItem('token').then(token => {
        Api.put('/api/AddEmergencyContact',{emergencyContacts:data}, token).then(res => {
          // console.log("phone number ---- : ", res)
          if(res.status=="Success"){
            setTimeout(()=>{
              this.setState({addcontactmodal:false})
              this.getContact()
            },3000)
            this.setState({ errorMsg: 'Sucessfull Added Contact' ,color:'green'})
            this.msgBarRef._animateMessage()
          }
        })
      })
     }
     else{
      this.setState({ errorMsg: 'Please Enter all Details' })
      this.msgBarRef._animateMessage()
     }
   }
   addcontactsModale(){
     let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
     return(
      <Modal
      isVisible={this.state.addcontactmodal}
      animationIn='slideInUp'
      animationOut='slideOutDown'
      onBackdropPress={() => this.setState({ addcontactmodal: false })}
      onBackButtonPress={() => this.setState({ addcontactmodal: false })}
      >
       <View style={{ backgroundColor: 'white', borderRadius: 5,overflow:'hidden' }}>
       <Text style={{padding:10,fontWeight:'bold'}}>Contact 1</Text>
              <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                <Image source={require('../assets/images/Name_icon.png')} style={styles.nameicon} />
                <TextInput
                  autoCapitalize = 'none'
                  onChangeText={(text) => this.setState({ fullName: text })}
                  style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '90%' }]}
                  placeholder='Full Name'
                  value={this.state.fullName}
                  editable={!this.state.contacts.length==1 || !this.state.contacts.length==2}
                />
              </View>
               <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                    <Image source={require('../assets/images/Otp_Phone_icon.png')} style={styles.phoneicon} />
                    <TouchableOpacity   disabled={this.state.contacts.length==1 || this.state.contacts.length==2} style={{ marginRight: 10,paddingHorizontal:10 }} onPress={() => this.setState({ openModal: true })}>
                      <Text>{this.state.countryCode}</Text>
                    </TouchableOpacity>
                    <TextInput
                      editable={!this.state.contacts.length==1 || !this.state.contacts.length==2}
                      keyboardType='phone-pad'
                      autoCapitalize = 'none'
                      maxLength={10}
                      value={this.state.phoneNumber.toString()}
                      style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '70%' }]}
                      placeholder='Phone Number 1'
                      onChangeText={(value) => this.setState({ phoneNumber: value })}
                    
                    />
                  </View>
                  <Text style={{padding:10,fontWeight:'bold'}}>Contact 2</Text>
                  <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                  <Image source={require('../assets/images/Name_icon.png')} style={styles.nameicon} />
                  <TextInput
                    autoCapitalize = 'none'
                    editable={!this.state.contacts.length==2}
                    onChangeText={(text) => this.setState({ fullName2: text })}
                    style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '90%' }]}
                    placeholder='Full Name'
                    value={this.state.fullName2}
                  />
                </View>
                  <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                    <Image source={require('../assets/images/Otp_Phone_icon.png')} style={styles.phoneicon} />
                    <TouchableOpacity   disabled={this.state.contacts.length==2} style={{ marginRight: 10,paddingHorizontal:10 }} onPress={() => this.setState({ openModal2: true })}>
                      <Text>{this.state.countryCode2}</Text>
                    </TouchableOpacity>
                    <TextInput
                      editable={!this.state.contacts.length==2}
                      maxLength={10}
                      keyboardType='phone-pad'
                      autoCapitalize = 'none'
                      style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '70%' }]}
                      placeholder='Phone Number 2'
                      onChangeText={(value) => this.setState({ phoneNumber2: value })}
                      value={this.state.phoneNumber2.toString()}
                    />
                  </View>
                  <TouchableOpacity disabled={this.state.contacts.length==2} onPress={()=>this.addNumber()} style={{backgroundColor:'#f37c26',justifyContent:'center',alignItems:'center',marginTop:20,padding:10}}>
                      <Text style={{color:'white',fontWeight:'bold'}}>Add</Text>
                  </TouchableOpacity>
       </View>
       <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} borderRadius={10}/>
       <Modal
            visible={this.state.openModal}
            onBackButtonPress={() => this.setState({ openModal: false })}
          >
            <View style={{ backgroundColor: 'white', flex: 1 }}>
              <TouchableOpacity
                style={{ right: 10, padding: 10, marginTop: Platform.OS == 'ios' ? 30 : 20, position: 'absolute' }}
                onPress={() => this.setState({ openModal: false })}>
                <Text>Cancel</Text>
              </TouchableOpacity>
              <CountryCodeList
                sectionHeaderHeight={0}
                onClickCell={(country) => {
                  this.setState({
                    countryCode: country.code,
                    openModal: false
                  })
                  // console.log(country)
                }
                }
              />
            </View>
          </Modal>
          <Modal
            visible={this.state.openModal2}
            onBackButtonPress={() => this.setState({ openModal2: false })}
          >
            <View style={{ backgroundColor: 'white', flex: 1 }}>
              <TouchableOpacity
                style={{ right: 10, padding: 10, marginTop: Platform.OS == 'ios' ? 30 : 20, position: 'absolute' }}
                onPress={() => this.setState({ openModal2: false })}>
                <Text>Cancel</Text>
              </TouchableOpacity>
              <CountryCodeList
                sectionHeaderHeight={0}
                onClickCell={(country) => {
                  this.setState({
                    countryCode2: country.code,
                    openModal2: false
                  })
                  // console.log(country)
                }
                }
              />
            </View>
          </Modal>
    </Modal>
     )
   }
   removeContact(id){
     // console.log("user id : ",id)
    AsyncStorage.getItem('token').then(token => {
      Api.put('/api/RemoveEmergencyContact',{removeContactId:id},token).then(res => {
        // console.log("remove number ---- : ", res)
        if(res.status=="Success"){
          this.getContact()
        }
      })
    })
   }
  renaderListItem(item, index) {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <TouchableOpacity
        style={[styles.listMainView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8', alignSelf: 'center' }]}>
        <View style={{ width: '20%' }}>
          <Image source={defaultPic} style={styles.logo} />
        </View>
        <View style={{ width: '50%', marginLeft: 5 }}>
          <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
        </View>
        <View style={{ width: '30%', alignItems: 'flex-end', paddingHorizontal: 22 }}>
         <TouchableOpacity onPress={()=>this.removeContact(item._id)}><Text style={{color:'red',fontWeight:'bold'}}>{I18n.t('remove')}</Text></TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }
  
  render() {
   
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header private={this.props.private} style={styles.header}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '80%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20}}>{I18n.t('ems')+' '+I18n.t('contact')}</Text>
          </View>
          <View style={{ width: '10%', alignItems: 'center' }}>
          <TouchableOpacity onPress={()=>{this.setState({addcontactmodal:true})}}><Text style={{ color: 'white', fontSize: 15}}>{I18n.t('add')}</Text></TouchableOpacity> 
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
    
        <View style={{ flex: 1 }}>
          {/* <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:5,justifyContent:'space-around',borderBottomWidth:2,borderBottomColor:'#ff7a00'}}>
            <Icon name='search' type='MaterialIcons' style={{color:'#ff7a00'}} />
            <TextInput placeholder='Search' style={{width:'90%',height:Platform.OS == 'ios' ? 45 : null, marginLeft:Platform.OS == 'ios' ? 5 : 0}} onChangeText={(text)=>this._search(text)} />
          </View> */}
          <View>
              <FlatList
                contentContainerStyle={{ paddingBottom: '40%' }}
                extraData={this.state}
                data={this.state.contacts}
                renderItem={({ item, index }) => this.renaderListItem(item, index)}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={<View>
                  <Text style={{textAlign:'center',marginTop:20,color:'black'}}>You dose not have any Emergancy EMS Contacts</Text>
                  </View>}
              /> 
          </View>
        
          {this.addcontactsModale()}
        </View>
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  privateGroupView: {
    backgroundColor: '#f37c26', paddingVertical: 4
  },
  privateGroupText: {
    color: 'white', textAlign: 'center', fontSize: 12
  },
  mapImage: {
    width: '100%', height: Dimensions.get("screen").height / 3.5
  },
  groupAdminMainView: {
    width: '90%', alignSelf: 'center', marginTop: -20, borderRadius: 5, overflow: 'hidden', elevation: 2
  },
  col1: {
    width: '60%', backgroundColor: '#f37c26', padding: 8, flexDirection: 'row'
  },
  col2: {
    width: '40%', backgroundColor: '#020304', padding: 8, flexDirection: 'row'
  },
  colTitle: {
    color: 'white', marginLeft: 5, fontSize: 12
  },
  listMainView: {
    padding: 10, alignItems: 'center', width: '100%'
  }
  ,
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '80%',
    borderRadius: 5,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  inputbox: {
    width:'100%',
    fontWeight: 'bold',
    fontSize: 12,
    height:35,
    color: 'black',
    borderBottomWidth:1,
    borderBottomColor:'grey'
  },
  nameicon: {
    width: 20,
    height: 20,
    marginHorizontal: 10
  },
});
