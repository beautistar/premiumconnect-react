
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  FlatList,
  AsyncStorage,
  PermissionsAndroid,
  TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import Api from '../componnents/api'
import Contacts from 'react-native-contacts';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';

const defaultPic = require('../assets/images/defaultprofile.png')

export default class EMSCall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'ar',
      appLanguage: 'ar',
      selectNumber: '',
      allMember: [],
      searched:[]
    }
  }
  getContact() {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {

      } else {
       // // console.log("contact : ", contacts)
        contacts.map(contact => {
          if (contact.phoneNumbers.length > 0) {

            data.push({
              name: contact.givenName,
              phone: contact.phoneNumbers[0].number,
              profile: contact.hasThumbnail ? contact.thumbnailPath : false
            })
          }
        })
        data.sort((first,second)=>{
          var textA = first.name.toUpperCase();
          var textB = second.name.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })
        this.setState({ allMember: data })
      }
    })
  }
  componentWillMount() {
    data = []
    AsyncStorage.getItem('token').then(token => {
      Api.get('/api/phoneUsers', token).then(res => {
        // console.log("phone number ---- : ", res)
        if (res.status == 'Success') {
          res.data.map(member => {
            data.push({
              name: member.fullName,
              phone: member.phoneNumber,
              profile: member.image
            })
          })
          if (Platform.OS == 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CALL_PHONE, {
              'title': 'Call Permission',
              'message': 'This app requires Call Permission to make call'
            })
      
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
              {
                'title': 'Contacts',
                'message': 'This app would like to view your contacts.'
              }
            ).then(() => {
              this.getContact()
            })
          }
          else {
            this.getContact()
          }
        }
      })
    })

    
    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

  }
  
  renaderListItem(item, index) {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <TouchableOpacity
        onPress={() => this.setState({ selectNumber: item.phone })}
        style={[styles.listMainView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: index % 2 == 0 ? 'white' : '#f8f8f8', alignSelf: 'center' }]}>
        <View style={{ width: '20%' }}>
          <Image source={item.profile ? { uri: item.profile } : defaultPic} style={styles.logo} />
        </View>
        <View style={{ width: '50%', marginLeft: 5 }}>
          <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>{item.name}</Text>
        </View>
        <View style={{ width: '30%', alignItems: 'flex-end', paddingHorizontal: 22 }}>
          <Icon name='ios-checkmark-circle-outline' style={{ color: this.state.selectNumber == item.phone ? '#f37c26' : 'grey' }} />
        </View>
      </TouchableOpacity>
    )
  }
  _search(text){
    var filtered
    if(text){
      filtered = this.state.allMember.filter((item)=>item.name.toLowerCase().includes(text.toLowerCase()))
      this.setState({searched:filtered})
    }
    else{
      this.setState({searched:[]})
    }
  }
  render() {

    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
        <Header private={this.props.private} style={styles.header}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '80%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20, paddingLeft: -'10%' }}>{I18n.t('ems') + ' ' + I18n.t('call')}</Text>
          </View>
          <View style={{ width: '10%', alignItems: 'center' }}>
           
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:5,justifyContent:'space-around'}}>
            <Icon name='search' type='MaterialIcons' style={{color:'#ff7a00'}} />
            <TextInput placeholder='Search' style={{width:'90%',height:Platform.OS == 'ios' ? 45 : null, marginLeft:Platform.OS == 'ios' ? 5 : 0}} onChangeText={(text)=>this._search(text)} />
          </View>
          <View style={styles.privateGroupView}>
            <Text style={styles.privateGroupText}>You Can Call Just 1 Person At a Time</Text>
          </View>
          {/* <View style={{ width: '95%', alignSelf: 'center', marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ selectNumber: '112' })}
              style={[styles.listMainView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', backgroundColor: '#ff7a00', borderRadius: 25, paddingHorizontal: 10 }]}>
              <View style={{ width: '70%' }}>
                <Text style={{ color: 'white', fontSize: 16, fontWeight: 'bold', textAlign: chnageLayout ? 'left' : 'right', paddingRight: chnageLayout ? 0 : 10 }}>Emergency Number</Text>
              </View>
              <View style={{ width: '30%', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
                <Text style={{ color: 'white' }}>112</Text>
                <Icon name='ios-checkmark-circle-outline' style={{ color: 'white' }} />
              </View>
            </TouchableOpacity>
          </View> */}
          <View>
              <FlatList
                contentContainerStyle={{ paddingBottom: '40%' }}
                extraData={this.state}
                data={this.state.searched.length > 0 ? this.state.searched : this.state.allMember}
                renderItem={({ item, index }) => this.renaderListItem(item, index)}
                keyExtractor={(item, index) => index.toString()}
              /> 
          </View>
        </View>
        <View style={styles.chatButtonMainView}>
          <TouchableOpacity
            onPress={() => { RNImmediatePhoneCall.immediatePhoneCall(String(this.state.selectNumber)) }}
            style={styles.chatButton}>
            <Icon name='call' type='MaterialIcons' style={{ color: 'white' }} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  subcontainner: {
    flex: 1,
    alignItems: 'center',
    marginTop: '25%'
  },
  title: {
    color: 'black',
    fontSize: 18
  },
  title2: {
    color: '#ff7a00',
    fontSize: 13
  },
  profileBtnview: {
    marginTop: 20
  },
  privateGroupView: {
    backgroundColor: '#f37c26', paddingVertical: 4
  },
  privateGroupText: {
    color: 'white', textAlign: 'center', fontSize: 12
  },
  mapImage: {
    width: '100%', height: Dimensions.get("screen").height / 3.5
  },
  groupAdminMainView: {
    width: '90%', alignSelf: 'center', marginTop: -20, borderRadius: 5, overflow: 'hidden', elevation: 2
  },
  col1: {
    width: '60%', backgroundColor: '#f37c26', padding: 8, flexDirection: 'row'
  },
  col2: {
    width: '40%', backgroundColor: '#020304', padding: 8, flexDirection: 'row'
  },
  colTitle: {
    color: 'white', marginLeft: 5, fontSize: 12
  },

  chatButtonMainView: {
    position: 'absolute', bottom: 0, width: '100%', backgroundColor: 'rgba(350,350,350,0.6)', padding: 10
  },
  chatButton: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#f37c26',
    padding: 8,
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 8,
    marginBottom: 10
  },
  listMainView: {
    padding: 10, alignItems: 'center', width: '100%'
  }


});
