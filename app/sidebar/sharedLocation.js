
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    AsyncStorage,
    PermissionsAndroid,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { Container, Header, Left, Body, Right, Button, Icon, } from 'native-base';
import I18n from 'react-native-i18n';
import { setLayout } from '../componnents/Layout';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

export default class SharedLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deviceLanguage: 'en',
            appLanguage: 'en',
            userLocation: {}
        }
    }
    componentDidMount() {
        this.setState({ userLocation: this.props.userLocation })
    }

    render() {
        let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
        let { user } = this.state;
        return (
            <View style={styles.container}>
                <Header style={[styles.header]}>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                        <TouchableOpacity
                            onPress={() => Actions.pop()}
                        >
                            <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('shared location')}</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center' }}>
                        {/* <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('ems')}</Text> */}
                    </View>
                </Header>
                <StatusBar backgroundColor='#020304' barStyle="light-content" />
                <MapView
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={{ height: Dimensions.get('window').height, width: '100%' }}
                    region={this.state.userLocation}
                    loadingEnabled={true}
                    toolbarEnabled={true}
                >
                    <Marker coordinate={this.state.userLocation}
                    //draggable={true}
                    //onDrag={(e)=>this.setState({pinLocation:e.nativeEvent.coordinate})}
                    />
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //backgroundColor:'#ffffff',
        // justifyContent: 'center',
        backgroundColor: 'white',
        //alignItems: 'center',
    },
    header: {
        width: '100%',
        backgroundColor: '#020304',
        alignItems: 'center',
    },
    title: {
        color: 'black',
        fontSize: 20,
        paddingTop: 10,
        textAlign: 'center'
    },
    title2: {
        color: '#ff8a2a',
        fontSize: 20,
        paddingBottom: 10,
        textAlign: 'center'
    },
    card: {
        width: '90%',
        backgroundColor: 'white',
        alignSelf: 'center',
        elevation: 3,
        borderRadius: 5,
        shadowOffset: { width: 0.5, height: 0.8 },
        shadowRadius: 4,
        shadowColor: 'black',
        shadowOpacity: 0.5,
    },
    cardheaderrow: {
        flexDirection: 'row',
        backgroundColor: '#ff8a2a',
        padding: 10,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    col: {
        width: '70%',
        paddingLeft: 5
    },
    title3: {
        fontSize: 15,
        color: 'white'
    },
    col2: {
        width: '30%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-around'
    },
    title4: {
        color: 'white',
        fontSize: 20
    },
    cardcontainer: {
        padding: 15,
        backgroundColor: 'white',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    listitemview: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    title5: {
        color: 'black',
        fontSize: 12,
        marginLeft: 10
    },
    subscribebtn: {
        width: '38%',
        borderRadius: 50,
        backgroundColor: '#f37c26',
        padding: 8,
        alignItems: 'center',
        alignSelf: 'center',
        elevation: 8,
        position: 'absolute',
        bottom: 30,
        shadowOffset: { width: 0.8, height: 0.8 },
        shadowRadius: 4,
        shadowOpacity: 0.5,
    }
});
