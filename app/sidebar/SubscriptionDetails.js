
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Animated,
  AsyncStorage,

} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../componnents/Layout'
import I18n from 'react-native-i18n';
import Api from '../componnents/api';
import MessageBar from '../componnents/messageBar';
import { getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { Header, Icon } from 'native-base';

export default class SubscriptionDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      errorMsg: '',
      profile: '',
      height: new Animated.Value(0),
      opacity: new Animated.Value(0),
      openModal: false,
      color: 'red'
    }
  }
  componentWillMount() {

    getDeviceLanguage().then(lng => {
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))

    AsyncStorage.getItem('userData').then(user => {
      // console.log("data0------sidebaer : ", JSON.parse(user))
      user = JSON.parse(user)
      this.setState({ profile: user.image })
    })
  }

  render() {
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    const dot = <Icon name='dot-single' type='Entypo' style={{color:'#ff8a2a',fontSize:18}} />

    return (
      <View style={styles.container}>
        <Header style={[styles.header]}>
          <View style={{ width: '10%', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => Actions.pop()}
            >
              <Icon name='arrow-left-circle' type='SimpleLineIcons' style={{ color: 'white' }} />
            </TouchableOpacity>
          </View>
          <View style={{ width: '80%', alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>{I18n.t('subscription details')}</Text>
          </View>
          <View style={{ width: '10%', alignItems: 'center' }}>
          
          </View>
        </Header>
        <StatusBar backgroundColor='#020304' barStyle="light-content" />
        <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color} />
        <View style={{backgroundColor:'white',flex:1,padding:10,borderRadius:5}}>
                        <View style={{alignItems:'center',paddingVertical:10}}>
                            <Text style={{fontSize:18,fontWeight:'bold',color:'#ff8a2a'}}>Subscription Details</Text>
                        </View>
                        <ScrollView>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>{this.props.selectedSubscription} Subscription will be charged for {this.props.selectedSubscriptionPrice}</Text>
                            </View>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>Payment will be charged on {Platform.OS == 'ios' ? 'iTunes' : 'Google'} Account at confirmation of purchase</Text>
                            </View>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>Subscription automatically renews unless auto-renew is turned OFF at least 24 hours before the end of the current period</Text>
                            </View>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of renewal</Text>
                            </View>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>Subscriptions may be managed by the user and auto-renewal may be turned OFF by going to the users account settings after purchase</Text>
                            </View>
                            <View style={{flexDirection:'row',width:'95%',marginVertical:3}}>
                            {dot}<Text>Any unused portion of a free trial period, if offered, will be forfeited when the user purchases a subscription to that publication, where applicatble </Text>
                            </View>
                        </ScrollView>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <TouchableOpacity onPress={()=>{Actions.pop()}} style={{padding:5,borderColor:'#ff8a2a',borderWidth:1,borderRadius:5,width:'30%',alignItems:'center'}}>
                                <Text style={{color:'#ff8a2a'}}>No Thanks</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.props.buy(true),Actions.pop()}} style={{padding:5,borderColor:'#ff8a2a',borderWidth:1,borderRadius:5,width:'30%',alignItems:'center'}}>
                                <Text style={{color:'#ff8a2a'}}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                 
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    //height: Dimensions.get('window').height - 24,
    justifyContent: 'center',
    padding: 20
  },
  header: {
    width: '100%',
    backgroundColor: '#020304',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 20
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '85%',
    height: 40,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },


});
