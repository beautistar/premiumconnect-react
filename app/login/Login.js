
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Modal,
  Alert,
  Linking,
  AppState,
  PermissionsAndroid,
  Keyboard
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { LoginManager, AccessToken } from "react-native-fbsdk";
import I18n from 'react-native-i18n';
import { setLayout } from '../componnents/Layout'
import { setDeviceLanguage, getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { GoogleSignin} from 'react-native-google-signin';
import firebase from 'react-native-firebase';
import RNFirbase from '../componnents/Firebase'
import FCM, { FCMEvent } from 'react-native-fcm'
import Api from '../componnents/api';
import Geocoder from 'react-native-geocoding';
import MessageBar from '../componnents/messageBar'
import RNSettings from 'react-native-settings'
import CountryCodeList from 'react-native-country-code-list'

var cityName, userLocation, location, fcmToken;
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      phoneNumber: '',
      profile: '',
      socialID: '',
      errorMsg: '',
      phone: '',
      indicator: true,
      countryCode: '+1',
      openModal: false,
      appState: AppState.currentState,
      email: '',
      password: '',
      emailErr: true,
      passwordErr: true,
      loginWithEmail: false,
      deviceToken:'',
    }
  }
  _openLocationSetting() {
    if (Platform.OS == 'android') {
      RNSettings.openSetting(RNSettings.ACTION_LOCATION_SOURCE_SETTINGS).
        then((result) => {
          if (result === RNSettings.ENABLED) {
            // console..log('location is enabled')
          }
        })
    }
    else {
      Linking.openURL("App-Prefs:root=Privacy&path=LOCATION_SERVICES")
    }
  }
  _getLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      // console..log('Current Location', position.coords.latitude, position.coords.longitude)
      Geocoder.init("AIzaSyCJDz_lrzpIdEqfkW4TH1UHzc_55Z-lxy0"); // use a valid API key
      userLocation = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }

      Geocoder.from({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      })
        .then(json => {
          // console..log('Location Address',json)
          var _filter = json.results.filter((item) => {
            if (item.types.includes('administrative_area_level_1')) {
              return item
            }
          })
          // console..log('Filter', _filter)
          // console..log(_filter[0].address_components[0].long_name);
          cityName = _filter[0].address_components[0].long_name
          // console..log(_filter[0].geometry.location)
          location = _filter[0].geometry.location

        })
        .catch(error =>  console.log('Geocoder Error',error));

    }, (error) => {
      // console..log(error)

      if (error.code == 2)
        Alert.alert(
          'Location Service Disable',
          'Please enable location service to use this App',
          [
            { text: 'OK', onPress: () => { this._openLocationSetting() } },
          ],
          { cancelable: false }
        )
    },
      { enableHighAccuracy: false, timeout: 20000 }
    )
  }
  _checkPermission() {
    PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
      .then((result) => {
        // console..log('Permission result', result)
        if (result) {
          PermissionsAndroid.request('android.permission.ACCESS_FINE_LOCATION')
            .then((result) => {
              if (result == 'granted') {
                this._getLocation()
              }
              else {
                this._checkPermission()
              }
            })
        }
        else {
          this._getLocation()
        }
      })
  }
  getDeviceToken(){
    FCM.getFCMToken()
    .then((token) => {
      // console..log('FCM TOken', token)
      fcmToken = token
      if(token){
        this.setState({deviceToken:token})
        AsyncStorage.setItem('deviceId', fcmToken.toString())
      }
      else{
        this.getDeviceToken()
      }
      
    })
  }
  componentWillMount() {
   
    this.getDeviceToken()
    AsyncStorage.getItem('token')
      .then((token) => {
        // console..log('Login Token', token)
        if (token) {
          Actions.reset('Home')
        }
        else {
          this.setState({ indicator: false })
        }
      })
    getDeviceLanguage().then(lng => {
      // console..log('device Langauge==========: ', lng)
      if (lng) {
        this.setState({ deviceLanguage: lng })
      } else {
        setDeviceLanguage()
      }
    })
  }
  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    FCM.getInitialNotification().then(res => {
      try {
        let resObj = JSON.stringify(res)
        // console..log("resObj", resObj)
      } catch (e) {
        // console..log(e)
      }
      if (Platform.OS == 'android') {
        this._checkPermission()
      }
      else {
        this._getLocation()
      }
    });

    GoogleSignin.configure({
      webClientId: '664483444581-mo081ldmbm5vgem3im56iq9kshoqqcik.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      iosClientId: '664483444581-ml6igvvmudu1usdjqe9hka51f063v2ad.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });664483444581
    getAppLanguage().then(lng => {
      if (lng) {
        this.setState({ appLanguage: lng })
      }
    })


  }
  
 
  
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }
  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      // console..log('App has come to the foreground!');
      this._getLocation()
    }
    this.setState({ appState: nextAppState });
  };
  signInWithPhoneNumber() {
    this._getLocation()
    phoneNumber = this.state.phoneNumber
    var apiData = {
      latitude: userLocation.latitude,
      longitude: userLocation.longitude,
      cityName: cityName,
      city_latitude: location.lat,
      city_longitude: location.lng,
      phoneNumber: phoneNumber,
      countryCode: this.state.countryCode,
      deviceId: this.state.deviceToken
    }
    // console..log('Login Api Data', apiData)
    if (this.state.phoneNumber)
      Api.post('/login', apiData)
        .then((result) => {
          // console..log("result : ", result)
          if (result.data.status == 'Success') {
            // firebase.auth().signInWithPhoneNumber(this.state.countryCode + phoneNumber)
            //   .then(confirmResult => {
            //   //  AsyncStorage.setItem('deviceId', this.state.deviceToken.toString())
            //     Actions.OtpPassword({ confirmResult: confirmResult, countryCode:this.state.countryCode ,phoneNumber: phoneNumber, token: result.data.token, userData: result.data.user })
            //     // console..log('result======>', confirmResult)
            //   })
            //   .catch(error =>  console.log(`Sign In With Phone Number Error: ${error.message}` ))
              // Api.post('/send-otp',{phoneNumber:this.state.countryCode + phoneNumber}).then(res=>{
              //   //console.log('otp response : ',res)
              //   if(res.status=='Success'){
                  Actions.OtpPassword({ phoneNumber: this.state.countryCode +phoneNumber, token: result.data.token, userData: result.data.user })
              //   }
              // })
          }
          else {
            this.setState({ errorMsg: result.data.message })
            this.msgBarRef._animateMessage()
          }
        })
    else {
      this.setState({ errorMsg: 'Please Enter Phone Number' })
      this.msgBarRef._animateMessage()
    }
  }

  _socialLogin() {
    this._getLocation()
    var data = {
      latitude: userLocation.latitude,
      longitude: userLocation.longitude,
      cityName: cityName,
      city_latitude: location.lat,
      city_longitude: location.lng,
      phoneNumber: this.state.phone,
      emailId: this.state.email,
      signUp_with: this.state.loginWith,
      socialId: this.state.socialID,
      fullName: this.state.name,
      image: this.state.profile,
      deviceId:this.state.deviceToken

    }
    // console..log('Api data', data)
    console.log('token : ',this.state.deviceToken)
    Api.post('/register', data)
      .then((result) => {
        // console..log('Socila login api ', result)
        if (result.data.status == "Success") {
          AsyncStorage.setItem('token', result.data.token)
          AsyncStorage.setItem('userData', JSON.stringify(result.data.user))
         // AsyncStorage.setItem('deviceId', this.state.deviceToken.toString())
          if (result.data.user.subscriptions) {
            AsyncStorage.setItem('receipt', JSON.stringify(result.data.user.subscriptions[result.data.user.subscriptions.length - 1]))
          }
          Api.postWithToken('/api/createDevGroup', null, result.data.token)
            .then((result) => {
              // console..log('Create Group api', result)
              Actions.reset('Home')
            })

        }
      })
  }
 
  async _googleSignUp() {
    console.log('google calllll')
    try {
      GoogleSignin.signIn()
        .then((result) => {
         // alert("sucessful login ")
           console.log('google', result.user)
          if (result.user.id) {
             this.setState({ socialID: result.user.id, loginWith: 'google', email: result.user.email, name: result.user.name, profile: result.user.photo, phone: null }, () => this._socialLogin())
          }
        }).catch((error)=>{
          // alert("sucessful fail "+error.message.toString())
          console.log('google error : ', error)
       })
    } catch (error) {
       // alert("sucessful fail "+error.message.toString())
       console.log('error', error)
    }
  }
  _facebookSignUp() {
    LoginManager.logOut()
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
         // alert('Login cancelled');
        }
        else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let token = data.accessToken;
            //// console..log(token)
            fetch('https://graph.facebook.com/v3.0/me?fields=email,name,picture&access_token=' + token)
              .then((resp) => resp.json())
              .then((result) => {
                // console..log('facebook', result)
                if (result.id) {
                  this.setState({ socialID: result.id, loginWith: 'facebook', email: result.email, name: result.name, profile: result.picture.data.url }, () => this._socialLogin())
                }
              })
          })
        }
      })
  }
  _loginWithEmail() {
    if (this.state.email && this.state.password) {

      var data = {
        latitude: userLocation.latitude,
        longitude: userLocation.longitude,
        cityName: cityName,
        city_latitude: location.lat,
        city_longitude: location.lng,
        emailId: this.state.email,
        password: this.state.password,
        deviceId: this.state.deviceToken
      }
      // console..log("data : ", data)
      Api.post('/loginWithEmail', data)
        .then((result) => {
          // console..log('result api : ', result)
          if (result.data.status == 'Success') {
            // console..log('is admin : ', result.data.user.isAdmin)
            AsyncStorage.setItem('isAdmin', JSON.stringify(result.data.user.isAdmin))
            AsyncStorage.setItem('token', result.data.token)
           // AsyncStorage.setItem('deviceId', this.state.deviceToken.toString())
            AsyncStorage.setItem('userData', JSON.stringify(result.data.user))
            Api.postWithToken('/api/createDevGroup', null, result.data.token)
              .then((result) => {
                // console..log('Create Group api', result)
                Actions.reset('Home')
              })
          }
          else {
            this.setState({ errorMsg: 'Please check your email and password' })
            this.msgBarRef._animateMessage()
          }
        })

    }
    else {
      this.setState({ errorMsg: 'Please fill all the details' })
      this.msgBarRef._animateMessage()
    }
  }
  render() {
    // console..log('FCM TOken', fcmToken)

    let { countryFlag } = this.state
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    if (this.state.indicator) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color='#000000' size='large' />
        </View>
      )
    }
    else {
      return (
        <ImageBackground source={require('../assets/images/Bg.png')} resizeMode='cover' style={styles.container}>
          <StatusBar backgroundColor='#020304' barStyle="light-content" />
          <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} />
          <ScrollView style={{ width: '100%' }}>
            <View style={styles.subcontainer}>
              <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
              <Text style={styles.appname}>PREMIUM CONNECT</Text>
              <Text style={styles.subtitle}>{I18n.t('to') + " " + I18n.t('connect') + " " + I18n.t('with') + " " + I18n.t('bikers') + " " + I18n.t('friends')}</Text>
              <View style={styles.box}>
                <Text style={styles.title}>{I18n.t('to')}</Text>
                <Text style={styles.title2}>{I18n.t('and') + " " + I18n.t('join') + " " + I18n.t('your') + " " + I18n.t('group')}</Text>
                {!this.state.loginWithEmail ? <View style={{ alignItems: 'center' }}>
                  <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                    <Image source={require('../assets/images/Otp_Phone_icon.png')} style={styles.phoneicon} />
                    <TouchableOpacity style={{ marginRight: 10 }} onPress={() => this.setState({ openModal: true })}>
                      <Text>{this.state.countryCode}</Text>
                    </TouchableOpacity>
                    <TextInput
                      keyboardType='phone-pad'
                      autoCapitalize = 'none'
                      style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '58%' }]}
                      placeholder='Registered Phone Number'
                      onChangeText={(value) => this.setState({ phoneNumber: value })}
                    />
                  </View>
                  <TouchableOpacity
                    onPress={() => this.signInWithPhoneNumber()}
                  >
                    <Image source={require('../assets/images/Otp_btn.png')} style={styles.otpbutton} />
                  </TouchableOpacity>
                  <Text style={styles.title3}>{I18n.t('request') + " " + I18n.t('otp')}</Text>
                </View> :
                  <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                      <Image source={require('../assets/images/email_icon.png')} style={styles.mailicon} />
                      <TextInput
                        keyboardType='email-address'
                        autoCapitalize = 'none'
                        onChangeText={(text) => this.setState({ email: text })}
                        style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                        placeholder={I18n.t('email') + I18n.t('address')}
                      />
                    </View>
                    <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                      <Image source={require('../assets/images/lock.png')} style={styles.loackicon} />
                      <TextInput
                        //keyboardType='visible-password'
                        secureTextEntry={true}
                        autoCapitalize = 'none'
                        onChangeText={(text) => this.setState({ password: text })}
                        style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                        placeholder={I18n.t('password')}
                      />
                    </View>
                    <TouchableOpacity onPress={() => Actions.ResetPassword()} style={{ marginTop: 10,marginBottom:5 }}>
                           <Text style={[styles.title2, { fontSize: 12 }]}>{ I18n.t('reset password') }</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: '#ffa433', alignSelf: 'center', width: '50%', marginTop: 5, borderRadius: 5, padding: 8, alignItems: 'center' }}
                      onPress={() => this._loginWithEmail()}
                    >
                      <Text style={{ color: 'white' }}>{I18n.t('login')}</Text>
                    </TouchableOpacity>
                  </View>}
                <Image source={require('../assets/images/or_icon.png')} style={styles.orbutton} />
                <TouchableOpacity onPress={() => this.setState({ loginWithEmail: !this.state.loginWithEmail })} style={{ marginTop: 10 }}>
                  <Text style={[styles.title2, { fontSize: 12 }]}>{this.state.loginWithEmail ? I18n.t('loginWidthPhoneNumber') : I18n.t('loginWithEmail')}</Text>
                </TouchableOpacity>
               
              </View>

              <View style={styles.fotterview}>
                <View style={[styles.logincontainer, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                  <Text style={styles.buttontitle}>{I18n.t('login') + " " + I18n.t('with')} </Text>
                  <TouchableOpacity onPress={() => this._facebookSignUp()} ><Image source={require('../assets/images/facebook_icon.png')} style={styles.facebookicon} /></TouchableOpacity>
                  <TouchableOpacity onPress={() => this._googleSignUp()}><Image source={require('../assets/images/google_icon.png')} style={styles.googleicon} /></TouchableOpacity>
                </View>
                <TouchableOpacity
                  onPress={() => Actions.SignUp()}
                  style={styles.signupbutton}>
                  <Text style={styles.signupbuttontitle} >{I18n.t('sign-up') + " " + I18n.t('now')}</Text>
                </TouchableOpacity>

              </View>
            </View>
          </ScrollView>
          <Modal
            transparent={true}
            visible={this.state.openModal}
            onRequestClose={() => { }}
          >
            <View style={{ backgroundColor: 'white', flex: 1 }}>
              <TouchableOpacity
                style={{ right: 10, padding: 10, marginTop: Platform.OS == 'ios' ? 30 : 20, position: 'absolute' }}
                onPress={() => this.setState({ openModal: false })}>
                <Text>Cancel</Text>
              </TouchableOpacity>
              <CountryCodeList
                sectionHeaderHeight={0}
                onClickCell={(country) => {
                  this.setState({
                    countryCode: country.code,
                    openModal: false
                  })
                  // console..log(country)
                }
                }
              />
            </View>
          </Modal>
        </ImageBackground>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    height: Dimensions.get('window').height - 24,
    justifyContent: 'center',
    paddingBottom: '15%'
  },
  logo: {
    width: 160,
    height: 100
  },
  appname: {
    color: '#ffa433',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 12
  },
  box: {
    marginTop: 10,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 15
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '80%',
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  mailicon: {
    width: 25,
    height: 20,
    marginHorizontal: 10
  },
  loackicon: {
    width: 45,
    height: 35,
    tintColor: '#ffa433'
  },
  countryFlagIcon: {
    width: 20,
    height: 20,
    marginHorizontal: 5,

  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  inputbox: {
    fontWeight: 'bold',
    fontSize: 12,
    height:35,
    color: 'black',
  },
  otpbutton: {
    width: 50,
    height: 50,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  orbutton: {
    width: 30,
    height: 30,
    marginTop: 10
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  logincontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 8,
    backgroundColor: 'black'
  },
  buttontitle: {
    color: 'white',
    fontSize: 18
  },
  facebookicon: {
    width: 25,
    height: 25,
    marginLeft: 10
  },
  googleicon: {
    width: 25,
    height: 25,
    marginLeft: 10
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 12,
    fontWeight: 'bold'
  }

});
