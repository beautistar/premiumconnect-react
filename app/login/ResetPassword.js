
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ImageBackground,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Modal,
  Alert,
  Linking,
  AppState,
  PermissionsAndroid,
} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { LoginManager, AccessToken } from "react-native-fbsdk";
import I18n from 'react-native-i18n';
import { setLayout } from '../componnents/Layout'
import { setDeviceLanguage, getAppLanguage, getDeviceLanguage } from '../componnents/Language'
import { GoogleSignin } from 'react-native-google-signin';
import firebase from 'react-native-firebase';
import FCM, { FCMEvent } from 'react-native-fcm'
import Api from '../componnents/api';
import Geocoder from 'react-native-geocoding';
import MessageBar from '../componnents/messageBar'
import RNSettings from 'react-native-settings'
import CountryCodeList from 'react-native-country-code-list'
var cityName, userLocation, location, fcmToken;
export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: 'en',
      appLanguage: 'en',
      appState: AppState.currentState,
      email: '',
      password: '',
      emailErr: true,
      passwordErr: true,
      color:'red'
    }
 
  }
  
 
  componentWillMount() {
    AsyncStorage.getItem('token')
      .then((token) => {
        // console.log('Login Token', token)
        if (token) {
          Actions.reset('Home')
        }
        else {
          this.setState({ indicator: false })
        }
      })
    getDeviceLanguage().then(lng => {
      // console.log('device Langauge==========: ', lng)
      if (lng) {
        this.setState({ deviceLanguage: lng })
      } else {
        setDeviceLanguage()
      }
    })
  }
  componentDidMount() {
    getAppLanguage().then(lng => {
      if (lng) {
        this.setState({ appLanguage: lng })
      }
    })


  }
  _resetPassword(){
    if(this.state.email){
      Api.post('/forgot',{emailId:this.state.email})
      .then((result) => {
          // console.log('result :' ,result)
          if(result.status== "Success"){
              this.setState({ errorMsg: 'Reset Password Link Successfully Sent.',color:'green'})
              this.msgBarRef._animateMessage()
              setTimeout(()=>{
                Actions.reset("Login")
              },5000)
          }
          else{
            this.setState({ errorMsg: 'Please Enter your Email ',color:'red'})
            this.msgBarRef._animateMessage()
          }
      })
     
    }
    else{
      this.setState({ errorMsg: 'Please Enter your Email ',color:'red'})
      this.msgBarRef._animateMessage()
    }
  }
 
  render() {
    // console.log('FCM TOken', fcmToken)

    let { countryFlag } = this.state
    I18n.locale = this.state.appLanguage
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)

      return (
        <ImageBackground source={require('../assets/images/Bg.png')} resizeMode='cover' style={styles.container}>
          <StatusBar backgroundColor='#020304' barStyle="light-content" />
          <MessageBar ref={(ref) => this.msgBarRef = ref} error={this.state.errorMsg} color={this.state.color}/>
          <ScrollView style={{ width: '100%' }}>
            <View style={styles.subcontainer}>
              <Image source={require('../assets/images/Logo.png')} style={styles.logo} />
              <Text style={styles.appname}>PREMIUM CONNECT</Text>
              <View style={styles.box}>
                  <View style={{ width: '100%', alignItems: 'center' }}>
                    <View style={[styles.textboxview, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                      <Image source={require('../assets/images/email_icon.png')} style={styles.mailicon} />
                      <TextInput
                        keyboardType='email-address'
                        autoCapitalize = 'none'
                        onChangeText={(text) => this.setState({ email: text })}
                        style={[styles.inputbox, { textAlign: chnageLayout ? 'left' : 'right', width: '84%' }]}
                        placeholder={I18n.t('email') + I18n.t('address')}
                      />
                    </View>
                   
                    <TouchableOpacity style={{ backgroundColor: '#ffa433', alignSelf: 'center', width: '50%', marginTop: 10, borderRadius: 5, padding: 10, alignItems: 'center' }}
                      onPress={() => this._resetPassword()}
                    >
                      <Text style={{ color: 'white' }}>{I18n.t('reset password')}</Text>
                    </TouchableOpacity>
                  </View>
          
              </View>

             
            </View>
          </ScrollView>
        
        </ImageBackground>
      );
    }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    alignItems: 'center',
  },
  subcontainer: {
    alignItems: 'center',
    width: '100%',
    height: Dimensions.get('window').height - 24,
    justifyContent: 'center',
    paddingBottom: '15%'
  },
  logo: {
    width: 150,
    height: 100
  },
  appname: {
    color: '#ffa433',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10
  },
  subtitle: {
    color: 'white',
    fontSize: 15
  },
  box: {
    marginTop: 15,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: 'white',
    fontSize: 20
  },
  title2: {
    color: '#ffa232',
    fontSize: 12,
    fontWeight: 'bold'
  },
  textboxview: {
    padding: Platform.OS == 'ios' ? 5 : 0,
    backgroundColor: 'white',
    width: '80%',
    height: 40,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  mailicon: {
    width: 25,
    height: 20,
    marginHorizontal: 10
  },
  loackicon: {
    width: 45,
    height: 45,
    tintColor: '#ffa433'
  },
  countryFlagIcon: {
    width: 20,
    height: 20,
    marginHorizontal: 5,

  },
  phoneicon: {
    width: 25,
    height: 25,
    marginHorizontal: 10
  },
  inputbox: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'black',
  },
  otpbutton: {
    width: 50,
    height: 50,
    marginTop: 10
  },
  title3: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  orbutton: {
    width: 30,
    height: 30,
    marginTop: 10
  },
  fotterview: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  logincontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 8,
    backgroundColor: 'black'
  },
  buttontitle: {
    color: 'white',
    fontSize: 18
  },
  facebookicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  googleicon: {
    width: 30,
    height: 30,
    marginLeft: 10
  },
  signupbutton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  signupbuttontitle: {
    color: '#ffa433',
    fontSize: 15,
    fontWeight: 'bold'
  }

});
